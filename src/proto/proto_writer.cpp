#include <fstream>

#include <geos/io/WKBWriter.h>

#include "proto_writer.h"

void WriteBuildingStuffToProtobuf(const std::map<long, std::unique_ptr<Node>> &building_nodes,
                                  const std::vector<std::shared_ptr<Building>> &buildings,
                                  const std::string &tile_index) {
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    std::map<osm_id_t, std::shared_ptr<BuildingParent>> parents {};
    std::map<osm_id_t, Zone*> zones {};

    proto::Buildings proto_buildings;
    //the buildings
    BOOST_LOG_TRIVIAL(info) << "Writing " << buildings.size() << " buildings to protobuf";
    for (auto &building : buildings) {
        proto::Building* proto_building = proto_buildings.add_buildings();
        proto_building->set_osm_id(building->GetOSMId());
        for (auto &tag : *building->GetTags()) {
            (*proto_building->mutable_tags())[tag.first] = tag.second;
        }
        for (auto &ref : *building->GetOuterRefs()) {
            proto_building->mutable_outer_refs()->add_refs(ref);
        }
        for (auto &refs : *building->GetInnerRefs()) {
            proto::refs_vec *inner_vec = proto_building->mutable_inner_refs()->add_vecs();
            for (auto &ref : refs) {
                inner_vec->add_refs(ref);
            }
        }
        //check for parent
        if (building->HasParent()) {
            if (not parents.contains(building->GetParent()->GetOSMId())) {
                parents[building->GetParent()->GetOSMId()] = building->GetParent();
            }
        }
        //zone stuff
        auto zone_ptr = building->GetZone();
        proto_building->set_zone_id(zone_ptr->GetOSMId());
        if (not zones.contains(zone_ptr->GetOSMId())) {
            zones[zone_ptr->GetOSMId()] = zone_ptr;
        }
        //roof_hint and neighbours
        proto_building->set_has_neighbours(building->HasNeighbours());
        if (building->HasRoofHint()) {
            auto roof_hint = building->GetRoofHint(false);
            proto::RoofHint* proto_roof_hint = proto_building->mutable_roof_hint();
            proto_roof_hint->set_ridge_orientation(roof_hint->GetRidgeOrientation());
            if (roof_hint->HasInnerNodeDefined()) {
                proto_roof_hint->set_inner_node_x(roof_hint->GetInnerNodeXY().first);
                proto_roof_hint->set_inner_node_y(roof_hint->GetInnerNodeXY().second);
            }
            proto_roof_hint->set_node_before_inner_is_shared(roof_hint->GetNodeBeforeInnerIsShared());
        }
    }

    //the building parents
    BOOST_LOG_TRIVIAL(info) << "Writing " << parents.size() << " building parents to protobuf";
    for (auto &it : parents) {
        proto::BuildingParent* proto_building_parent = proto_buildings.add_parents();
        proto_building_parent->set_osm_id(it.first);
        for (auto &tag : *it.second->GetTags()) {
            (*proto_building_parent->mutable_tags())[tag.first] = tag.second;
        }
        proto_building_parent->set_simple_3d(it.second->IsSimple3d());
        for (auto &osm_id : it.second->GetOSMIdOfChildren())
            proto_building_parent->add_children(osm_id);
    }

    // FIXME https://gitlab.com/osm2city/osm2gear/-/issues/43 transfer shared_refs

    //the building nodes
    BOOST_LOG_TRIVIAL(info) << "Writing " << building_nodes.size() << " nodes to protobuf";
    for (auto &it : building_nodes) {
        proto::Node* proto_node = proto_buildings.add_nodes();
        proto_node->set_osm_id(it.second->GetOSMId());
        proto_node->set_lon(it.second->GetLonLat().lon);
        proto_node->set_lat(it.second->GetLonLat().lat);
    }

    //the zones
    BOOST_LOG_TRIVIAL(info) << "Writing " << zones.size() << " zones to protobuf";
    for (auto &it : zones) {
        proto::Zone* proto_zone = proto_buildings.add_zones();
        proto_zone->set_osm_id(it.first);
        proto_zone->set_building_zone_type(static_cast<int>(it.second->GetBuildingZoneType()));
        proto_zone->set_settlement_type(static_cast<int>(it.second->GetSettlementType()));
        auto* proto_wkb = new proto::WellKnownBinary();
        geos::io::WKBWriter wkbWriter;
        std::stringstream s;
        wkbWriter.write(*it.second->GetGeometry(), s);
        proto_wkb->set_wkb(s.str());
        proto_zone->set_allocated_geometry(proto_wkb);
    }

    //write out to file
    auto filename = BuildFileNameWithTileIndex("buildings", tile_index, "proto", false);
    auto success = true;
    std::fstream output_b(filename, std::ios::out | std::ios::trunc | std::ios::binary);
    if (!proto_buildings.SerializeToOstream(&output_b)) {
        success = false;
    }
    google::protobuf::ShutdownProtobufLibrary();
    if (not success) {
        throw std::runtime_error("Failed to write buildings info to proto: " + filename);
    }
}
