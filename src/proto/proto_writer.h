
#ifndef OSM2GEAR_PROTO_WRITER_H
#define OSM2GEAR_PROTO_WRITER_H

#include "buildings.pb.h"
#include "../main/buildings.h"

/*! \brief Write messages for building information to a proto file, such that the info can be read by Python
 *
 * Throws runtime_error if the file cannot be written
 */
void WriteBuildingStuffToProtobuf(const std::map<long, std::unique_ptr<Node>> &building_nodes,
                                  const std::vector<std::shared_ptr<Building>> &,
                                  const std::string &);


#endif  // OSM2GEAR_PROTO_WRITER_H
