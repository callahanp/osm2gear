#include "osm_types.h"

#include <sstream>
#include <utility>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/hawick_circuits.hpp>

#include <geos/geom/Coordinate.h>
#include <geos/geom/CoordinateSequence.h>
#include <geos/geom/CoordinateSequenceFactory.h>
#include <geos/geom/GeometryFactory.h>
#include <geos/operation/polygonize/Polygonizer.h>


osm_id_t IdCreator::GetNextPseudoOSMId(OSMFeatureType osm_feature_type) {
    ++counter_;
    osm_id_t type_factor = 1'000'000 * (int)osm_feature_type;
    return -1 * (counter_ + type_factor);
}

InvalidGeometryFromOSM::InvalidGeometryFromOSM(osm_id_t osm_id, InvalidGeometryFromOSMType e_type): std::runtime_error("") {
    std::stringstream ss;
    ss << "Invalid geometry based on OSM Data: ";
    ss << mapper.at(e_type); //yes, if missing we will get an exception - we test runtime and catch it during development
    ss << " (osm id = " + std::to_string(osm_id) << ")";
    //TODO: https://gitlab.com/osm2city/osm2gear/-/issues/14 e_type should be translated to string and included into message
    static_cast<std::runtime_error&>(*this) = std::runtime_error(ss.str());
}

Element::Element(osm_id_t osm_id): osm_id_ {osm_id} {
    tags_ = {};
}

void Element::AddTag(const std::string &key, std::string &value) {
    tags_[key] = value;
}

osm_id_t Element::GetOSMId() const {
    return osm_id_;
}

const tags_t* Element::GetTags() const {
    const tags_t* tags_pointer = &tags_;
    return tags_pointer;
}

tags_t CombineTags(const tags_t* primary, const tags_t* secondary) {
    tags_t combined_tags {};
    for (const auto& it : *primary) {
        combined_tags[it.first] = it.second;
    }
    for (const auto& it : *secondary) {
        if (not combined_tags.contains(it.first)) {
            combined_tags[it.first] = it.second;
        }
    }
    return combined_tags;
}

Node::Node(osm_id_t osm_id, LonLat lon_lat): lon_lat_ (lon_lat), Element(osm_id) {
}

Node::Node(osm_id_t osm_id, LonLat lon_lat, tags_t &tags): lon_lat_ (lon_lat), Element(osm_id) {
    for (auto [key, val]: tags) {
        AddTag(key, val);
    }
}

LonLat Node::GetLonLat() const {
    return lon_lat_;
}

SGGeod Node::GetSGGeod() const {
    return SGGeod::fromDeg(lon_lat_.lon, lon_lat_.lat);
}

std::unique_ptr<geos::geom::Point> Node::PointFromOSMNode(const TileHandler &tile_handler) const {
    geos::geom::GeometryFactory::Ptr factory =
        TileHandler::MakeGeometryFactory();
    geos::geom::Coordinate coord = tile_handler.ToLocal(this->GetLonLat());
    return std::unique_ptr<geos::geom::Point>{factory->createPoint(coord)};
}

Way::Way(osm_id_t osm_id, refs_t &refs, tags_t &tags): refs_ (refs), Element(osm_id) {
    for (auto [key, val]: tags) {
        AddTag(key, val);
    }
}

const refs_t* Way::GetRefs() const {
    const refs_t* refs_pointer = &refs_;
    return refs_pointer;
}

bool Way::CheckAtLeastOneNodeInTile(const node_map_t &nodes_map, const TileHandler &tile_handler) const {
    for (osm_id_t ref : *this->GetRefs()) {
        if (!nodes_map.contains(ref)) {
            throw InvalidGeometryFromOSM(this->osm_id_, InvalidGeometryFromOSMType::nodeNotFound);
        }
        if (tile_handler.GetTileBoundary().IsInsideBoundary(nodes_map.at(ref)->GetLonLat())) {
            return true;
        }
    }
    return false;
}


std::unique_ptr<geos::geom::LinearRing> Way::LinearRingFromRefs(const osm_id_t &osm_id, const refs_t * refs,
                                                                const node_map_t &nodes_map,
                                                                const TileHandler &tile_handler) {
    geos::geom::GeometryFactory::Ptr factory =
        TileHandler::MakeGeometryFactory();
    std::vector<geos::geom::Coordinate> coordinates {};
    for (osm_id_t ref : *refs) {
        if (!nodes_map.contains(ref)) {
            throw InvalidGeometryFromOSM(osm_id, InvalidGeometryFromOSMType::nodeNotFound);
        }
        geos::geom::Coordinate coord =
            tile_handler.ToLocal(nodes_map.at(ref)->GetLonLat());
        coordinates.push_back(coord);
    }
    //need to guarantee that the last coordinate is the same as the first - mostly for buildings, where we consciously have removed the repeating element
    if (refs->front() != refs->back()) {
        coordinates.push_back(
            tile_handler.ToLocal(nodes_map.at(refs->front())->GetLonLat()));
    }
    if (coordinates.size() < 4) {  //a linear ring needs at least 3 unique coordinates plus the repeated node at start
        throw InvalidGeometryFromOSM(osm_id, InvalidGeometryFromOSMType::notEnoughPoints);
    }
    const geos::geom::CoordinateSequenceFactory* coord_factory = factory->getCoordinateSequenceFactory();
    std::unique_ptr<geos::geom::LinearRing> ring = factory->createLinearRing(coord_factory->create(std::move(coordinates)));

    if (ring->isEmpty()) {
        throw InvalidGeometryFromOSM(osm_id, InvalidGeometryFromOSMType::emptyGeometry);
    }
    if (not ring->isValid()) {  // let us try with a trick
        std::unique_ptr<geos::geom::CoordinateSequence> cleaned_ccord_seq = ring->buffer(0)->getCoordinates();
        if (cleaned_ccord_seq->getSize() >= 3) {
            ring = factory->createLinearRing(std::move(cleaned_ccord_seq));
        }
    }
    if (not ring->isValid()) {
        throw InvalidGeometryFromOSM(osm_id, InvalidGeometryFromOSMType::invalidGeometry);
    }
    if (not ring->isSimple()) {
        throw InvalidGeometryFromOSM(osm_id, InvalidGeometryFromOSMType::complexGeometry);
    }
    return ring;
}

std::unique_ptr<geos::geom::LineString> Way::LineStringFromRefs(const osm_id_t &osm_id, const refs_t * refs, const node_map_t &nodes_map, const TileHandler &tile_handler) {
    geos::geom::GeometryFactory::Ptr factory =
        TileHandler::MakeGeometryFactory();
    std::vector<geos::geom::Coordinate> coordinates {};
    for (osm_id_t ref : *refs) {
        if (!nodes_map.contains(ref)) {
            throw InvalidGeometryFromOSM(osm_id, InvalidGeometryFromOSMType::nodeNotFound);
        }
        geos::geom::Coordinate coord =
            tile_handler.ToLocal(nodes_map.at(ref)->GetLonLat());
        coordinates.push_back(coord);
    }
    if (coordinates.size() < 2) {
        throw InvalidGeometryFromOSM(osm_id, InvalidGeometryFromOSMType::notEnoughPoints);
    }
    const geos::geom::CoordinateSequenceFactory* coord_factory = factory->getCoordinateSequenceFactory();
    std::unique_ptr<geos::geom::LineString> line = factory->createLineString(coord_factory->create(std::move(coordinates)));

    if (line->isEmpty()) {
        throw InvalidGeometryFromOSM(osm_id, InvalidGeometryFromOSMType::emptyGeometry);
    }
    if (not line->isValid()) {
        throw InvalidGeometryFromOSM(osm_id, InvalidGeometryFromOSMType::invalidGeometry);
    }
    if (not line->isSimple()) {
        throw InvalidGeometryFromOSM(osm_id, InvalidGeometryFromOSMType::complexGeometry);
    }
    return line;
}

std::unique_ptr<geos::geom::LinearRing> Way::LinearRingFromOSMWay(const node_map_t &nodes_map, const TileHandler &tile_handler) const {
    geos::geom::GeometryFactory::Ptr factory =
        TileHandler::MakeGeometryFactory();
    return Way::LinearRingFromRefs(this->osm_id_, this->GetRefs(), nodes_map,
                                   tile_handler);
}

std::unique_ptr<geos::geom::Polygon> Way::PolygonFromOSMWay(const node_map_t &nodes_map, const TileHandler &tile_handler) const {
    geos::geom::GeometryFactory::Ptr factory = TileHandler::MakeGeometryFactory();
    std::unique_ptr<geos::geom::LinearRing> ring = Way::LinearRingFromRefs(this->osm_id_, this->GetRefs(), nodes_map, tile_handler);
    return factory->createPolygon(std::move(ring));
}

/*! \brief Used to catch values from visited cycles in a Boost graph and store in a referenced container.
 *
 */
struct CycleVisitor {
    //A visitor is passed by value - therefore we need a pointer to an external container such that
    //the values are available later (when the visitor is dead)
    std::vector<std::vector<int>>* cycles;

    void cycle(auto const& c, auto const& g) {
        if (c.size() > 2) {  // it is an undirected graph and in OSM we want at least 3 vertices
            std::vector<int> cycle_references {};
            for(int i=0; i < c.size(); ++i) {
                cycle_references.push_back(c.at(i));
            }
            cycles->push_back(cycle_references);
        }
    }
};

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,
        // vertex properties
        boost::property<boost::vertex_index_t, osm_id_t,
        boost::property<boost::vertex_color_t, boost::default_color_type> >,
        // edge properties
        boost::no_property> DirectedOSMIdGraph;

/*! \brief Create polygons (which can have inner holes) from multiple line strings.
 *
 * Often the line strings will come from OSM multi-polygons.
 */
std::vector<std::unique_ptr<geos::geom::Polygon>> PolygonizedWaysFromMultipleWays(const std::vector<std::shared_ptr<Way>> &way_parts,
                                                                  const node_map_t &nodes_map,
                                                                  const TileHandler &tile_handler) {
    std::unique_ptr<geos::operation::polygonize::Polygonizer> polygonizer = std::make_unique<geos::operation::polygonize::Polygonizer>(true);
    //need to store the lines so the geometry pointer survives until polygonizer is done
    std::vector<std::unique_ptr<geos::geom::LineString>> line_strings;
    for (auto const& way_part : way_parts) {
        auto line_string = Way::LineStringFromRefs(way_part->GetOSMId(), way_part->GetRefs(), nodes_map, tile_handler);
        polygonizer->add(line_string->getGeometryN(0));
        line_strings.push_back(std::move(line_string));
    }
    auto candidates = polygonizer->getPolygons();
    std::vector<std::unique_ptr<geos::geom::Polygon>> result {};
    for (auto &candidate : candidates) {
        if (candidate->isValid()) {
            result.push_back(std::move(candidate));
        }
    }
    return result;
}

/*!
 * This uses the hawick_unique_circuits() method of cycle detection simply because I got it to work.
 * https://godbolt.org/z/9MroeGEPq has a way of making tiernan_all_cycles working, however the
 * workaround mentioned to get it compiling I cannot make work (and I am not sure whether tiernan is better
 * than hawick):
 * namespace boost {
 *  // see https://github.com/boostorg/graph/issues/182
 *  void renumber_vertex_indices(adjacency_list<>&) {}
 * } // namespace boost
 */
std::vector<std::unique_ptr<Way>> ClosedWaysFromMultipleWays(const std::vector<std::shared_ptr<Way>> &way_parts,
                                                             const bool transfer_tags_from_original_ways) {
    // first find all cycles
    DirectedOSMIdGraph g;

    //Need to do a mapping between large OSM ids and int in order not to get allocation errors
    //https://stackoverflow.com/questions/71099900/how-to-use-vertice-identifiers-in-boost-graph-of-size-int64-t
    std::map<int, osm_id_t> vertex_to_osm {};
    std::map<osm_id_t , int> osm_to_vertex {};

    int vertex_index {0};
    int current, previous;
    bool is_first = true;
    for (auto &way : way_parts) {
        auto refs = * way->GetRefs();
        for (auto ref : refs) {
            if (!osm_to_vertex.contains(ref)) {
                vertex_to_osm[vertex_index] = ref;
                osm_to_vertex[ref] = vertex_index;
                ++vertex_index;
            }
            current = osm_to_vertex[ref];
            if (is_first) {
                is_first = false;
            } else {  //only when we have passed the first we know the previous
                boost::add_edge(previous, current, g);
            }
            previous = current;
        }
    }
    //Can be used for debugging: print_graph(g); needs #include <boost/graph/graph_utility.hpp>

    std::vector<std::vector<int>> total_cycles {};
    CycleVisitor visitor{&total_cycles};
    hawick_unique_circuits(g, visitor);  //finds the actual cycles aka. circuits

    //create new connected ways from cycles
    std::vector<std::unique_ptr<Way>> connected_ways {};
    for (auto &cycle : total_cycles) {
        tags_t cycle_way_tags {};
        refs_t cycle_way_refs {};
        // find the original tags - we will use brute force and the last one wins if several with same key
        // were found
        osm_id_t first_node {0}; //needed at the end to close the ring
        for (auto vertex_id : cycle) {
            auto osm_id = vertex_to_osm[vertex_id];
            cycle_way_refs.push_back(osm_id);
            if (first_node == 0) {
                first_node = osm_id;
            }
            if (transfer_tags_from_original_ways) {
                for (auto &way: way_parts) {
                    auto refs = *way->GetRefs();
                    std::vector<osm_id_t>::iterator iter;
                    iter = find(refs.begin(), refs.end(), osm_id);
                    if (iter != refs.end()) {
                        auto way_tags = *way->GetTags();
                        for (auto &it: way_tags) {
                            cycle_way_tags[it.first] = it.second;
                        }
                    }
                }
            }
        }
        cycle_way_refs.push_back(first_node);
        connected_ways.push_back(std::make_unique<Way>(IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::generic_way), cycle_way_refs, cycle_way_tags));
    }
    return connected_ways;
}

MemberRole ParseMemberRole(const std::string &role, bool handle_empty_role_as_outer) {
    if (handle_empty_role_as_outer and role.empty()) {
        return MemberRole::outer;  //deprecated - sometimes empty means outer
    }
    if (role == v_inner) {
        return MemberRole::inner;
    }
    if (role == v_outer) {
        return MemberRole::outer;
    }
    if (role == v_outline) {
        return MemberRole::outline;
    } else {
        return MemberRole::undefined;
    }
}

Member::Member(osm_id_t ref, MemberType member_type, MemberRole member_role): ref_ {ref}, type_ {member_type}, role_ {member_role} {}

Relation::Relation(osm_id_t osm_id, tags_t &tags): Element(osm_id) {
    for (auto [key, val]: tags) {
        AddTag(key, val);
    }
}

std::vector<std::unique_ptr<geos::geom::LinearRing>> LinearRingsFromWays(const std::vector<std::unique_ptr<Way>> &connected_ways,
                                                                         const node_map_t &rel_nodes_dict, const TileHandler &tile_handler) {
    std::vector<std::unique_ptr<geos::geom::LinearRing>> rings{};
    for (auto &way: connected_ways) {
        std::unique_ptr<geos::geom::LinearRing> ring;
        try {
            ring = Way::LinearRingFromRefs(way->GetOSMId(), way->GetRefs(), rel_nodes_dict, tile_handler);
        } catch (InvalidGeometryFromOSM &e) {
            continue;  //happens sometimes - here we can live with it and just ignore the place
        }
        if (ring->isValid() and !ring->isEmpty()) {
            rings.push_back(std::move(ring));
        }
    }
    return rings;
}

std::vector<std::unique_ptr<geos::geom::Polygon>> Relation::PolygonsFromMembers(const OSMReadRelationsResult &rel_result,
                                                                                const TileHandler &tile_handler,
                                                                                const MemberRole member_role,
                                                                                bool check_against_tile_boundary) const {
    std::vector<std::unique_ptr<geos::geom::Polygon>> polygons {};
    std::vector<std::shared_ptr<Way>> member_ways_open{};
    int has_at_least_one_node_in_tile {0};
    bool push_to_open {};
    for (auto &member: members_) {
        if (member->GetType() == MemberType::way and rel_result.rel_ways_dict.contains(member->GetRef())) {
            if (member->GetRole() == member_role) {
                push_to_open = true;
                if (check_against_tile_boundary) {
                    if (rel_result.rel_ways_dict.at(member->GetRef())->CheckAtLeastOneNodeInTile(rel_result.rel_nodes_dict, tile_handler)) {
                        has_at_least_one_node_in_tile++;
                    }
                }
                if (rel_result.rel_ways_dict.at(member->GetRef())->RefsAreClosed()) {
                    try {
                        auto poly =
                            rel_result.rel_ways_dict.at(member->GetRef())->PolygonFromOSMWay(rel_result.rel_nodes_dict, tile_handler);
                        polygons.push_back(std::move(poly));
                        push_to_open = false;
                    } catch (InvalidGeometryFromOSM &e) {
                        push_to_open = true;
                    }
                }
                if (push_to_open) {
                    member_ways_open.push_back(rel_result.rel_ways_dict.at(member->GetRef()));
                }
            }
        }
    }
    if (check_against_tile_boundary and has_at_least_one_node_in_tile == 0) {
        polygons.clear();
        return polygons;
    }
    if (not member_ways_open.empty()) {
        auto polygonized_polys = PolygonizedWaysFromMultipleWays(member_ways_open, rel_result.rel_nodes_dict, tile_handler);
        for (auto &poly : polygonized_polys) {
            polygons.push_back(std::move(poly));
        }
    }
    return MergePolygons(polygons);
}

std::vector<std::unique_ptr<geos::geom::Polygon>> Relation::PolygonsFromAllMembers(const OSMReadRelationsResult &rel_result,
                                                                                   const TileHandler &tile_handler,
                                                                                   bool check_against_tile_boundary) const {

    auto outer_polys = PolygonsFromMembers(rel_result, tile_handler, MemberRole::outer, check_against_tile_boundary);
    auto inner_polys = PolygonsFromMembers(rel_result, tile_handler, MemberRole::inner, check_against_tile_boundary);

    std::vector<std::unique_ptr<geos::geom::Polygon>> result;
    for (auto &outer_poly : outer_polys) {
        std::vector<std::unique_ptr<geos::geom::LinearRing>> matched_inner;
        for (auto &inner_poly : inner_polys) {
            if (inner_poly->within(outer_poly.get())) {
                // a bit lazy here - the inner_poly should never match another outer_poly
                matched_inner.push_back(inner_poly->clone()->releaseExteriorRing());
            }
        }
        if (matched_inner.empty()) {
            result.push_back(std::move(outer_poly));
        } else {
            geos::geom::GeometryFactory::Ptr factory = TileHandler::MakeGeometryFactory();
            result.push_back(factory->createPolygon(outer_poly->releaseExteriorRing(), std::move(matched_inner)));
        }
    }
    return result;
}

std::optional<long> ParseLong(const std::string &osm_input) {
    try {
        return std::stol(osm_input);
    } catch (std::invalid_argument &e) {
        return std::nullopt;
    }
}

std::optional<int> ParseInt(const std::string &osm_input) {
    try {
        return std::stoi(osm_input);
    } catch (std::invalid_argument &e) {
        return std::nullopt;
    }
}

std::optional<double> ParseDouble(const std::string &osm_input) {
    try {
        return std::stod(osm_input);
    } catch (std::invalid_argument &e) {
        return std::nullopt;
    }
}

constexpr char osm_multi_values = ';';

std::optional<double> ParseMaxDouble(const std::string &osm_input) {
    std::vector<std::string> out;
    TokenizeString(osm_input, osm_multi_values, out);
    double max_value {-9999.0};
    bool value_found = false;
    try {
        for (auto &s: out) {
            auto this_value = std::stod(s);
            max_value = std::max(this_value, max_value);
            value_found = true;
        }
    } catch (std::invalid_argument &e) {
        return std::nullopt;
    }
    if (value_found)  {
        return max_value;
    }
    return std::nullopt;
}

int ParseLanes(const tags_t* tags, int default_lanes, bool is_motorway) {
    std::optional<int> my_lanes;
    if (tags->contains(k_lanes)) {
        my_lanes = ParseInt(tags->at(k_lanes));
    } else if (is_motorway) {
        my_lanes = 2;
    }
    return my_lanes.value_or(default_lanes);
}

int ParseTracks(const tags_t* tags) {
    std::optional<int> my_tracks;
    if (tags->contains(k_tracks)) {
        my_tracks = ParseInt(tags->at(k_tracks));
    }
    return my_tracks.value_or(1);
}

bool ParseIsTunnel(const tags_t* tags) {
    if (tags->contains(k_tunnel) and tags->at(k_tunnel) != v_no) {
        return true;
    }
    return false;
}
bool ParseHasEmbankmentOrCutting(const tags_t* tags) {
    if (tags->contains(k_embankment) and tags->at(k_embankment) != v_no) {
        return true;
    }
    if (tags->contains(k_cutting) and tags->at(k_cutting) != v_no) {
        return true;
    }
    return false;
}

bool ParseIsOneWay(const tags_t* tags, bool is_motorway) {
    if (is_motorway) {
        if (tags->contains(k_oneway) and tags->at(k_oneway) == v_no) {
            return false;
        }
        return true;
    } else if (tags->contains(k_oneway) and tags->at(k_oneway) == v_yes) {
        return true;
    }
    return false;
}

bool ParseIsRoundabout(const tags_t* tags) {
    return tags->contains(k_junction) and (tags->at(k_junction) == v_roundabout or tags->at(k_junction) == v_circular);
}

bool ParseHasKeyValue(const tags_t* tags, const char *key, const char *value) {
    return tags->contains(key) and (tags->at(key) == value);
}

bool ParseHasKeyNotValue(const tags_t* tags, const char *key, const char *value) {
    return tags->contains(key) and (tags->at(key) != value);
}

bool ParseIsServiceSpur(const tags_t* tags) {
    return ParseHasKeyValue(tags, k_service, v_spur);
}

std::optional<HighwayType> ParseTagsForHighwayType(const tags_t* tags) {
    if (!tags->contains(k_highway)) {
        return std::nullopt;
    }
    if (ParseIsRoundabout(tags)) {
        return HighwayType::roundabout;
    }
    auto value = tags->at(k_highway);
    if (value == v_motorway) { // must be first due to oneway parsing logic
        return HighwayType::motorway;
    } else if (ParseIsOneWay(tags, false)) {
        if (ParseLanes(tags) > 1) {
            return HighwayType::one_way_multi_lane;
        }
        if (value == v_trunk or value == v_trunk_link or value == v_motorway_link) {
            return HighwayType::one_way_large;
        }
        return HighwayType::one_way_normal;
    } else if (value == v_trunk) {
        return HighwayType::trunk;
    } else if (value == v_primary or value == v_primary_link or value == v_motorway_link or value == v_trunk_link) {
        return HighwayType::primary;
    } else if (value == v_secondary or value == v_secondary_link) {
        return HighwayType::secondary;
    } else if (value == v_tertiary or value == v_tertiary_link) {
        return HighwayType::tertiary;
    } else if (value == v_unclassified) {
        return HighwayType::unclassified;
    } else if (value == v_road) {
        return HighwayType::road;
    } else if (value == v_residential) {
        return HighwayType::residential;
    } else if (value == v_living_street) {
        return HighwayType::living_street;
    } else if (value == v_service) {
        return HighwayType::service;
    } else if (value == v_pedestrian) {
        return HighwayType::pedestrian;
    } else if (value == v_track or value == v_footway or value == v_cycleway or value == v_bridleway or value == v_steps or value == v_path) {
        return HighwayType::slow;
    }
    return std::nullopt;
}

std::optional<PlaceType> ParseTagsForPlaceType(const tags_t* tags) {
    if (!tags->contains(k_place)) {
        return std::nullopt;
    }
    std::string value = tags->at(k_place);
    if (value == v_city) {
        return PlaceType::city;
    } else if (value == v_town) {
        return PlaceType::town;
    } else if (value == v_farm) {
        return PlaceType::farm;
    }
    return std::nullopt;
}

std::optional<WaterwayType> ParseTagsForWaterwayType(const tags_t* tags) {
    if (!tags->contains(k_waterway)) {
        return std::nullopt;
    }
    std::string value = tags->at(k_waterway);
    if (value == v_river or value == v_canal) {
        return WaterwayType::large;
    } else if (value == v_stream or value == v_wadi or value == v_drain or value == v_ditch) {
        return WaterwayType::narrow;
    }
    return std::nullopt;
}

std::optional<RailwayLineType> ParseTagsForRailwayLineType(const tags_t* tags) {
    if (!tags->contains(k_railway)) {
        return std::nullopt;
    }
    std::string value = tags->at(k_railway);
    if (value == v_rail) {
        return RailwayLineType::rail;
    } else if (value == v_subway) {
        return RailwayLineType::subway;
    } else if (value == v_monorail) {
        return RailwayLineType::monorail;
    } else if (value == v_light_rail) {
        return RailwayLineType::light_rail;
    } else if (value == v_funicular) {
        return RailwayLineType::funicular;
    } else if (value == v_narrow_gauge) {
        return RailwayLineType::narrow_gauge;
    } else if (value == v_tram) {
        return RailwayLineType::tram;
    } else if (value == v_abandoned or value == v_construction or value == v_disused or value == v_preserved) {
        return RailwayLineType::other;
    }
    return std::nullopt;
}

std::optional<BuildingZoneType> ParseTagsForBuildingZoneType(const tags_t* tags) {
    if (tags->contains(k_building) or tags->contains(k_building_part)) {
        //A few relations are used for buildings and land-use at the same time.
        // we do not want those. E.g. https://www.openstreetmap.org/relation/9249133
        return std::nullopt;
    }
    if (tags->contains(k_aeroway) && tags->at(k_aeroway) == v_aerodrome) {
        return BuildingZoneType::aerodrome;
    } else if (tags->contains(k_landuse)) {
        std::string tag_value = tags->at(k_landuse);
        if (tag_value == v_aerodrome) {
            return BuildingZoneType::aerodrome;
        } else if (tag_value == v_commercial) {
            return BuildingZoneType::commercial;
        } else if (tag_value == v_farmyard) {
            return BuildingZoneType::farmyard;
        } else if (tag_value == v_industrial) {
            return BuildingZoneType::industrial;
        } else if (tag_value == v_port) {
            return BuildingZoneType::port;
        } else if (tag_value == v_residential) {
            return BuildingZoneType::residential;
        } else if (tag_value == v_retail) {
            return BuildingZoneType::retail;
        }
    }
    return std::nullopt;
}

bool ParseIsYesBuilding(const tags_t* tags) {
    if (tags->contains(k_building)) {
        if (tags->at(k_building) == v_yes) {
            return true;
        }
        return false;
    }
    if (tags->contains(k_building_part)) {
        if (tags->at(k_building_part) == v_yes) {
            return true;
        }
        return false;
    }
    return false;
}

std::optional<BuildingType> ParseTagsForBuildingType(const tags_t* tags) {
    if (tags->contains(k_parking) && tags->at(k_parking) == v_multi_storey) {
        return BuildingType::parking;
    } else if (ParseIsStorageTank(tags, true) or ParseIsStorageTank(tags, false)) {
        return BuildingType::storage_tank;
    } else if (tags->contains(k_building) or tags->contains(k_building_part)) {
        std::string tag_value;
        if (tags->contains(k_building)) {
            tag_value = tags->at(k_building); // must have precedence if both tags are present
        } else {
            tag_value = tags->at(k_building_part);
        }
        if (tag_value == v_parking) {
            return BuildingType::parking;
        } else if (tag_value == v_apartments) {
            return BuildingType::apartments;
        } else if (tag_value == v_attached) {
            return BuildingType::attached;
        } else if (tag_value == v_house) {
            return BuildingType::house;
        } else if (tag_value == v_detached) {
            return BuildingType::detached;
        } else if (tag_value == v_residential) {
            return BuildingType::residential;
        } else if (tag_value == v_dormitory) {
            return BuildingType::dormitory;
        } else if (tag_value == v_terrace) {
            return BuildingType::terrace;
        } else if (tag_value == v_bungalow) {
            return BuildingType::bungalow;
        } else if (tag_value == v_static_caravan) {
            return BuildingType::static_caravan;
        } else if (tag_value == v_cabin) {
            return BuildingType::cabin;
        } else if (tag_value == v_hut) {
            return BuildingType::hut;
        } else if (tag_value == v_commercial) {
            return BuildingType::commercial;
        } else if (tag_value == v_office) {
            return BuildingType::office;
        } else if (tag_value == v_retail) {
            return BuildingType::retail;
        } else if (tag_value == v_industrial) {
            return BuildingType::industrial;
        } else if (tag_value == v_warehouse) {
            return BuildingType::warehouse;
        } else if (tag_value == v_cathedral) {
            return BuildingType::cathedral;
        } else if (tag_value == v_chapel) {
            return BuildingType::chapel;
        } else if (tag_value == v_church) {
            return BuildingType::church;
        } else if (tag_value == v_mosque) {
            return BuildingType::mosque;
        } else if (tag_value == v_temple) {
            return BuildingType::temple;
        } else if (tag_value == v_synagogue) {
            return BuildingType::synagogue;
        } else if (tag_value == v_public) {
            return BuildingType::publicx;
        } else if (tag_value == v_civic) {
            return BuildingType::civic;
        } else if (tag_value == v_school) {
            return BuildingType::school;
        } else if (tag_value == v_hospital) {
            return BuildingType::hospital;
        } else if (tag_value == v_hotel) {
            return BuildingType::hotel;
        } else if (tag_value == v_farm) {
            return BuildingType::farm;
        } else if (tag_value == v_barn) {
            return BuildingType::barn;
        } else if (tag_value == v_cowshed) {
            return BuildingType::cowshed;
        } else if (tag_value == v_farm_auxiliary) {
            return BuildingType::farm_auxiliary;
        } else if (tag_value == v_greenhouse) {
            return BuildingType::greenhouse;
        } else if (tag_value == v_glasshouse) {
            return BuildingType::glasshouse;
        } else if (tag_value == v_stable) {
            return BuildingType::stable;
        } else if (tag_value == v_sty) {
            return BuildingType::sty;
        } else if (tag_value == v_riding_hall) {
            return BuildingType::riding_hall;
        } else if (tag_value == v_slurry_tank) {
            return BuildingType::slurry_tank;
        } else if (tag_value == v_hangar) {
            return BuildingType::hangar;
        } else if (tag_value == v_stadium) {
            return BuildingType::stadium;
        } else if (tag_value == v_sports_hall) {
            return BuildingType::sports_hall;
        } else { // either because no direct mapping ("exotic") or because mapped as v_yes
            return BuildingType::yes;
        }
    }
    return std::nullopt;
}

BuildingClass ParseTagsForBuildingClass(const tags_t* tags) {
    auto building_type = ParseTagsForBuildingType(tags);
    if (building_type.has_value()) {
        BuildingType bt = building_type.value();
        if (bt == BuildingType::house or bt == BuildingType::detached or bt == BuildingType::residential) {
            return BuildingClass::residential;
        } else if (bt == BuildingType::bungalow or bt == BuildingType::static_caravan or bt == BuildingType::cabin or bt == BuildingType::hut) {
            return BuildingClass::residential_small;
        } else if (bt == BuildingType::apartments or bt == BuildingType::dormitory or bt == BuildingType::hotel) {
            return BuildingClass::apartments;
        } else if (bt == BuildingType::terrace) {
            return BuildingClass::terrace;
        } else if (bt == BuildingType::commercial or bt == BuildingType::office) {
            return BuildingClass::commercial;
        } else if (bt == BuildingType::retail) {
            return BuildingClass::retail;
        } else if (bt == BuildingType::industrial or bt == BuildingType::storage_tank) {
            return BuildingClass::industrial;
        } else if (bt == BuildingType::warehouse) {
            return BuildingClass::warehouse;
        } else if (bt == BuildingType::parking) {
            return BuildingClass::parking_house;
        } else if (bt == BuildingType::cathedral or bt == BuildingType::chapel or bt == BuildingType::church
                    or bt == BuildingType::mosque or bt == BuildingType::temple or bt == BuildingType::synagogue) {
            return BuildingClass::religion;
        } else if (bt == BuildingType::publicx or bt == BuildingType::civic or bt == BuildingType::school or bt == BuildingType::hospital) {
            return BuildingClass::publicx;
        } else if (bt == BuildingType::farm or bt == BuildingType::barn or bt == BuildingType::cowshed or bt == BuildingType::farm_auxiliary
                    or bt == BuildingType::greenhouse or bt == BuildingType::stable or bt == BuildingType::sty
                    or bt == BuildingType::riding_hall or bt == BuildingType::slurry_tank) {
            return BuildingClass::farm;
        } else if (bt == BuildingType::hangar) {
            return BuildingClass::airport;
        }
    }
    return BuildingClass ::undefined;
}

bool ParseIsGlasshouse(const tags_t* tags, const bool is_building_part) {
    auto building_key = is_building_part ? k_building_part : k_building;
    return ParseHasKeyValue(tags, building_key, v_glasshouse);
}

bool ParseIsGreenhouse(const tags_t* tags, const bool is_building_part) {
    auto building_key = is_building_part ? k_building_part : k_building;
    return ParseHasKeyValue(tags, building_key, v_greenhouse);
}

bool ParseIsChimney(const tags_t* tags) {
    return ParseHasKeyValue(tags, k_man_made, v_chimney);
}

bool ParseIsStorageTank(const tags_t* tags, const bool is_building_part) {
    auto building_key = is_building_part ? k_building_part : k_building;
    if (tags->contains(building_key)) {
        if (tags->at(building_key) == v_storage_tank or
            tags->at(building_key) == v_tank or
            tags->at(building_key) == v_oil_tank or
            tags->at(building_key) == v_fuel_storage_tank or
            tags->at(building_key) == v_digester) {
            return true;
        }
    } else if (tags->contains(k_man_made)) {
        if (tags->at(k_man_made) == v_storage_tank or
            tags->at(k_man_made) == v_tank or
            tags->at(k_man_made) == v_oil_tank or
            tags->at(k_man_made) == v_fuel_storage_tank or
            tags->at(k_man_made) == v_digester) {
            return true;
        }
    }
    return false;
}

bool ParseIsIndoor(const tags_t* tags)  {
    return ParseHasKeyNotValue(tags, k_indoor, v_no);
}

bool ParseIsUndergroundBuilding(const tags_t* tags) {
    if (ParseHasKeyValue(tags, k_location, v_underground)) {
        return true;
    }
    if (ParseHasKeyNotValue(tags, k_tunnel, v_no)) {
        return true;
    }
    // level see https://wiki.openstreetmap.org/wiki/Key:level
    // using double instead of int because there might be values like 0.5 etc.
    // there can be multivalues like "0;1"
    if (tags->contains(k_level)) {
        auto level = ParseMaxDouble(tags->at(k_level));
        if (level.has_value()) {
            if (level.value() < 0.) {
                // we make an extra check with building:levels
                // according to https://wiki.openstreetmap.org/wiki/Key:building:levels this should be
                // tagged otherwise, but you never know
                double building_levels {0.};
                if (tags->contains(k_building_levels)) {
                    auto my_building_levels = ParseDouble(tags->at(k_building_levels));
                    if (my_building_levels.has_value()) {
                        building_levels = my_building_levels.value();
                    }
                }
                if (building_levels + level.value() < 0.) { // the combination is still under ground
                    return true;
                }
            }
        }
    }
    return false;
}

bool ParseIsSmallBuildingDetail(const tags_t* tags, const bool is_building_part) {
    auto building_key = is_building_part ? k_building_part : k_building;
    if (tags->contains(building_key)) {
        if (tags->at(building_key) == v_garage or
            tags->at(building_key) == v_garages or
            tags->at(building_key) == v_carport or
            tags->at(building_key) == v_car_port or
            tags->at(building_key) == v_kiosk or
            tags->at(building_key) == v_toilets or
            tags->at(building_key) == v_service or
            tags->at(building_key) == v_shed or
            tags->at(building_key) == v_tree_house or
            tags->at(building_key) == v_roof or
            tags->at(building_key) == v_houseboat) {
            return true;
        }
    }
    return false;
}

bool ParseIsTooSmallBuildingTypeForRendering(const tags_t* tags, bool is_building_part) {
    auto building_key = is_building_part ? k_building_part : k_building;
    if (tags->contains(building_key)) {
        if (tags->at(building_key) == v_sty or
            tags->at(building_key) == v_slurry_tank or
            tags->at(building_key) == v_static_caravan) {
            return true;
        }
    }
    return false;
}
