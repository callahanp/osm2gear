#ifndef OSM2GEAR_OSMDATAREADER_H
#define OSM2GEAR_OSMDATAREADER_H

#include <map>
#include <vector>

#include "osm_types.h"
#include "../main/parameters.h"
#include "../main/utils.h"

class OSMDBDataReader {
private:
    const std::string db_connection_string_;
    const Boundary boundary_;
    const Boundary extended_boundary_;
    bool use_extended_boundary_ = false;

    std::string ConstructIntersectBBoxQuery(bool) const;
    OSMReadResult FetchWays(const std::vector<std::string> &required, bool is_key_values) const;
    OSMReadResult FetchNodesIsolated(const std::vector<std::string> &required, bool is_key_values) const;
    node_map_t FetchNodesForWay(const std::vector<std::string>& required_keys,
                                const std::vector<std::string>& required_key_values) const;
    way_map_t FetchWayData(const std::vector<std::string>& required_keys,
                           const std::vector<std::string>& required_key_values) const;

    OSMReadRelationsResult FetchOSMRelationsKeys(const std::string &first_part, const std::string &relation_debug_string,
                                                 bool handle_empty_role_as_outer) const;

public:
    OSMDBDataReader(std::string, Boundary, Boundary);
    OSMReadResult FetchWaysFromKeyValues(const std::vector<std::string> &) const;
    OSMReadResult FetchWaysFromKeys(const std::vector<std::string> &) const;

    OSMReadResult FetchNodesIsolatedFromKeyValues(const std::vector<std::string> &) const;
    OSMReadResult FetchNodesIsolatedFromKeys(const std::vector<std::string> &) const;

    OSMReadRelationsResult FetchRelationsPlaces() const;

    OSMReadRelationsResult FetchRelationsAerodrome() const;

    OSMReadRelationsResult FetchRelationsLanduse() const;

    OSMReadRelationsResult FetchRelationsBuildings() const;

    OSMReadRelationsResult FetchRelationsRiverbanks() const;

    void SetUseExtendedBoundary(bool);

    static std::string CreateKeyValuePair(const std::string &key, const std::string &value);
};
#endif //OSM2GEAR_OSMDATAREADER_H
