#include "data_reader.h"

#include <cassert>
#include <memory>
#include <utility>

#include "boost/log/trivial.hpp"
#include <pqxx/pqxx>

#include "osm_types.h"


constexpr char table_alias_ways[] = "w";
constexpr char table_alias_nodes[] = "n";

OSMDBDataReader::OSMDBDataReader(std::string db_connection_string,
                                 const Boundary boundary, const Boundary extended_boundary): db_connection_string_ {std::move(db_connection_string)},
                                                                                             boundary_ {boundary},
                                                                                             extended_boundary_ {extended_boundary}{}

void OSMDBDataReader::SetUseExtendedBoundary(bool use_extended_boundary) {
    use_extended_boundary_ = use_extended_boundary;
}

/*
def _construct_intersect_bbox_query(is_way: bool = True) -> str:
"""Constructs the part of a sql where clause, which constrains to bounding box."""
*/
std::string OSMDBDataReader::ConstructIntersectBBoxQuery(bool is_way) const {
    std::string query = "ST_Intersects(";
    if (is_way) {
        query.append("w.bbox");
    } else {
        query.append("n.geom");
    }
    query.append(", ST_SetSRID(ST_MakeBox2D(ST_Point(");
    query.append(std::to_string(use_extended_boundary_ ? extended_boundary_.west : boundary_.west));
    query.append(", ");
    query.append(std::to_string(use_extended_boundary_ ? extended_boundary_.south : boundary_.south));
    query.append("), ST_Point(");
    query.append(std::to_string(use_extended_boundary_ ? extended_boundary_.east : boundary_.east));
    query.append(", ");
    query.append(std::to_string(use_extended_boundary_ ? extended_boundary_.north : boundary_.north));
    query.append(")), 4326))");
    return query;
}

/*
def _construct_tags_query(req_tag_keys: List[str], req_tag_key_values: List[str], table_alias: str = "w") -> str:
    """Constructs the part of a sql WHERE clause, which constrains the result based on required tag keys.
    In req_tag_keys at least one of the key needs to be present in the tags of a given record.
    In req_tag_key_values at least one key/value pair must be present (e.g. 'railway=>platform') - the key
    must be separated without blanks from the value by a '=>'."""
*/
std::string ConstructTagsQueryKeys(const std::vector<std::string>& required_keys,
                                   const std::string& table_alias) {
    std::string query {};
    if (required_keys.size() == 1) {
        query.append(table_alias).append(".tags ? '").append(required_keys[0]).append("'");
    } else if (required_keys.size() > 1) {
        bool isFirst = true;
        query.append(table_alias).append(".tags ?| ARRAY[");
        for (const auto & key : required_keys) {
            if (isFirst) {
                isFirst = false;
            } else {
                query.append(", ");
            }
            query.append("'").append(key).append("'");
        }
        query.append("]");
    }
    return query;
}

std::string ConstructTagsQueryKeyValues(const std::vector<std::string>& required_key_values,
                                        const std::string& table_alias) {
    std::string query {};
    if (required_key_values.size() == 1) {
        query.append(table_alias).append(".tags @> '").append(required_key_values[0]).append("'");
    } else if (required_key_values.size() > 1) {
        bool isFirst = true;
        query.append("(");
        for (const auto & keyValue : required_key_values) {
            if (isFirst) {
                isFirst = false;
            } else {
                query.append(" OR ");
            }
            query.append(table_alias).append(".tags @> '").append(keyValue).append("'");
        }
        query.append(")");
    }
    return query;
}

std::string ConstructTagsQuery(const std::vector<std::string>& required_keys,
                               const std::vector<std::string>& required_key_values,
                               const char table_alias[] = table_alias_ways) {
    assert(required_keys.size() + required_key_values.size() > 0);
    std::string query {};
    if (!required_keys.empty()) {
        query.append(ConstructTagsQueryKeys(required_keys, table_alias));
    }
    if (!required_keys.empty() && !required_key_values.empty()) {
        query.append(" AND ");
    }
    if (!required_key_values.empty()) {
        query.append(ConstructTagsQueryKeyValues(required_key_values, table_alias));
    }
    return query;
}

/*! \brief Parses the content of a string representation of a PostGIS hstore content for tags.
 * Returns a dict of key value pairs as string.
 * "\"iata\"=>\"EDI\", \"icao\"=>\"EGPH\", \"name\"=>\"Edinburgh Airport\""
*/
std::map<std::string, std::string> ParseHstoreTags(std::string &tags_string) {
    std::map<std::string, std::string> tags {};
    std::stringstream ss(tags_string);

    std::string delimiter = "=>";
    while(ss.good()) {
        std::string part;
        getline(ss, part, ' ' ); //-> part looks like \"iata\"=>\"EDI\",
        part = part.substr(0, part.find(','));  //remove the last comma - might not be there
        std::string::size_type n;
        while (true) {
            n = part.find('"');
            if (n == std::string::npos)
                break;
            else
                part = part.erase(n, 1);
        }
        n = part.find(delimiter);
        if (n != std::string::npos) {
            std::string my_key = part.substr(0, n);
            std::string my_value = part.substr(n + delimiter.length(), part.length());
            if (!my_key.empty() && !my_value.empty())
                tags[my_key] = my_value;
        } //else we do nothing, because not a valid value
    }
    return tags;
}

// the input string looks like "{111,2345,234356,2323}"
std::vector<osm_id_t> ParseRefsFromColumnArray(std::string &string_array) {
    auto brackets_removed = string_array.substr(1, string_array.length() - 2);
    std::stringstream ss(brackets_removed);
    std::vector<osm_id_t> refs;

    while(ss.good()) {
        std::string substr;
        getline( ss, substr, ',' );

        refs.push_back(std::stoll(substr));
    }
    return refs;
}

/*
def _fetch_db_way_data(req_way_keys: List[str], req_way_key_values: List[str], db_connection) -> Dict[int, Way]:
"""Fetches Way objects out of database given required tag keys and boundary in parameters."""
*/
way_map_t OSMDBDataReader::FetchWayData(const std::vector<std::string>& required_keys,
                                         const std::vector<std::string>& required_key_values) const {
    pqxx::connection c(this->db_connection_string_);

    std::string query = "SELECT id, tags, nodes"
                        " FROM ways AS w"
                        " WHERE ";
    query.append(ConstructTagsQuery(required_keys, required_key_values));
    query.append(" AND ");
    query.append(ConstructIntersectBBoxQuery(true));
    query.append(";");

    BOOST_LOG_TRIVIAL(debug) << "FetchWayData SQL: " << query;

    pqxx::nontransaction n(c);
    pqxx::result r = n.exec(query);

    way_map_t ways_dict;
    for (pqxx::result::const_iterator i = r.begin(); i != r.end(); ++i) {
        auto osm_id = i[0].as<osm_id_t>();
        auto tags_string = to_string(i[1]);
        auto nodes_string = to_string(i[2]);
        auto refs = ParseRefsFromColumnArray(nodes_string);
        auto tags = ParseHstoreTags(tags_string);
        std::unique_ptr<Way> way = std::make_unique<Way>(osm_id, refs, tags);
        ways_dict[osm_id] = std::move(way);
    }
    c.disconnect();
    return ways_dict;
}

/*
def _fetch_db_nodes_for_way(req_way_keys: List[str], req_way_key_values: List[str], db_connection) -> Dict[int, Node]:
    """Fetches Node objects for ways out of database given same constraints as for Way.
    Constraints for way: see fetch_db_way_data"""

 */
node_map_t OSMDBDataReader::FetchNodesForWay(const std::vector<std::string>& required_keys,
                               const std::vector<std::string>& required_key_values) const {
    pqxx::connection c(this->db_connection_string_);

    std::string query = "SELECT n.id, ST_X(n.geom) as lon, ST_Y(n.geom) as lat"
                        " FROM ways AS w, way_nodes AS r, nodes AS n"
                        " WHERE"
                        " r.way_id = w.id"
                        " AND r.node_id = n.id"
                        " AND ";
    query.append(ConstructTagsQuery(required_keys, required_key_values));
    query.append(" AND ");
    query.append(ConstructIntersectBBoxQuery(true));
    query.append(";");

    BOOST_LOG_TRIVIAL(debug) << "FetchNodesForWay SQL: " << query;

    pqxx::nontransaction n(c);
    pqxx::result r = n.exec(query);

    node_map_t nodes_dict;
    for (pqxx::result::const_iterator i = r.begin(); i != r.end(); ++i) {
        auto osm_id = i[0].as<osm_id_t>();
        LonLat lon_lat {i[1].as<double>(), i[2].as<double>()};
        std::unique_ptr<Node> node = std::make_unique<Node>(osm_id, lon_lat);
        nodes_dict[osm_id] = std::move(node);
    }
    c.disconnect();
    return nodes_dict;
}

OSMReadResult OSMDBDataReader::FetchWays(const std::vector<std::string> &required, bool is_key_values) const {
    std::vector<std::string> empty {};
    OSMReadResult readResult {};
    if (is_key_values) {
        readResult.ways_dict = FetchWayData(empty, required);
        readResult.nodes_dict = FetchNodesForWay(empty, required);
    }
    else {
        readResult.ways_dict = FetchWayData(required, empty);
        readResult.nodes_dict = FetchNodesForWay(required, empty);
    }
    return readResult;
}

OSMReadResult OSMDBDataReader::FetchNodesIsolated(const std::vector<std::string> &required, bool is_key_values) const {
    std::vector<std::string> empty {};
    OSMReadResult readResult {};
    pqxx::connection c(this->db_connection_string_);

    std::string query = "SELECT n.id, ST_X(n.geom) as lon, ST_Y(n.geom) as lat, n.tags"
                        " FROM nodes AS n"
                        " WHERE ";
    if (is_key_values) {
        query.append(ConstructTagsQuery(empty, required, table_alias_nodes));
    } else {
        query.append(ConstructTagsQuery(required, empty, table_alias_nodes));
    }
    query.append(" AND ");
    query.append(ConstructIntersectBBoxQuery(false));
    query.append(";");

    BOOST_LOG_TRIVIAL(debug) << "FetchNodesIsolated SQL: " << query;

    pqxx::nontransaction n(c);
    pqxx::result r = n.exec(query);

    node_map_t nodes_dict;
    for (pqxx::result::const_iterator i = r.begin(); i != r.end(); ++i) {
        auto osm_id = i[0].as<osm_id_t>();
        LonLat lon_lat {i[1].as<double>(), i[2].as<double>()};
        auto tags_string = to_string(i[3]);
        auto tags = ParseHstoreTags(tags_string);
        std::unique_ptr<Node> node = std::make_unique<Node>(osm_id, lon_lat, tags);
        nodes_dict[osm_id] = std::move(node);
    }
    c.disconnect();

    readResult.nodes_dict = std::move(nodes_dict);

    return readResult;
}

OSMReadResult OSMDBDataReader::FetchWaysFromKeyValues(const std::vector<std::string> &required_key_values) const {
    return FetchWays(required_key_values, true);
}

OSMReadResult OSMDBDataReader::FetchWaysFromKeys(const std::vector<std::string> &required_keys) const {
    return FetchWays(required_keys, false);
}

OSMReadResult OSMDBDataReader::FetchNodesIsolatedFromKeyValues(const std::vector<std::string> &required_key_values) const {
    return FetchNodesIsolated(required_key_values, true);
}

OSMReadResult OSMDBDataReader::FetchNodesIsolatedFromKeys(const std::vector<std::string> &required_keys) const {
    return FetchNodesIsolated(required_keys, false);
}

std::string OSMDBDataReader::CreateKeyValuePair(const std::string &key, const std::string &value) {
    return key + "=>" + value;
}

/*! Creates an OSMReadResult with relation data based on required keys.
 */
OSMReadRelationsResult OSMDBDataReader::FetchOSMRelationsKeys(const std::string &first_part, const std::string &relation_debug_string,
                                                              bool handle_empty_role_as_outer) const {
    OSMReadRelationsResult read_result {};
    pqxx::connection c(this->db_connection_string_);

    std::string sub_query = first_part + " AND r.id = rm.relation_id"
                                         " AND rm.member_type = 'W'"
                                         " AND rm.member_id = w.id"
                                         " AND ";
    sub_query += ConstructIntersectBBoxQuery(true);

    // === Relations and members and ways
    // Getting related way data might add a bit of volume, but reduces the number of queries and might be seldom that
    // same way is in different relations for buildings.
    std::string query = "SELECT r.id, r.tags, rm.member_id, rm.member_role, w.nodes, w.tags"
                        " FROM relations AS r, relation_members AS rm, ways AS w"
                        " WHERE";
    query += sub_query;
    query += " ORDER BY rm.relation_id, rm.sequence_id";
    query += ";";

    BOOST_LOG_TRIVIAL(debug) << "FetchOSMRelationsKeys SQL relations for" << relation_debug_string << ": " << query;

    pqxx::nontransaction n(c);
    pqxx::result r = n.exec(query);

    std::map<osm_id_t, std::unique_ptr<Relation>> relations_dict {};
    shared_way_map_t rel_ways_dict {};

    for (pqxx::result::const_iterator i = r.begin(); i != r.end(); ++i) {
        std::unique_ptr<Relation> relation;
        auto relation_id = i[0].as<osm_id_t>();

        auto member_id = i[2].as<osm_id_t>();
        auto role = i[3].as<std::string>();
        auto member_role = ParseMemberRole(role, handle_empty_role_as_outer);
        std::unique_ptr<Member> member = std::make_unique<Member>(member_id, MemberType::way, member_role);

        if (relations_dict.contains(relation_id)) {
            relations_dict.at(relation_id)->AddMember(std::move(member));
        } else {
            auto hstore_string = i[1].as<std::string>();
            auto tags = ParseHstoreTags(hstore_string);
            relation = std::make_unique<Relation>(relation_id, tags);
            relation->AddMember(std::move(member));
            relations_dict[relation_id] = std::move(relation);
        }
        if (!rel_ways_dict.contains(member_id)) {
            auto hstore_string = i[5].as<std::string>();
            auto tags = ParseHstoreTags(hstore_string);
            auto refs_string = to_string(i[4]);
            auto refs = ParseRefsFromColumnArray(refs_string);
            std::shared_ptr<Way> way = std::make_shared<Way>(member_id, refs, tags);
            rel_ways_dict[member_id] = std::move(way);
        }
    }

    // === Nodes for the ways
    query = "SELECT n.id, ST_X(n.geom) as lon, ST_Y(n.geom) as lat"
            " FROM relations AS r, relation_members AS rm, ways AS w, way_nodes AS wn, nodes AS n"
            " WHERE";
    query += sub_query;
    query += " AND wn.way_id = w.id";
    query += " AND wn.node_id = n.id";
    query += ";";

    BOOST_LOG_TRIVIAL(debug) << "FetchOSMRelationsKeys SQL nodes: " << query;

    r = n.exec(query);

    std::map<osm_id_t, std::unique_ptr<Node>> rel_nodes_dict {};

    for (pqxx::result::const_iterator i = r.begin(); i != r.end(); ++i) {
        auto osm_id = i[0].as<osm_id_t>();
        LonLat lon_lat {i[1].as<double>(), i[2].as<double>()};
        std::unique_ptr<Node> node = std::make_unique<Node>(osm_id, lon_lat);
        rel_nodes_dict[osm_id] = std::move(node);
    }
    c.disconnect();

    read_result.relations_dict = std::move(relations_dict);
    read_result.rel_ways_dict = std::move(rel_ways_dict);
    read_result.rel_nodes_dict = std::move(rel_nodes_dict);
    return read_result;
}

OSMReadRelationsResult OSMDBDataReader::FetchRelationsPlaces() const {
    std::string first_part = "((r.tags @> '" + CreateKeyValuePair(k_type, v_multipolygon) + "' OR r.tags @> '" + CreateKeyValuePair(k_type, v_boundary) + "')";
    first_part += " AND (r.tags @> '" + CreateKeyValuePair(k_place, v_city) + "' OR r.tags @> '" + CreateKeyValuePair(k_place, v_town) + "'))";
    return FetchOSMRelationsKeys(first_part, "places", true);
}

OSMReadRelationsResult OSMDBDataReader::FetchRelationsAerodrome() const {
    std::string first_part = "(r.tags @> 'type=>multipolygon'";
    first_part += " AND r.tags @> '" + CreateKeyValuePair(k_aeroway, v_aerodrome) + "')";
    return FetchOSMRelationsKeys(first_part, "aerodrome", true);
}

OSMReadRelationsResult OSMDBDataReader::FetchRelationsLanduse() const {
    std::string first_part = "(r.tags @> 'type=>multipolygon'";
    first_part += " AND r.tags ? '";
    first_part += k_landuse;
    first_part += "')";
    return FetchOSMRelationsKeys(first_part, "landuse", true);
}

OSMReadRelationsResult OSMDBDataReader::FetchRelationsBuildings() const {
    std::string first_part = "((r.tags @> 'type=>multipolygon'";
    first_part += " AND " + ConstructTagsQuery({k_building, k_building_part}, {}, "r");
    first_part += ") OR r.tags @> 'type=>building')";
    return FetchOSMRelationsKeys(first_part, "buildings", true);
}

OSMReadRelationsResult OSMDBDataReader::FetchRelationsRiverbanks() const {
    std::string first_part = "(r.tags @> 'type=>multipolygon'";
    first_part += " AND r.tags @> '" + CreateKeyValuePair(k_waterway, v_riverbank) + "')";
    return FetchOSMRelationsKeys(first_part, "riverbanks", true);
}
