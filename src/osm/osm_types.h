#ifndef OSM2GEAR_OSMTYPES_HXX
#define OSM2GEAR_OSMTYPES_HXX

#include <exception>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include <geos/geom/Geometry.h>
#include <geos/geom/LineString.h>
#include <geos/geom/LinearRing.h>
#include <geos/geom/Point.h>
#include <geos/geom/Polygon.h>
#include "simgear/math/SGMath.hxx"

#include "../main/utils.h"
#include "../main/enumerations.h"
#include "constants.h"

using osm_id_t = int64_t;
using tags_t = std::map<std::string, std::string>;
using refs_t = std::vector<osm_id_t>;

class IdCreator {
private:
    IdCreator() = default;
    osm_id_t counter_ {};

public:
    static IdCreator &Get() {
        static IdCreator instance;
        return instance;
    }
    /*! \brief Creates a unique (within a tile/computing context) number simulating a osm_id
     *
     * The number is negative to make sure it does not by chance correspond to a real one.
     *
     * @return a negative number differentiating the number range for different features
     */
    osm_id_t GetNextPseudoOSMId(OSMFeatureType);

    IdCreator(IdCreator const &) = delete;

    IdCreator(IdCreator &&) = delete;

    IdCreator operator=(IdCreator const &) = delete;

    IdCreator operator=(IdCreator &&) = delete;
};

enum InvalidGeometryFromOSMType {
    nodeNotFound,
    notEnoughPoints,
    emptyGeometry,
    complexGeometry,  // not simple i.e. self intersecting
    invalidGeometry
};

const std::map<InvalidGeometryFromOSMType,std::string> mapper {
    {InvalidGeometryFromOSMType::nodeNotFound, "nodeNotFound"},
    {InvalidGeometryFromOSMType::notEnoughPoints, "notEnoughPoints"},
    {InvalidGeometryFromOSMType::emptyGeometry,"emptyGeometry"},
    {InvalidGeometryFromOSMType::complexGeometry,"complexGeometry"},
    {InvalidGeometryFromOSMType::invalidGeometry, "invalidGeometry"}
    };

class InvalidGeometryFromOSM : public std::runtime_error {
public:
    explicit InvalidGeometryFromOSM(osm_id_t, InvalidGeometryFromOSMType);
};

class Element {
protected:
    const osm_id_t osm_id_;
    tags_t tags_;

public:
    explicit Element(osm_id_t osmId);
    [[nodiscard]]
    osm_id_t GetOSMId() const;
    [[nodiscard]]
    const tags_t* GetTags() const;

    void AddTag(const std::string &key, std::string &value);
};

tags_t CombineTags(const tags_t*, const tags_t*);

class Node : public Element {
private:
    LonLat lon_lat_;
public:
    Node(osm_id_t, LonLat);
    Node(osm_id_t, LonLat, tags_t &);
    [[nodiscard]]
    std::unique_ptr<geos::geom::Point> PointFromOSMNode(const TileHandler &) const;
    [[nodiscard]]
    LonLat GetLonLat() const;
    [[nodiscard]]
    SGGeod GetSGGeod() const;
};

using node_map_unique_ptr_t = std::map<osm_id_t, std::unique_ptr<Node>>;
using node_map_t = node_map_unique_ptr_t;

class Way : public Element {
private:
    refs_t refs_;
public:
    Way(osm_id_t , refs_t &, tags_t &);
    [[nodiscard]]
    bool CheckAtLeastOneNodeInTile(const node_map_t &nodes_map, const TileHandler &tile_handler) const;
    [[nodiscard]]
    std::unique_ptr<geos::geom::LinearRing> LinearRingFromOSMWay(const node_map_t&, const TileHandler &) const;
    [[nodiscard]]
    std::unique_ptr<geos::geom::Polygon> PolygonFromOSMWay(const node_map_t&, const TileHandler &) const;
    [[nodiscard]]
    const refs_t* GetRefs() const;
    static std::unique_ptr<geos::geom::LinearRing> LinearRingFromRefs(const osm_id_t &, const refs_t *, const node_map_t &, const TileHandler &);
    static std::unique_ptr<geos::geom::LineString> LineStringFromRefs(const osm_id_t &, const refs_t *, const node_map_t &, const TileHandler &);
    [[nodiscard]]
    bool RefsAreClosed() const {
        return (refs_.front() == refs_.back());
    }
};

/*! \brief Create closed ways from multiple not closed ways where possible.
 * See http://wiki.openstreetmap.org/wiki/Relation:multipolygon.
 * If parts of ways cannot be used, they just get disregarded.
 * The new Ways get the osm_id from a way reused and all tags removed.
*
* @param way_parts
* @param transfer_tags_from_original_ways set to false if the tags are subsequently not used to optimise speed/space
* @return a list of Way objects combined from the loose input way parts.
 */
std::vector<std::unique_ptr<Way>> ClosedWaysFromMultipleWays(const std::vector<std::shared_ptr<Way>> &way_parts,
                                                             bool transfer_tags_from_original_ways);

using way_map_unique_ptr_t = std::map<osm_id_t, std::unique_ptr<Way>>;
using way_map_t = way_map_unique_ptr_t;

using way_map_shared_ptr_t = std::map<osm_id_t , std::shared_ptr<Way>>;
using shared_way_map_t = way_map_shared_ptr_t;

enum class MemberType {
    way,
};

enum class MemberRole {
    inner,
    outer,
    outline,
    undefined,
};

MemberRole ParseMemberRole(const std::string &role, bool handle_empty_role_as_outer);

class Member {
private:
    osm_id_t ref_;
    MemberType type_;
    MemberRole role_;
public:
    Member(osm_id_t, MemberType, MemberRole);
    [[nodiscard]]
    osm_id_t GetRef() const {
        return ref_;
    }
    [[nodiscard]]
    MemberType GetType() const {
        return type_;
    }
    [[nodiscard]]
    MemberRole GetRole() const {
        return role_;
    }
};

struct OSMReadRelationsResult; //forward declare

class Relation : public Element {
private:
    std::vector<std::unique_ptr<Member>> members_ {};
public:
    Relation(osm_id_t, tags_t &tags);
    void AddMember(std::unique_ptr<Member> member) {
        members_.push_back(std::move(member));
    }
    std::vector<std::unique_ptr<Member>>* GetMembers() {
        return &members_;
    }
    /*! \brief Based on a specific member role returns the found polygons in the Relation.
     */
    [[nodiscard]]
    std::vector<std::unique_ptr<geos::geom::Polygon>> PolygonsFromMembers(const OSMReadRelationsResult &rel_result,
                                                                          const TileHandler &tile_handler,
                                                                          MemberRole member_role,
                                                                          bool check_against_tile_boundary = false) const;
    /*! \brief Based on outer and inner member roles returns the found polygons in the Relation.
     * The inner polygons are set inside outer polygons - or discarded.
     *
     * Stuff is done a bit complicated here, because basically we would like to throw all inner and outer
     * members directly at the GEOS polygonizer. However, that does not work out:
     * E.g. https://www.openstreetmap.org/relation/10811363 has problems in poligonizer,
     * which cannot be caught by tyr/catch.
     * osm2gear: /home/vanosten/develop_vcs/geos-3.11.1/include/geos/index/strtree/Interval.h:32:
     * geos::index::strtree::Interval::Interval(double, double): Assertion `imin <= imax' failed
     * Process finished with exit code 134 (interrupted by signal 6: SIGABRT)
     */
    [[nodiscard]]
    std::vector<std::unique_ptr<geos::geom::Polygon>> PolygonsFromAllMembers(const OSMReadRelationsResult &rel_result,
                                                                             const TileHandler &tile_handler,
                                                                             bool check_against_tile_boundary) const;
};

using relation_map_unique_ptr_t = std::map<osm_id_t, std::unique_ptr<Relation>>;
using relation_map_t = relation_map_unique_ptr_t;

struct OSMReadResult {
    node_map_t nodes_dict;
    way_map_t ways_dict;
};

struct OSMReadRelationsResult {
    relation_map_t relations_dict;
    node_map_t rel_nodes_dict;
    shared_way_map_t rel_ways_dict;  //because the same way can be shared among several relations
};

std::optional<long> ParseLong(const std::string &);

std::optional<int> ParseInt(const std::string &);

std::optional<double> ParseDouble(const std::string &);

/*! \brief Finds the largest double in an OSM value which has multi values.
 *
 * @return std::nullopt if there was no parsable value
 */
std::optional<double> ParseMaxDouble(const std::string &);

int ParseLanes(const tags_t *, int = 1, bool = false);

int ParseTracks(const tags_t *);

bool ParseIsTunnel(const tags_t *);

bool ParseHasEmbankmentOrCutting(const tags_t *);

bool ParseIsOneWay(const tags_t *, bool);

bool ParseIsRoundabout(const tags_t *);

bool ParseIsServiceSpur(const tags_t *);

std::optional<HighwayType> ParseTagsForHighwayType(const tags_t *);

std::optional<PlaceType> ParseTagsForPlaceType(const tags_t*);

std::optional<WaterwayType> ParseTagsForWaterwayType(const tags_t *);

std::optional<RailwayLineType> ParseTagsForRailwayLineType(const tags_t *);

std::optional<BuildingZoneType> ParseTagsForBuildingZoneType(const tags_t *);

bool ParseIsYesBuilding(const tags_t *);

std::optional<BuildingType> ParseTagsForBuildingType(const tags_t *);

BuildingClass ParseTagsForBuildingClass(const tags_t *);

bool ParseIsGlasshouse(const tags_t* tags, bool is_building_part);

bool ParseIsGreenhouse(const tags_t* tags, bool is_building_part);

/** \brief Whether this is a chimney and processed as pylons.
 * @param tags
 * @return
 */
bool ParseIsChimney(const tags_t* tags);

/** \brief Whether this is a storage tank (or similar) and processed as pylons.
 * @param tags
 * @param is_building_part
 * @return
 */
bool ParseIsStorageTank(const tags_t* tags, bool is_building_part);

bool ParseIsIndoor(const tags_t* tags);

bool ParseIsUndergroundBuilding(const tags_t* tags);

/*! \brief Whether this is a very small building not used for land-use processing.
 * And therefor not used in rendering neither.
 * @param tags
 * @param is_building_part
 * @return
 */
bool ParseIsSmallBuildingDetail(const tags_t* tags, bool is_building_part);

/*! \brief Whether this is a very small building not used for rendering processing.
 * But it was used for land-use processing.
 * @param tags
 * @param is_building_part
 * @return
 */
bool ParseIsTooSmallBuildingTypeForRendering(const tags_t* tags, bool is_building_part);

#endif //OSM2GEAR_OSMTYPES_HXX
