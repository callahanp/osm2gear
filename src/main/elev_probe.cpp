#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>

#include "elev_probe.h"
#include "parameters.h"


ElevationProbingException::ElevationProbingException(std::string message) : message_(std::move(message)) {}

FgElevInstance::FgElevInstance(const LonLat &lon_lat_global) {
    std::vector<std::string> args;
    // solidness is not used, therefore no need for args.emplace_back("--print-solidness");
    args.emplace_back("--expire");
    args.emplace_back(FG_ELEV_EXPIRE);
    args.emplace_back("--fg-scenery");
    args.emplace_back(Parameters::Get().PATH_TO_SCENERY);
    if (Parameters::Get().USE_WS30_SCENERY) {
        args.emplace_back("--use-vpb");
        args.emplace_back("--tile-file");
        std::string tile_file{"ws_"};
        auto dir_name = ConstructPathToFiles(Parameters::Get().PATH_TO_SCENERY,
                                             "vpb", lon_lat_global);
        auto f_name = dir_name.filename().string();
        tile_file.append(f_name);
        tile_file.append(".osgb");
        dir_name.append(tile_file);
        args.emplace_back(dir_name.string());
    }

    from_fg_elev_output_ = std::make_shared<boost::process::ipstream>();
    to_fg_elev_input_ = std::make_shared<boost::process::opstream>();
    fg_elev_ = std::make_unique<boost::process::child>(
        boost::process::exe = GetEnv("O2C_PATH_TO_FG_ELEV"),
        boost::process::args = args,
        boost::process::std_out > *from_fg_elev_output_,
        boost::process::std_in < *to_fg_elev_input_
    );
}

float FgElevInstance::QueryElevation(const LonLat &lon_lat_global) {
    records_++;
    *to_fg_elev_input_ << records_ << " " << lon_lat_global.lon << " " << lon_lat_global.lat << std::endl;

    std::string line;
    float elevation;
    uint lines_read {0};
    while (true) {
        if (lines_read++ > 20) {
            throw ElevationProbingException("Probing loops infinitive");
        }
        std::getline(*from_fg_elev_output_, line);
        std::vector<std::string> v;

        // these happen at start-up and can just be ignored
        if (line.starts_with("Now checking") or
            line.starts_with("osg::Registry::addImageProcessor") or
            line.starts_with("Loaded plug-in") or line.empty()) {
            continue;
        }
        boost::split(v, line, boost::is_any_of(" "));
        if (v.size() > 1) {
            if (v[1] == FG_ELEV_NOT_FOUND) {
                elevation = FG_ELEV_NO_ELEV;
            } else {
                try {
                    elevation = std::stof(v[1]);
                } catch (std::invalid_argument const &ex) {
                    // we cannot do anything about it
                    elevation = FG_ELEV_NO_ELEV;
                }
            }
            return elevation;
        }
    }
}

FgElevInstance::~FgElevInstance() {
    fg_elev_->terminate();
}

ElevationProber::ElevationProber(const TileHandler &tile_handler): tile_handler_ {tile_handler}{}

float ElevationProber::Probe(const LonLat &lon_lat, bool is_global) {
    LonLat lon_lat_global = lon_lat;
    if (not is_global) {
        lon_lat_global = tile_handler_.ToGlobal(lon_lat);
    }
    auto location_dir_name = LocationDirectoryName(lon_lat_global);
    if (Parameters::Get().USE_WS30_SCENERY) {
        if (not fg_elev_instances_.contains(location_dir_name)) {
            fg_elev_instances_[location_dir_name] =
                std::make_unique<FgElevInstance>(lon_lat_global);
        }
        return fg_elev_instances_.at(location_dir_name).get()->QueryElevation(lon_lat_global);
    } else {
        if (fg_elev_instances_.empty()) {
            fg_elev_instances_[location_dir_name] =
                std::make_unique<FgElevInstance>(lon_lat_global);
        }
        return fg_elev_instances_.begin()->second->QueryElevation(lon_lat_global);
    }
}
