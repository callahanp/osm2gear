#ifndef OSM2GEAR_LANDUSE_PROCESSOR_H
#define OSM2GEAR_LANDUSE_PROCESSOR_H

#include "utils.h"
#include "../osm/data_reader.h"

void ProcessLandUse(const TileHandler &tile_handler, OSMDBDataReader &osm_reader,
                    const bool &create_trees);

#endif //OSM2GEAR_LANDUSE_PROCESSOR_H
