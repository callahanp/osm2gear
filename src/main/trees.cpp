#include "trees.h"

#include <iomanip>
#include <random>
#include <sstream>
#include <vector>

#include "boost/log/trivial.hpp"

#include <geos/geom/CoordinateSequence.h>
#include <geos/geom/CoordinateSequenceFactory.h>
#include <geos/geom/GeometryFactory.h>
#include <geos/linearref/LengthIndexedLine.h>

#include "osgDB/Options"

#include <simgear/scene/util/SGImageUtils.hxx>

#include "buildings.h"
#include "plotting.h"
#include "stg_io.h"
#include "elev_probe.h"


void Tree::ParseTags(const tags_t* tags) {
    if (tags->contains(k_denotation)) {  //significant trees should be treated as such
        type_ = TreeType::significant_trees;
    }
}

Tree::Tree(LonLat lon_lat, const tags_t* tags, TreeOrigin origin, const TileHandler &tile_handler,
           const TreeType tree_type): origin_ {origin}, type_ {tree_type} {
    coord_ = tile_handler.ToLocal(lon_lat);
    ParseTags(tags);
}

Tree::Tree(const geos::geom::Coordinate &coordinate, const TreeOrigin origin,
           const TreeType tree_type): origin_ {origin}, type_ {tree_type} {
    coord_ = coordinate;
}

/*! \brief Extend the existing list of trees with new trees if the new trees have a minimal dist from the existing ones.
 *
 * @param potential_trees
 * @param existing_trees
 */
void ExtendTreesIfDistanceOk(std::vector<std::unique_ptr<Tree>> &potential_trees,
                             std::vector<std::unique_ptr<Tree>> &existing_trees) {
    std::vector<std::unique_ptr<Tree>> to_be_extended_trees {};
    for (auto &pot : potential_trees) {
        bool is_good = true;
        for (auto &ex : existing_trees) {
            if (CalcDistanceLocal(pot->GetX(), pot->GetY(), ex->GetX(), ex->GetY()) < Parameters::Get().TREES_DIST_MINIMAL) {
                is_good = false;
                break;
            }
        }
        if (is_good) {
            to_be_extended_trees.push_back(std::move(pot));
        }
    }
    //we first expand at the end to be able to reserve a good amount of space and in order not to compare
    //with the distance of newly added trees
    existing_trees.reserve(existing_trees.size() + to_be_extended_trees.size());
    for (auto &tree : to_be_extended_trees) {
        existing_trees.push_back(std::move(tree));
    }
}

/*! \brief Place trees based on manually mapped single trees in OSM.
 *
 * @param tile_handler
 * @param osm_reader
 * @return
 */
std::vector<std::unique_ptr<Tree>> ProcessOSMTreeNodes(const TileHandler &tile_handler, const OSMDBDataReader &osm_reader) {
    std::vector<std::unique_ptr<Tree>> trees {};
    std::vector<std::string> tree_kv {};
    tree_kv.push_back(OSMDBDataReader::CreateKeyValuePair(k_natural, v_tree));
    OSMReadResult result = osm_reader.FetchNodesIsolatedFromKeyValues(tree_kv);
    for (auto &it : result.nodes_dict) {
        trees.push_back(std::make_unique<Tree>(it.second->GetLonLat(), it.second->GetTags(), TreeOrigin::mapped, tile_handler,
                                               TreeType::significant_trees));
    }
    return trees;
}

/*! \brief Trees in a row as mapped in OSM (natural=tree_row).
 *
 * Cf. https://wiki.openstreetmap.org/wiki/Tag:natural%3Dtree_row - the single trees do not have to be mapped.
 * Therefore, we use a heuristic in the code to test whether the trees probably were mapped or not.
 *
 *
 * @param trees gets extended with additional trees
 * @param tile_handler
 * @param osm_reader
 */
void ProcessOSMTreeRows(std::vector<std::unique_ptr<Tree>> &existing_trees,
                        const TileHandler &tile_handler, const OSMDBDataReader &osm_reader) {
    std::vector<std::string> tree_row_kv {};
    tree_row_kv.push_back(OSMDBDataReader::CreateKeyValuePair(k_natural, v_tree_row));
    OSMReadResult result = osm_reader.FetchWaysFromKeyValues(tree_row_kv);

    std::vector<std::unique_ptr<Tree>> potential_trees {};
    for (auto &it : result.ways_dict) {
        const std::unique_ptr<geos::geom::LineString> line = Way::LineStringFromRefs(it.second->GetOSMId(),
                                                                                     it.second->GetRefs(),
                                                                                     result.nodes_dict, tile_handler);
        if (line->isValid()) {
            if (line->getLength() / (double)it.second->GetRefs()->size() > Parameters::Get().TREES_MAX_AVG_DIST_TREES_ROW) {
                auto length_indexed_line = geos::linearref::LengthIndexedLine(line.get());
                for (int i = 0; i < (int)(line->getLength() / Parameters::Get().TREES_DIST_TREES_ROW_CALCULATED); ++i) {
                    auto my_coord = length_indexed_line.extractPoint(i * Parameters::Get().TREES_DIST_TREES_ROW_CALCULATED);
                    potential_trees.push_back(std::make_unique<Tree>(my_coord, TreeOrigin::mapped, TreeType::significant_trees));
                }
                // and then always the last node
                auto last_node = result.nodes_dict[it.second->GetRefs()->back()].get();
                potential_trees.push_back(std::make_unique<Tree>(last_node->GetLonLat(),
                                                                 last_node->GetTags(),
                                                                 TreeOrigin::mapped,
                                                                 tile_handler,
                                                                 TreeType::significant_trees));
            } else {
                for (auto ref : *it.second->GetRefs()) {
                    auto node = result.nodes_dict[ref].get();
                    potential_trees.push_back(std::make_unique<Tree>(node->GetLonLat(),
                                                                     node->GetTags(),
                                                                     TreeOrigin::mapped,
                                                                     tile_handler,
                                                                     TreeType::significant_trees));
                }
            }
        }
    }
    ExtendTreesIfDistanceOk(potential_trees, existing_trees);
}

/*! \brief Trees in a line as mapped in OSM (tree_lined=*).
 *
 * Cf. https://wiki.openstreetmap.org/wiki/Key:tree_lined
 * We assume that "yes" is the same as "both".
 *
 * Must be called after TreeRows - because tree in rows is an actual mapping, while this is a feature indication.
 *
 * @param existing_trees gets extended with additional trees
 * @param tile_handler
 * @param osm_reader
 */
void ProcessOSMTreeLines(std::vector<std::unique_ptr<Tree>> &existing_trees,
                         const TileHandler &tile_handler, const OSMDBDataReader &osm_reader) {
    OSMReadResult result = osm_reader.FetchWaysFromKeys({k_tree_lined});

    std::vector<std::unique_ptr<Tree>> potential_trees {};
    for (auto &it : result.ways_dict) {
        if (it.second->GetTags()->contains(k_natural) and it.second->GetTags()->at(k_natural) == v_tree_row) {
            continue; // was already processed in ProcessOSMTreeRows()
        }
        auto tag_value = it.second->GetTags()->at(k_tree_lined);
        if (tag_value == v_no) {
            continue;
        }
        const std::unique_ptr<geos::geom::LineString> line = Way::LineStringFromRefs(it.second->GetOSMId(),
                                                                                     it.second->GetRefs(),
                                                                                     result.nodes_dict, tile_handler);
        if (line->isValid()) {
            auto length_indexed_line = geos::linearref::LengthIndexedLine(line.get());
            for (int i = 0; i < (int) (line->getLength() / Parameters::Get().TREES_DIST_TREES_ROW_CALCULATED); ++i) {
                if (tag_value == v_left or tag_value == v_both or tag_value == v_yes) {
                    auto my_coord = length_indexed_line.extractPoint(
                            i * Parameters::Get().TREES_DIST_TREES_ROW_CALCULATED, 4.0);
                    potential_trees.push_back(std::make_unique<Tree>(my_coord, TreeOrigin::mapped, TreeType::significant_trees));
                }
                if (tag_value == v_right or tag_value == v_both or tag_value == v_yes) {
                    auto my_coord = length_indexed_line.extractPoint(
                            i * Parameters::Get().TREES_DIST_TREES_ROW_CALCULATED, -4.0);
                    potential_trees.push_back(std::make_unique<Tree>(my_coord, TreeOrigin::mapped, TreeType::significant_trees));
                }
            }
        }
    }
    ExtendTreesIfDistanceOk(potential_trees, existing_trees);
}

/*! \brief Extracts all city blocks from existing buildings.
 *
 * If the zone linked to a building is not a CityBlock, then use a generic one spanning the whole tile.
 * In buildings._clean_building_zones_dangling_children there is also some cleanup of not valid buildings
 * However, that uses significant runtime and e.g. for Edinburgh out of ca. 110k buildings only ca.
 * 150 would be removed - which does not matter for what buildings are used here (collision detection)
 *
 * @param buildings
 * @param tile_handler
 * @return
 */
std::set<CityBlock*> PrepareCityBlocks(CityBlock* remaining_block, std::vector<std::shared_ptr<Building>> &buildings) {
    std::set<CityBlock*> city_blocks {};

    for (auto &building : buildings) {
        auto zone = building->GetZone();
        if (zone->GetZoneType() == ZoneType::city_block) {
            city_blocks.insert(dynamic_cast<CityBlock*>(zone));
        } else {
            remaining_block->RelateBuilding(building);
        }
    }
    city_blocks.insert(remaining_block);
    return city_blocks;
}

/*! \brief Find the geometry of parks and stuff from OSM, where trees often would be planted
 *
 * 'landuse=>recreation_ground' would be a possibility, but often has also swimming pools etc.
 * making it a bit difficult
 *
 * @param tile_handler
 * @param osm_reader
 * @return
 */
std::vector<std::unique_ptr<geos::geom::Geometry>> ProcessParks(const TileHandler &tile_handler, const OSMDBDataReader &osm_reader) {
    std::vector<std::string> parks_kv {};
    parks_kv.push_back(OSMDBDataReader::CreateKeyValuePair(k_leisure, v_park));
    OSMReadResult result = osm_reader.FetchWaysFromKeyValues(parks_kv);

    std::vector<std::unique_ptr<geos::geom::Geometry>> parks {};
    for (auto &it : result.ways_dict) {
        auto poly = it.second->PolygonFromOSMWay(result.nodes_dict, tile_handler);
        if (poly->isValid() and poly->getArea() > Parameters::Get().TREES_PARK_MIN_SIZE) {
            parks.push_back(std::move(poly));
        }
    }
    return parks;
}

/*! \brief Tests whether a point is within a building respectively not at least min_distance away.
 *
 * The use of CityBlocks should speed up the process considerably by using fewer geometric calculations.
 * @param point
 * @param city_blocks
 * @param min_dist
 * @return true if point is within bounds plus min_dist margin of building
 */
bool TestIsCoordInBuilding(const geos::geom::Point* point, const std::set<CityBlock*> &city_blocks, double min_dist) {
    for (auto city_block : city_blocks) {
        if (city_block->GetGeometry()->contains(point)) {
            for (const auto& building : city_block->GetOSMBuildings()) {
                auto bounds = building->GetOuterPolygon()->getEnvelopeInternal();
                if ((bounds->getMinX() - min_dist) < point->getX() and point->getX() < (bounds->getMaxX() + min_dist) \
                    and (bounds->getMinY() - min_dist) < point->getY() and point->getY() < (bounds->getMaxY() + min_dist)) {
                    return true;
                }
            }
        }
    }
    return false;
}

/*! \brief Creates a random set of points for trees within a polygon based on an average distance and a skip rate.
 *
 * This is not a very efficient algorithm, but it should be enough to give an ok distribution.
 * @param area_bounds
 * @param area_prep_geom
 * @param default_distance
 * @param keep_rate
 * @return
 */
std::vector<std::unique_ptr<geos::geom::Coordinate>> GenerateRandomTreePointsInArea(const geos::geom::Envelope* area_bounds,
                                                                                    std::unique_ptr<geos::geom::prep::PreparedPolygon> &area_prep_geom,
                                                                                    int default_distance, double keep_rate) {
    geos::geom::GeometryFactory::Ptr factory =
        TileHandler::MakeGeometryFactory();
    std::vector<std::unique_ptr<geos::geom::Coordinate>> random_coords {};
    int max_x {static_cast<int>((area_bounds->getMaxX() - area_bounds->getMinX()) / default_distance)};
    int max_y {static_cast<int>((area_bounds->getMaxY() - area_bounds->getMinY()) / default_distance)};

    std::mt19937_64 generator(12345); //we do not mind a pseudo random function seeded with a constant number
    std::uniform_real_distribution<double> keeping(0.0,1.0);

    std::uniform_real_distribution<double> variation(-default_distance / Parameters::Get().TREES_RADIUS_TEST,
                                                     default_distance / Parameters::Get().TREES_RADIUS_TEST);
    for (int i = 0; i < max_x; ++i) {
        for (int j = 0; j < max_y; ++j) {
            if (keeping(generator) > keep_rate) { // e.g. if random = 0.5 and keep_rate = 0.3, then this tree is not kept
                continue;
            }
            auto x = area_bounds->getMinX() + i * default_distance + variation(generator);
            auto y = area_bounds->getMinY() + j * default_distance + variation(generator);
            auto coord = geos::geom::Coordinate(x, y);
            auto point = factory->createPoint(coord);
            if (area_prep_geom->contains(point)) {
                random_coords.push_back(std::make_unique<geos::geom::Coordinate>(x, y));
            }
        }
    }
    return random_coords;
}

/*! \brief Creates random trees in an area respecting the presence of buildings.
 *
 * The trees are geometrically tested against the boundary box of buildings (not the whole tree, just the centre).
 * In parks trees are often around open spaces and not just randomly distributed - this heuristic is taken into
 * account by using a Perlin noise image.
 * @param area_bounds
 * @param area_prep_geom
 * @param city_blocks
 * @param potential_trees created trees are added to this container
 * @param default_distance
 * @param keep_rate
 * @return
 */
void PlaceRandomTreesInArea(const geos::geom::Envelope* area_bounds,
                            std::unique_ptr <geos::geom::prep::PreparedPolygon> &area_prep_geom,
                            const std::set<CityBlock*> &city_blocks,
                            std::vector<std::unique_ptr<Tree>> &potential_trees,
                            int default_distance, double keep_rate, TreeOrigin tree_origin) {
    // create random points
    std::vector<std::unique_ptr<geos::geom::Coordinate>> random_coords = GenerateRandomTreePointsInArea(area_bounds,
                                                                                                        area_prep_geom,
                                                                                                        default_distance,
                                                                                                        keep_rate);
    if (random_coords.empty()) {
        return;  //can happen if the area is too small for trees -> do not wast more computing
    }

    // read the Perlin noise file
    std::filesystem::path file_path = std::string(GetEnv("O2C_PATH_TO_DATA"));
    file_path /= "tex";
    file_path /= "perlin_noise_trees.png";
    std::ifstream file(file_path, std::ios_base::in | std::ios_base::binary);
    if (not std::filesystem::exists(file_path)) {
        std::string message = "File ";
        message.append(file_path);
        message.append(" does not exist");
        throw std::invalid_argument(message);
    }
    osg::ref_ptr<osgDB::Options> options = new osgDB::Options();
    auto perlin_noise = simgear::ImageUtils::readStream(file, options);

    // find the city blocks, which might have buildings which could interfere
    std::set<CityBlock*> test_city_blocks {};
    for (auto city_block : city_blocks) {
        if (!area_prep_geom->disjoint(city_block->GetGeometry())) {
            test_city_blocks.insert(city_block);
        }
    }

    // now we are ready to place trees
    geos::geom::GeometryFactory::Ptr factory = TileHandler::MakeGeometryFactory();
    int s_coord;
    int t_coord;
    int offset;
    TreeType tree_type;
    float red_value;
    const int perlin_image_size{512};
    for (auto &random_coord : random_coords) {
        if (random_coord->x >= 0.) {
            s_coord = int(random_coord->x) % perlin_image_size;
        } else {
            offset = (-1 * int(random_coord->x) / perlin_image_size + 1) *
                     perlin_image_size;
            s_coord = (offset + int(random_coord->x)) % perlin_image_size;
        }
        if (random_coord->y >= 0.) {
            t_coord = int(random_coord->y) % perlin_image_size;
        } else {
            offset = (-1 * int(random_coord->y) / perlin_image_size + 1) *
                     perlin_image_size;
            t_coord = (offset + int(random_coord->y)) % perlin_image_size;
        }

        red_value = perlin_noise->getColor(s_coord, t_coord).r();
        if (red_value <= 0.45) {
            continue; // no tree planted - open space (more white in Perlin image)
        } else if (red_value <= 0.60) {
            tree_type = TreeType::garden_vegetation;
        } else {
            tree_type = TreeType::significant_trees;
        }
        auto point = factory->createPoint(geos::geom::Coordinate(random_coord->x, random_coord->y));
        if (!TestIsCoordInBuilding(point, test_city_blocks, Parameters::Get().TREES_RADIUS_TEST)) {
            potential_trees.push_back(std::make_unique<Tree>(geos::geom::Coordinate(random_coord->x, random_coord->y),
                                                             tree_origin, tree_type));
        }
    }
}

/*! \brief Place additional trees based on specific land-use (not woods) in urban areas - aka./e.g. parks.
 *
 * A check is made against manually mapped trees - such that we do not add extra trees
 * if it is probable that the whole area was mapped manually and therefore should not be extended.
 * @param parks
 * @param trees gets extended by additionally placed trees in parks etc.
 * @param city_blocks
 */
void ProcessOSMTreesInParks(const std::vector<std::unique_ptr<geos::geom::Geometry>> &parks,
                            std::vector<std::unique_ptr<Tree>> &existing_trees,
                            const std::set<CityBlock*> &city_blocks) {
    geos::geom::GeometryFactory::Ptr factory =
        TileHandler::MakeGeometryFactory();
    std::vector<std::unique_ptr<Tree>> potential_trees {};
    auto mapped_factor = pow(Parameters::Get().TREES_DIST_BETWEEN_TREES_PARK_MAPPED, 2);
    std::vector<geos::geom::Point*> tree_points_to_check {};
    for (auto &tree : existing_trees) {
        auto coord = geos::geom::Coordinate(tree->GetX(), tree->GetY());
        tree_points_to_check.push_back(factory->createPoint(coord));
    }
    for (auto &my_geometry : parks) {
        int trees_contained {0};
        auto prep_park_geom = std::make_unique<geos::geom::prep::PreparedPolygon>(my_geometry.get());
        //check whether any of the existing manually mapped trees is within the area.
        //if above a certain ratio, then most probably all trees where manually mapped
        for (auto tree_point : tree_points_to_check) {
            if (prep_park_geom->contains(tree_point)) {
                trees_contained++;
            }
        }
        if (trees_contained == 0 or (my_geometry->getArea() / trees_contained) > mapped_factor) {
            // we are good to try to place more trees
            PlaceRandomTreesInArea(my_geometry->getEnvelopeInternal(), prep_park_geom, city_blocks, potential_trees,
                                   Parameters::Get().TREES_DIST_BETWEEN_TREES_PARK,
                                   Parameters::Get().TREES_KEEP_RATE_TREES_PARK,
                                   TreeOrigin::park);
        }
    }
    ExtendTreesIfDistanceOk(potential_trees, existing_trees);
}

/*! \brief Place trees in open space - gardens for residential areas and other open space for e.g. commercial.
 *
 * @param parks
 * @param existing_trees
 * @param city_blocks
 * @param remaining_block
 * @param tile_handler
 */
void ProcessOSMTreesInGardens(const std::vector<std::unique_ptr<geos::geom::Geometry>> &parks,
                              std::vector<std::unique_ptr<Tree>> &existing_trees,
                              const std::set<CityBlock*> &city_blocks,
                              CityBlock* remaining_block) {
    geos::geom::GeometryFactory::Ptr factory =
        TileHandler::MakeGeometryFactory();
    std::vector<std::unique_ptr<Tree>> potential_trees {};
    for (auto city_block : city_blocks) {
        auto default_distance = Parameters::Get().TREES_DIST_BETWEEN_TREES_GARDEN; //default - e.g. for farmyard and residential
        auto keep_rate = Parameters::Get().TREES_KEEP_RATE_TREES_GARDEN;
        if (city_block->GetBuildingZoneType() == BuildingZoneType::specialProcessing) {
            continue;  //no gardens for all those, who are not in a real city block
        }
        if (city_block->GetBuildingZoneType() == BuildingZoneType::aerodrome) {
            continue; //no trees on airport
        }
        if (city_block->GetBuildingZoneType() == BuildingZoneType::industrial or \
            city_block->GetBuildingZoneType() == BuildingZoneType::retail or \
            city_block->GetBuildingZoneType() == BuildingZoneType::commercial) {
            default_distance = Parameters::Get().TREES_DIST_BETWEEN_TREES_LOT;
            keep_rate = Parameters::Get().TREES_KEEP_RATE_TREES_LOT;
        }
        if (city_block->GetSettlementType() == SettlementType::centre) {
            keep_rate = keep_rate * 0.2; //reduce trees even more in centre
        } else if (city_block->GetSettlementType() == SettlementType::block) {
            keep_rate = keep_rate * 0.3;
        }
        auto city_block_prep_geom = std::make_unique<geos::geom::prep::PreparedPolygon>(city_block->GetGeometry());
        std::vector<std::unique_ptr<geos::geom::Coordinate>> random_coords = GenerateRandomTreePointsInArea(city_block->GetGeometry()->getEnvelopeInternal(),
                                                                                                            city_block_prep_geom,
                                                                                                            default_distance,
                                                                                                            keep_rate);
        for (auto &random_coord : random_coords) {
            bool exclude = false;
            auto coord = geos::geom::Coordinate(random_coord->x, random_coord->y);
            auto point = factory->createPoint(coord);
            if (not remaining_block->GetGeometry()->contains(point)) {
                continue; //no trees outside the tile (which here the remaining_block is a proxy for)
            }
            for (auto &park_geom : parks) {
                if (park_geom->contains(point)) {
                    exclude = true;
                    break;
                }
            }
            if (exclude) {
                continue;
            }
            std::set<CityBlock*> test_city_blocks {};
            test_city_blocks.insert(city_block);
            test_city_blocks.insert(remaining_block); //explicit test against outside buildings needed
            if (!TestIsCoordInBuilding(point, test_city_blocks, Parameters::Get().TREES_RADIUS_TEST)) {
                std::unique_ptr<Tree> tree = std::make_unique<Tree>(coord, TreeOrigin::garden, TreeType::garden_vegetation);
                potential_trees.push_back(std::move(tree));
            }
        }
    }
    ExtendTreesIfDistanceOk(potential_trees, existing_trees);
}

/*! Maps from a TreeType to a FlightGear material type to be used for the tree.
 *
 * The mapping should be provided for all TreeTypes.
 *
 * @param tree_type
 * @return the FlightGear material name to be used for this tree type
 */
std::string MapTreeTypeToMaterial(TreeType tree_type) {
    if (tree_type == TreeType::significant_trees) {
        return "SignificantTrees";
    } else { // TreeType::garden_vegetation
        return "GardenVegetation";
    }
}

void WriteTreesList(const std::vector<std::unique_ptr<Tree>> &trees, TreeType tree_type,
                    std::unique_ptr<STGManager> &stg_manager,
                    const std::string &tile_index,
                    ElevationProber &elev_prober) {
    std::vector<std::string> lines {};
    for (auto & tree : trees) {
        if (tree->GetTreeType() == tree_type) {
            std::stringstream stream;
            stream << std::fixed << std::setprecision(5);
            stream << tree->GetY()*-1 << " " << tree->GetX() << " ";
            auto probed_elev = elev_prober.Probe(LonLat(tree->GetX(), tree->GetY()), false);
            probed_elev -= CalcHorizonElev(tree->GetY(), tree->GetX());  // the earth is round
            stream << probed_elev;
            lines.emplace_back(stream.str());
        }
    }
    if (lines.empty()) {
        return; //nothing to do
    }
    auto material = MapTreeTypeToMaterial(tree_type);
    std::stringstream file_name;
    file_name << tile_index << "_";
    file_name << material << "_tree_list.txt";
    stg_manager->AddTreeList(file_name.str(), material);
    std::ofstream tree_list_file {stg_manager->GetSTGDirectoryPath().append(file_name.str()), std::ios::out};
    for (auto p = lines.begin(); p != lines.end(); ++p) {
        tree_list_file << *p;
        if (p != lines.end() -1) { // FGFS complains with a WARNING if a file contains an empty last line
            tree_list_file << std::endl;
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Total number of shader trees written to a tree_list " << material << ": " << lines.size();
}

void ProcessTrees(std::vector<std::shared_ptr<Building>> &buildings, const TileHandler &tile_handler, OSMDBDataReader &osm_reader) {
    BOOST_LOG_TRIVIAL(info) << "Processing trees";

    // create a special CityBlock that handles those buildings, which do not have a CityBlock assigned
    auto [min_coord, max_coord] = tile_handler.GetTileExtentLocal();
    std::unique_ptr<geos::geom::Polygon> box = MakeLocalBox(min_coord.x, min_coord.y, max_coord.x, max_coord.y);
    auto remaining_block = new CityBlock(0, std::move(box), BuildingZoneType::specialProcessing);

    std::set<CityBlock*> city_blocks = PrepareCityBlocks(remaining_block, buildings);

    std::vector<std::unique_ptr<Tree>> trees = ProcessOSMTreeNodes(tile_handler, osm_reader);
    BOOST_LOG_TRIVIAL(info) << "Number of manually mapped trees found: " << trees.size();

    ProcessOSMTreeRows(trees, tile_handler, osm_reader);
    BOOST_LOG_TRIVIAL(info) << "Total number of trees after trees in rows etc.: " << trees.size();
    ProcessOSMTreeLines(trees, tile_handler, osm_reader);
    BOOST_LOG_TRIVIAL(info) << "Total number of trees after trees in lines etc.: " << trees.size();

    std::vector<std::unique_ptr<geos::geom::Geometry>> parks = ProcessParks(tile_handler, osm_reader);
    BOOST_LOG_TRIVIAL(info) << "Number of park areas used: " << parks.size();

    ProcessOSMTreesInParks(parks, trees, city_blocks);
    BOOST_LOG_TRIVIAL(info) << "Total number of trees after placing in parks: " << trees.size();

    ProcessOSMTreesInGardens(parks, trees, city_blocks, remaining_block);
    BOOST_LOG_TRIVIAL(info) << "Total number of trees after placing in gardens: " << trees.size();

    // =========== Do the plotting =============================================
    if (Parameters::Get().DEBUG_PLOT_TREES) {
        DrawTrees(min_coord, max_coord, buildings, trees,
                  tile_handler.GetTileIndex());
    }

    // =========== Write to scenery folders ====================================
    ElevationProber ep {tile_handler};

    std::unique_ptr<STGManager> stg_manager = std::make_unique<STGManager>(SceneryType::trees, tile_handler.GetTileCentre());
    WriteTreesList(trees, TreeType::significant_trees, stg_manager,
                   tile_handler.GetTileIndex(), ep);
    WriteTreesList(trees, TreeType::garden_vegetation, stg_manager,
                   tile_handler.GetTileIndex(), ep);
    stg_manager->Write(tile_handler.GetTileIndex());

    // =========== Clean up ====================================================
    delete remaining_block;
}
