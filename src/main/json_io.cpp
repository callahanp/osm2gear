#include "json_io.h"

#include <thread>         // std::this_thread::sleep_for

#include "boost/log/trivial.hpp"

#include <cpr/cpr.h>

#include "../osm/osm_types.h"


/*!
 *  Might raise all sorts of exceptions. In any case of problem the return value is None.
    Errors will be logged.

    https://wiki.openstreetmap.org/wiki/Key:wikidata?uselang=en-GB
    https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/Wikidata_Query_Help
    https://www.mediawiki.org/wiki/Wikidata_Query_Service/User_Manual#SPARQL_endpoint

    E.g. https://www.wikidata.org/wiki/Q1425473

    Population property: https://www.wikidata.org/wiki/Property:P1082

    select * where {
      wd:Q1425473 wdt:P1082 ?o .
    }
 */
std::optional<int> QueryPopulationWikidata(const std::string &entity_id) {
    //This method is often called in a loop.
    //Currently, there is a limit of 60 requests to e.g. WikiData -> throttle
    //See https://www.mediawiki.org/wiki/Wikidata_Query_Service/User_Manual#Query_limits
    std::this_thread::sleep_for (std::chrono::seconds(1));

    std::string query = "https://query.wikidata.org/sparql?query=SELECT%20*%20WHERE%20{wd:" + entity_id;
    query += "%20wdt:P1082%20?o%20.}&format=json";
    BOOST_LOG_TRIVIAL(debug) << "Request to wikidata: " << query;

    auto ssl_not_verified = cpr::VerifySsl(false); //otherwise, we get status code: 0 and error: SSL certificate problem: unable to get local issuer certificate
    cpr::Response r = cpr::Get(cpr::Url{query}, ssl_not_verified);
    if (r.status_code == 200 or r.status_code == 203) {
        std::size_t first_pos = r.text.find("value");
        if (first_pos != std::string::npos) {
            first_pos = r.text.find(':', first_pos);
            if (first_pos != std::string::npos) {
                first_pos = r.text.find('"', first_pos) + 1;
                std::size_t second_pos = r.text.find('"', first_pos);
                std::string value_string = r.text.substr(first_pos, second_pos);
                auto parsed_value = ParseInt(value_string);
                if (parsed_value.has_value() and parsed_value.value() > 0) {
                    return parsed_value.value();
                }
            }
        }
        LogDebugWarning("No suitable value found in wikidata returned data structure: " + r.text);
    }
    BOOST_LOG_TRIVIAL(warning) << "Request to wikidata failed with status code: " << r.status_code << " and error: " << r.error.message;
    return std::nullopt;
}
