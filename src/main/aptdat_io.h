/*
 Handles reading from apt.dat airport files and read/write to pickle file for minimized representation.
 See http://developer.x-plane.com/?article=airport-data-apt-dat-file-format-specification for the specification.

 FlightGear 2016.4 can read multiple apt.data files - see e.g. http://wiki.flightgear.org/FFGo
 and https://sourceforge.net/p/flightgear/flightgear/ci/516a5cf016a7d504b09aaac2e0e66c7e9efd42b2/.
 However, this module does only support reading from the apt.dat.ws3.gz in
 $FG_ROOT/Airports/ - i.e. the version for WS3.
 */

#ifndef OSM2GEAR_APTDAT_IO_H
#define OSM2GEAR_APTDAT_IO_H

#include <vector>

#include "geos/geom/Polygon.h"

#include "utils.h"

class Perimeter {
private:
    std::vector<std::unique_ptr<std::vector<std::unique_ptr<LonLat>>>> list_of_nodes_lists_ {};
public:
    Perimeter() = default;
    /**
     * Append new nodes list. There can be situations, where several closed polygons make up a pavement etc.
     * @param nodes_list
     */
    void AppendNodesList(std::unique_ptr<std::vector<std::unique_ptr<LonLat>>> && nodes_list) {
        list_of_nodes_lists_.push_back(std::move(nodes_list));
    }
    /**
     * If no node within - or there are no nodes - then return False.
     * That is ok, because at least the runways will be checked.
     * @return
     */
    [[nodiscard]] bool WithinBoundary(const Boundary &boundary) const;

    [[nodiscard]] std::vector<std::unique_ptr<geos::geom::Polygon>> CreatePolygons(const TileHandler &tile_handler) const;
};

class Runway {
public:
    virtual ~Runway() = default;
    [[nodiscard]] virtual bool WithinBoundary(const Boundary &boundary) const = 0;
    //virtual std::unique_ptr<geos::geom::Polygon> CreateBlockedArea(TileHandler &tile_handler) = 0;
};

class RunwayStrip : public Runway {
   private:
    float width_;
    LonLat start_;
    LonLat end_;
   public:
    RunwayStrip(float width, LonLat &start, LonLat &end);
    [[nodiscard]] bool WithinBoundary(const Boundary &boundary) const override;
};

class Helipad : public Runway {
   private:
    float length_;
    float width_;
    LonLat centre_;
    float orientation_;
   public:
    Helipad(float length, float width, LonLat &centre, float orientation);
    [[nodiscard]] bool WithinBoundary(const Boundary &boundary) const override;
};


class Airport {
private:
    std::string code_;
    std::string name_;
    std::shared_ptr<Perimeter> apt_boundary_ = nullptr;
    std::vector<std::shared_ptr<Perimeter>> pavements_ {};
    std::vector<std::unique_ptr<Runway>> runways_{};
public:
    Airport(std::string code, std::string name);
    void SetAirportBoundary(std::shared_ptr<Perimeter> &apt_boundary) {
        apt_boundary_ = apt_boundary;
    }
    void AppendPavement(std::shared_ptr<Perimeter> &pavement) {
        pavements_.push_back(pavement);
    }
    void AppendRunway(std::unique_ptr<Runway> &&runway) {
        runways_.push_back(std::move(runway));
    }
    [[nodiscard]] bool WithinBoundary(const Boundary &boundary) const;
    [[nodiscard]] std::vector<std::unique_ptr<geos::geom::Polygon>> CreateBoundaryPolygons(const TileHandler &tile_handler) const;
};

std::vector<std::unique_ptr<Airport>> ReadAptDatFile(const TileHandler &tile_handler);

#endif  // OSM2GEAR_APTDAT_IO_H
