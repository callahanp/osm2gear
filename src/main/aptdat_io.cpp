# include "aptdat_io.h"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <set>
#include <utility>

#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/log/trivial.hpp>
#include <geos/geom/Coordinate.h>
#include <geos/geom/CoordinateSequenceFactory.h>
#include <geos/geom/GeometryFactory.h>

#include "utils.h"

bool Perimeter::WithinBoundary(const Boundary &boundary) const {
    if (list_of_nodes_lists_.empty()) {
        return false;
    }
    for (auto &node_list : list_of_nodes_lists_) {
        for (auto &lon_lat : *node_list) {
            if ((boundary.west <= lon_lat->lon and lon_lat->lon <= boundary.east) and (boundary.south <= lon_lat->lat and lon_lat->lat <= boundary.north)) {
                return true;
            }
        }
    }
    return false;
}

std::vector<std::unique_ptr<geos::geom::Polygon>> Perimeter::CreatePolygons(const TileHandler &tile_handler) const {
    std::vector<std::unique_ptr<geos::geom::Polygon>> polygons {};
    if (not list_of_nodes_lists_.empty()) {
        geos::geom::GeometryFactory::Ptr factory =
            TileHandler::MakeGeometryFactory();
        geos::geom::Coordinate coord;
        for (auto &nodes_list : list_of_nodes_lists_) {
            std::vector<geos::geom::Coordinate> coordinates {};
            for (auto &lon_lat : *nodes_list) {
                coord = tile_handler.ToLocal(*lon_lat);
                coordinates.push_back(coord);
            }
            //x-plane apt.dat does not close the ring - but GEOS needs it
            coord = tile_handler.ToLocal(*(nodes_list->front()));
            coordinates.push_back(coord);
            if (coordinates.size() <= 3) { //a linear ring needs at least 3 unique coordinates plus the repeated node at start
                continue;  // silently ignore instead of raising an exception, from which we would recover the same way
            }
            const geos::geom::CoordinateSequenceFactory* coord_factory = factory->getCoordinateSequenceFactory();
            std::unique_ptr<geos::geom::LinearRing> ring = factory->createLinearRing(coord_factory->create(std::move(coordinates)));
            if (ring->isEmpty() or (not ring->isValid()) or (not ring->isSimple())) {
                continue;
            }
            polygons.push_back(std::move(factory->createPolygon(std::move(ring))));
        }
    }
    return polygons;
}

RunwayStrip::RunwayStrip(float width, LonLat &start, LonLat &end): width_ {width}, start_ {start}, end_ {end} {}

bool RunwayStrip::WithinBoundary(const Boundary &boundary) const {
    if ((boundary.west <= start_.lon and start_.lon <= boundary.east) and (boundary.south <= start_.lat and start_.lat <= boundary.north)) {
        return true;
    }
    if ((boundary.west <= end_.lon and end_.lon <= boundary.east) and (boundary.south <= end_.lat and end_.lat <= boundary.north)) {
        return true;
    }
    return false;
}

Helipad::Helipad(float length, float width, LonLat &centre, float orientation): length_ {length}, width_ {width}, centre_ {centre}, orientation_ {orientation} {}

bool Helipad::WithinBoundary(const Boundary &boundary) const {
    if ((boundary.west <= centre_.lon and centre_.lon <= boundary.east) and (boundary.south <= centre_.lat and centre_.lat <= boundary.north)) {
        return true;
    }
    return false;
}

Airport::Airport(std::string code, std::string name): code_ {std::move(code)}, name_ {std::move(name)} {}

bool Airport::WithinBoundary(const Boundary &boundary) const {
    for (auto &runway : runways_) {
        if (runway->WithinBoundary(boundary)) {
            return true;
        }
    }
    if (apt_boundary_ != nullptr and apt_boundary_->WithinBoundary(boundary)) {
        return true;
    }
    return false;
}

std::vector<std::unique_ptr<geos::geom::Polygon>> Airport::CreateBoundaryPolygons(const TileHandler &tile_handler) const {
    std::vector<std::unique_ptr<geos::geom::Polygon>> polygons {};
    if (apt_boundary_ != nullptr) {
        polygons = std::move(apt_boundary_->CreatePolygons(tile_handler));
    }
    return polygons;
}

std::vector<std::unique_ptr<Airport>> ReadAptDatFile(const TileHandler &tile_handler) {
    TimeLogger time_logger {};
    std::filesystem::path file_path = std::string(GetEnv("FG_ROOT"));
    file_path /= "Airports";
    file_path /= "apt.dat.ws3.gz";
    std::ifstream file(file_path, std::ios_base::in | std::ios_base::binary);
    if (not std::filesystem::exists(file_path)) {
        std::string message = "File ";
        message.append(file_path);
        message.append(" does not exist");
        throw std::invalid_argument(message);
    }

    boost::iostreams::filtering_streambuf<boost::iostreams::input> inbuf;
    inbuf.push(boost::iostreams::gzip_decompressor()); //gzip not zlib_decompressor!
    inbuf.push(file);
    std::istream instream(&inbuf); //Convert streambuf to istream

    //now we can iterate line by line
    std::vector<std::unique_ptr<Airport>> airports {};
    std::string line;
    std::shared_ptr<Perimeter> perimeter;
    std::unique_ptr<Airport> airport;
    const std::set<std::string> airport_row_codes {"1", "16", "17", "99"};
    const std::set<std::string> node_row_codes {"111", "112", "113", "114", "115", "116"};
    const std::set<std::string> node_close_loop_row_codes {"113", "114"};
    std::unique_ptr<std::vector<std::unique_ptr<LonLat>>> current_apt_boundary_nodes = std::make_unique<std::vector<std::unique_ptr<LonLat>>>();
    bool in_boundary = false;
    int total_airports {0};
    while(std::getline(instream, line)) {
        // splitting from https://www.fluentcpp.com/2017/04/21/how-to-split-a-string-in-c/
        std::istringstream iss(line);
        std::vector<std::string> parts(std::istream_iterator<std::string>{iss},
                                       std::istream_iterator<std::string>());
        if (parts.empty()) {
            continue;
        }
        if (in_boundary) {
            if (node_row_codes.count(parts[0]) == 0) {
                in_boundary = false;
            } else {
                current_apt_boundary_nodes->push_back(std::make_unique<LonLat>(
                    std::stod(parts[2]), std::stod(parts[1])));
                if (node_close_loop_row_codes.count(parts[0]) > 0) {
                    perimeter->AppendNodesList(
                        std::move(current_apt_boundary_nodes));
                    current_apt_boundary_nodes = std::make_unique<
                        std::vector<std::unique_ptr<LonLat>>>();
                }
            }
        } else if (airport_row_codes.count(parts[0]) > 0) {
            // first actually append the previously read airport data to the collection if within bounds
            if (airport != nullptr and airport->WithinBoundary(tile_handler.GetTileBoundary())) {
                airports.push_back(std::move(airport));
            }
            // create a new empty airport unless we have reached the file termination 99
            if (parts[0] != "99") {
                airport = std::make_unique<Airport>(parts[4], parts[5]);
                total_airports++;
            }
        } else if (parts[0] == "100") { //Runway on land
            LonLat start {std::stof(parts[10]), std::stof(parts[9])};
            LonLat end {std::stof(parts[19]), std::stof(parts[18])};
            airport->AppendRunway(std::make_unique<RunwayStrip>(std::stof(parts[1]), start, end));
        } else if (parts[0] == "101") { //Runway on water
            LonLat start {std::stof(parts[5]), std::stof(parts[4])};
            LonLat end {std::stof(parts[8]), std::stof(parts[7])};
            airport->AppendRunway(std::make_unique<RunwayStrip>(std::stof(parts[1]), start, end));
        } else if (parts[0] == "102") { //Helipad
            LonLat centre {std::stof(parts[3]), std::stof(parts[2])};
            airport->AppendRunway(std::make_unique<Helipad>(std::stof(parts[5]), std::stof(parts[6]), centre, std::stof(parts[4])));
        } else if (parts[0] == "110") {  //Pavement
            perimeter = std::make_shared<Perimeter>();
            in_boundary = true;
            airport->AppendPavement(perimeter);
        } else if (parts[0] == "130") { // Airport boundary header
            perimeter = std::make_shared<Perimeter>();
            in_boundary = true;
            airport->SetAirportBoundary(perimeter);
        }
    }
    //Cleanup
    file.close();

    BOOST_LOG_TRIVIAL(info) << "Read " << total_airports << " airports, " << airports.size() << " having runways/helipads within the boundary";
    time_logger.Log("Execution time");
    return airports;
}