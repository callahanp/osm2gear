#ifndef OSM2GEAR_PLOTTING_H
#define OSM2GEAR_PLOTTING_H


#include <memory>
#include <vector>

#include "geos/geom/Geometry.h"

#include "models.h"
#include "trees.h"


void DrawLandUse(const geos::geom::Coordinate &min_coord, const geos::geom::Coordinate &max_coord,
                 const std::vector<std::shared_ptr<Building>> &buildings,
                 const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                 const std::vector<std::unique_ptr<geos::geom::Geometry>> &lit_areas,
                 const std::vector<std::unique_ptr<GridIndexedGeometry>> &settlement_clusters,
                 const std::string &tile_index);

void DrawTrees(const geos::geom::Coordinate &min_coord, const geos::geom::Coordinate &max_coord,
               const std::vector<std::shared_ptr<Building>> &buildings,
               const std::vector<std::unique_ptr<Tree>> &trees,
               const std::string &tile_index);

#endif //OSM2GEAR_PLOTTING_H
