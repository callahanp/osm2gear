#ifndef OSM2GEAR_MODELS_H
#define OSM2GEAR_MODELS_H

#include <map>
#include <set>

#include <geos/geom/Geometry.h>
#include <geos/geom/prep/PreparedPolygon.h>

#include "../osm/osm_types.h"
#include "enumerations.h"
#include "utils.h"

class Building; // forward declaration instead of include "buildings.h" -> circular dependency

class GridIndexed {
private:
    std::set<int> grid_indices_ {};  //assuming set is better than unsorted_set when doing intersection
public:
    GridIndexed() = default;
    void AddGridIndex(int);
    std::set<int>* GetGridIndices() {
        return &grid_indices_;
    }
};

class GridIndexedGeometry : public GridIndexed {
private:
    std::unique_ptr<geos::geom::Geometry> geometry_;
public:
    explicit GridIndexedGeometry(std::unique_ptr<geos::geom::Geometry> &&);
    geos::geom::Geometry* GetGeometry();
    bool ContainsOther(const geos::geom::Geometry* other) const;
    bool Intersects(const geos::geom::Geometry* other) const;
};

class GridIndexedPreparedPolygon : public GridIndexedGeometry {
private:
    std::unique_ptr<geos::geom::prep::PreparedPolygon> prepared_polygon_;
public:
    explicit GridIndexedPreparedPolygon(std::unique_ptr<geos::geom::Geometry> &&);
    bool IsDisjoint(const geos::geom::Geometry* other) const;
};

class OSMFeature {
protected:
    const osm_id_t osm_id_;
    std::unique_ptr<geos::geom::Geometry> geometry_;

public:
    OSMFeature(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&);
    [[nodiscard]] geos::geom::Geometry* GetGeometry() const;
    void SetGeometry(std::unique_ptr<geos::geom::Geometry>);
    osm_id_t GetOSMId() const {
        return osm_id_;
    }
    std::unique_ptr<geos::geom::Geometry> CloneOfGeometry() const {
        return geometry_->clone();
    }
    virtual bool IsDefinedType() const = 0;
};

class OSMFeatureLinear : public OSMFeature {
protected:
    bool has_embankment_or_cutting_;
    bool tunnel_;
public:
    OSMFeatureLinear(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, const tags_t *);
    bool IsTunnel() const {
        return tunnel_;
    }
    bool HasEmbankmentOrCutting() const {
        return has_embankment_or_cutting_;
    }
    virtual double GetWidth() const = 0;
    virtual bool UseForBuildingZoneSplit() const = 0;
};

class Highway : public OSMFeatureLinear {
private:
    std::optional<HighwayType> highway_type_;
    bool roundabout_;
    bool oneway_;
    int lanes_;
public:
    Highway(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, const tags_t *);
    bool IsDefinedType() const override {
        return highway_type_.has_value();
    }
    double GetWidth() const override;
    bool UseForBuildingZoneSplit() const override;
    bool PopulateBuildingsAlong() const;
};

class Waterway : public OSMFeatureLinear {
private:
    std::optional<WaterwayType> waterway_type_;
public:
    Waterway(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, const tags_t *);
    bool IsDefinedType() const override {
        return waterway_type_.has_value();
    }
    double GetWidth() const override {
        return 10;
    }
    bool UseForBuildingZoneSplit() const override {
        if (tunnel_) {
            return false;
        }
        return waterway_type_ == WaterwayType::large;
    }
};

class RailwayLine : public OSMFeatureLinear {
private:
    std::optional<RailwayLineType> rail_way_line_type_;
    int tracks_;
    bool service_spur_;
public:
    RailwayLine(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, const tags_t *);
    bool IsDefinedType() const override {
        return rail_way_line_type_.has_value();
    }
    double GetWidth() const override;
    bool UseForBuildingZoneSplit() const override;
};

class Place : public OSMFeature {
private:
    std::optional<PlaceType> place_type_;
    bool is_point_;
    std::optional<int> population_;
    void ParseTagsForPopulation(const tags_t*);
    double CalculateCircleRadius(long population, double power) const;
public:
    Place(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, const tags_t *, bool is_point);
    void TransformToPoint();
    bool IsDefinedType() const override {
        return place_type_.has_value();
    }
    bool IsPoint() const {
        return is_point_;
    }
    PlaceType GetPlaceType() const {
        return place_type_.value();
    }
    std::tuple<std::unique_ptr<GridIndexedPreparedPolygon>, std::unique_ptr<GridIndexedPreparedPolygon>, std::unique_ptr<GridIndexedPreparedPolygon>> CreatePreparedPlacePolygons(const geos::geom::GeometryFactory::Ptr &) const;
};

class Zone : public OSMFeature {
protected:
    ZoneType zone_type_;
    BuildingZoneType building_zone_type_;
    SettlementType settlement_type_ = SettlementType::rural;
    std::vector<std::shared_ptr<Building>> osm_buildings_ {};
public:
    Zone(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, ZoneType, BuildingZoneType);
    ZoneType GetZoneType() const {
        return zone_type_;
    }
    BuildingZoneType GetBuildingZoneType() const {
        return building_zone_type_;
    }
    SettlementType GetSettlementType() const {
        return settlement_type_;
    }
    void RelateBuilding(std::shared_ptr<Building> &building);
    void UnRelateBuilding(const std::shared_ptr<Building> &building);
    std::vector<std::shared_ptr<Building>> GetOSMBuildings() {
        return osm_buildings_;
    }
    bool HasOSMBuildings() const {
        return !this->osm_buildings_.empty();
    }
    long NumberOfOSMBuildings() const {
        return osm_buildings_.size();
    }
};

class CityBlock : public Zone {
private:
    std::unique_ptr<geos::geom::prep::PreparedPolygon> prep_geom_;
    void MakePreparedGeometry();
public:
    CityBlock(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, BuildingZoneType);
    bool IsDefinedType() const override {
        return true;
    }
    void SetSettlementType(SettlementType settlement_type) {
        settlement_type_ = settlement_type;
        //FIXME: self.__building_levels = bl.calc_levels_for_settlement_type(value, enu.BuildingClass.residential)
    }
    void ClearPreparedGeometry() {
        prep_geom_ = nullptr;
    }
    bool ContainsProperly(geos::geom::Geometry * other) {
        if (prep_geom_ == nullptr) {
            MakePreparedGeometry();
        }
        return prep_geom_->containsProperly(other);
    }
    bool Intersects(geos::geom::Geometry * other) {
        if (prep_geom_ == nullptr) {
            MakePreparedGeometry();
        }
        return prep_geom_->intersects(other);
    }
};

class BuildingZone : public GridIndexed, public Zone {
private:
    std::optional<BuildingZoneOrigin> building_zone_origin_;
    std::vector<std::unique_ptr<CityBlock>> city_blocks_ {};

public:
    BuildingZone(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, BuildingZoneType, std::optional<BuildingZoneOrigin>);
    bool ContainsOrIntersects(const geos::geom::Geometry *);
    /*! Geometry calculation of difference such that the result is the other's geometry reduced by this zone's geometry.
     *
     * @param other_geometry
     * @return
     */
    std::unique_ptr<geos::geom::Geometry> NonIntersectingDifference(const geos::geom::Geometry *other_geometry);
    std::shared_ptr<Building> PopOSMBuilding() {
        if (osm_buildings_.empty()) {
            return nullptr;
        }
        auto pointer = osm_buildings_.back();
        osm_buildings_.pop_back();
        return pointer;
    }
    void ReassignOSMBuildingsToCityBlocks();
    bool IsDefinedType() const override {
        return true;
    }
    bool IsAerodrome() const {
        if (this->IsDefinedType() and building_zone_type_ == BuildingZoneType::aerodrome) {
            return true;
        }
        return false;
    }
    /** \brief Whether this zone was generated from buildings.
     *
     * NB: there are also building zones generated from apt_dat, but they are
     * treated as if these aerodrome zones were from OSM.
     *
     * @return true if the origin is from_buildings.
     */
    bool IsGenerated() const {
        return (building_zone_origin_ == BuildingZoneOrigin::from_buildings);
    }
    std::optional<BuildingZoneOrigin> GetBuildingZoneOrigin() const {
        return building_zone_origin_;
    }
    void SetMaxSettlementType(SettlementType new_type) {
        if (new_type > settlement_type_) {
            settlement_type_ = new_type;
        }
    }
    void SetSettlementTypeCityBlocks(SettlementType, std::unique_ptr<GridIndexedPreparedPolygon>&);
    void AddCityBlock(std::unique_ptr<CityBlock> block) {
        city_blocks_.push_back(std::move(block));
    }
    void ResetCityBlocks() {
        city_blocks_.clear();
    }
    std::vector<std::unique_ptr<CityBlock>>* GetCityBlocks() {
        return &city_blocks_;
    }

    void GuessBuildingZoneType(const std::vector<std::unique_ptr<Place>> &farm_places);
};

#endif //OSM2GEAR_MODELS_H
