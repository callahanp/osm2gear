#ifndef OSM2GEAR_PARAMETERS_H
#define OSM2GEAR_PARAMETERS_H

#include <string>

#include <geos/geom/Coordinate.h>

#include "utils.h"


class Parameters {
private:
    Parameters() = default;
public:
    static Parameters& Get() {
        static Parameters instance;
        return instance;
    }
    Parameters(Parameters const&) = delete;
    Parameters(Parameters &&) = delete;
    Parameters operator=(Parameters const&) = delete;
    Parameters operator=(Parameters &&) = delete;

    // from command line options
    std::string PATH_TO_SCENERY;
    std::string PATH_TO_OUTPUT;
    bool USE_WS30_SCENERY = false;

    // fixed
    double OWBB_PLACE_BOUNDARY_EXTENSION {10'000};

    bool DEBUG_PLOT_LANDUSE {true};
    bool DEBUG_PLOT_TREES {true};

    int OWBB_GENERATE_LANDUSE_BUILDING_BUFFER_DISTANCE {30};
    int OWBB_GENERATE_LANDUSE_BUILDING_BUFFER_DISTANCE_MAX {50};
    int OWBB_GENERATE_LANDUSE_HOLES_MIN_AREA {20000};
    int OWBB_GENERATE_LANDUSE_SIMPLIFICATION_TOLERANCE {20};

    int OWBB_BUILT_UP_BUFFER {50};

    long OWBB_BUILT_UP_MIN_LIT_AREA {100000};

    int OWBB_PLACE_POPULATION_DEFAULT_CITY {200000};
    int OWBB_PLACE_POPULATION_DEFAULT_TOWN {20000};
    int OWBB_PLACE_POPULATION_MIN_BLOCK = 15000;
    double OWBB_PLACE_RADIUS_EXPONENT_CENTRE {0.5};  // 1/2
    double OWBB_PLACE_RADIUS_EXPONENT_BLOCK {0.6};  // 5/8
    double OWBB_PLACE_RADIUS_EXPONENT_DENSE {0.666};  // 2/3
    double OWBB_PLACE_RADIUS_FACTOR_CITY {1.};
    double OWBB_PLACE_RADIUS_FACTOR_TOWN {1.};

    int OWBB_GRID_SIZE {2000};

    int OWBB_MIN_CITY_BLOCK_AREA {200};
    int OWBB_CITY_BLOCK_HIGHWAY_BUFFER {3}; // in metres buffer around highways to find city blocks
    bool OWBB_SPLIT_AT_MAJOR_LINES {false};

    double BUILDING_MIN_AREA {50.0}; // minimum area for a building to be included in output (not used for buildings with parent)
    double BUILDING_INNER_MIN_AREA {BUILDING_MIN_AREA}; // minimum area for an inner ring - we want it to be visible
    double BUILDING_REDUCE_THRESHOLD {200.0}; // threshold area of a building below which a rate of buildings gets reduced from output
    double BUILDING_REDUCE_RATE {0.1}; // rate (between 0 and 1) of buildings below a threshold which get reduced randomly in output

    double BUILDING_MIN_NODE_DISTANCE {0.2}; // make sure nodes in buildings hava a minimum distance from each other

    double BUILDING_BALCONY_TOLERANCE_BASE {1.0};
    double BUILDING_BALCONY_TOLERANCE_RAILING {2.5};
    double BUILDING_BALCONY_TOLERANCE_DIFF {0.2};

    bool OWBB_GENERATE_BUILDINGS {false};


    int TREES_DIST_BETWEEN_TREES_PARK {15}; // the average distance between trees in a park
    // the rate at which randomly generated trees are kept (0.7 means 7 out of 10 are kept)
    // relative high value due to use of Perlin
    double TREES_KEEP_RATE_TREES_PARK {0.8};
    int TREES_DIST_BETWEEN_TREES_PARK_MAPPED {100}; // the average distance between trees in a park for testing manually mapped trees
    int TREES_PARK_MIN_SIZE {600}; // the minimal size of a park to be taken into account

    //garden is for residential and farmyards
    //keep rates are for dense/rural settlement areas - might get reduced in other areas
    int TREES_DIST_BETWEEN_TREES_GARDEN {6}; // the average distance between trees in a garden
    double TREES_KEEP_RATE_TREES_GARDEN {0.3}; // the rate at which trees are randomly kept (max = 1.0)
    //lot is for surrounding of industrial, retail, commercial
    int TREES_DIST_BETWEEN_TREES_LOT {20};
    double TREES_KEEP_RATE_TREES_LOT {0.2};


    int TREES_MAX_AVG_DIST_TREES_ROW {20}; // if average distance between mapped trees smaller, then replace with calculated
    int TREES_DIST_TREES_ROW_CALCULATED {15}; // the distance used between trees in a row/line if calculated manually
    int TREES_DIST_MINIMAL {10}; // minimal distance between trees unless mapped manually
    double TREES_RADIUS_TEST {3.0}; //radius to test around tree coordinate and for random distribution
};

#endif //OSM2GEAR_PARAMETERS_H
