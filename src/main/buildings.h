#ifndef OSM2GEAR_BUILDINGS_H
#define OSM2GEAR_BUILDINGS_H

#include <memory>

#include "../osm/data_reader.h"
#include "models.h"
#include "roofs.h"

class BuildingParent; //forward declaration

class Building : public Element {
   private:
    refs_t outer_refs_ {}; //for the outer ring - the last and first ref are the same (from OSM)!
    std::vector<refs_t> inner_refs_ {};
    std::map<std::shared_ptr<Building>, std::set<osm_id_t>> shared_refs_;
    bool has_neighbours_ {false};
    Zone * zone_ = nullptr;
    std::unique_ptr<geos::geom::Polygon> outer_polygon_;
    bool role_outline_{false}; // true if only used as outline for building parts (MemberRole::outline) -> not rendered
    std::shared_ptr<BuildingParent> parent_ = nullptr;
    std::unique_ptr<RoofHint> roof_hint_ = nullptr;

    /*! \brief """Calculates the roof orientation for roofs with neighbours for OSM buildings.
     * (Only used for buildings in meshes. Generated buildings do not have neighbours, but have a street angle.)
     * There is a possibility that the there is more than one neighbour or that the same neighbour
     * has more than 2 edges shared. Therefore, we assume that the longest shared edge is representative
     * for the side of the roof, which is 90 degrees to the ridge respectively is a gable end.
     */
    void CalcRidgeOrientation(const std::map<long, std::unique_ptr<Node>> &building_nodes, const TileHandler &tile_handler);
    /*! \brief For L-shaped roofs with at least one neighbour or a 4-edge building with 2 neighbours around a corner.
     * See description in roofs.RoofHint.
     * There are lots of situations, when it does not get an L-shaped roof, e.g. depending on where the neighbours are.
     */
    void CalcInnerNodeLShapeRoof(const std::map<long, std::unique_ptr<Node>> &building_nodes, const TileHandler &tile_handler);
    void CalcHasNeighbours() {
        for (auto &it : shared_refs_) {
            if (it.second.size() >= 2) {
                bool prev_found {false};
                for (size_t pos = 0; pos < outer_refs_.size() - 1; ++pos) {
                    if (it.second.contains(outer_refs_[pos])) {
                        if (prev_found) {
                            has_neighbours_ = true;
                            return;
                        }
                        prev_found = true;
                    } else {
                        prev_found = false;
                    }
                }
            }
        }
        has_neighbours_ = false;
    }
    /*! \brief Return the number of base refs in outer - i.e. without the repeated last ref from OSM.
     */
    [[nodiscard]] size_t GetOuterRefsBaseSize() const {
        if (outer_refs_.empty()) {
            return 0;
        }
        return outer_refs_.size() - 1;
    }
   public:
    Building(osm_id_t, std::unique_ptr<geos::geom::LinearRing> &&,
             const tags_t *tags, const refs_t *outer_refs,
             const TileHandler &);
    static bool IsBuildingProcessed(const tags_t *);
    [[nodiscard]] bool IsBuildingRendered(double random_0_1) const;
    [[nodiscard]] geos::geom::Geometry * GetOuterPolygon() const;
    /*! \ If outer references might have changed, then this allows to refresh the polygon.
     */
    void RefreshOuterPolygon(const std::map<long, std::unique_ptr<Node>> &building_nodes, const TileHandler &tile_handler);
    [[nodiscard]] double GetArea() const;
    void SetZoneNull() {
        this->zone_ = nullptr;
    }
    bool HasZone() {
        return this->zone_ != nullptr;
    }
    [[nodiscard]] bool HasHoles() const {
        return not inner_refs_.empty();
    }
    void SetZone(Zone *);
    Zone * GetZone() {
        return this->zone_;
    }
    [[nodiscard]] const refs_t* GetOuterRefs() const {
        const refs_t* refs_pointer = &outer_refs_;
        return refs_pointer;
    }
    void RemoveOuterRef(osm_id_t ref_to_remove);
    [[nodiscard]] const std::vector<refs_t>* GetInnerRefs() const {
        const std::vector<refs_t>* refs_pointer = &inner_refs_;
        return refs_pointer;
    }
    void AddSharedRef(std::shared_ptr<Building> &building, osm_id_t ref) {
        shared_refs_.try_emplace(building, std::set<osm_id_t>());
        shared_refs_[building].insert(ref);
    }
    /*! \brief Returns true if this buildings shares at least two consecutive references with another building.
     * I.e. we want a shared wall, not just 2 points, which might be in opposed edges.
     */
    [[nodiscard]] bool HasNeighbours() const {
        return has_neighbours_;
    }
    [[nodiscard]] bool HasBuildingAndBuildingPartTag() const;
    [[nodiscard]] bool IsBuildingPart() const;
    void AddInnerRefs(const refs_t *);

    /*! \brief To mark that this building/building part has an explicit MemberRole::outline in Simple3D
     *
     */
    void SetAsRoleOutline() {
        this->role_outline_ = true;
    }
    [[nodiscard]] bool IsRoleOutline() const {
        return role_outline_;
    }
    [[nodiscard]] bool HasParent() const {
        return this->parent_ != nullptr;
    }
    /*! \brief Should only ever be called indirectly by Building::SetParentAndSetAsChild
     */
    void SetParent(const std::shared_ptr<BuildingParent> &parent) {
        this->parent_ = parent;
    }
    /*! \brief Makes sure that when setting a BuildingParent then the buildings is automatically added as a child to parent.
     */
    static void SetParentAndSetAsChild(const std::shared_ptr<Building> &building, const std::shared_ptr<BuildingParent> &parent);
    void SetParentNull() {
        parent_.reset();
    }
    /*! \brief Remove the BuildingParent (if it exists) and clean-up in BuildingParent
     */
    static void RemoveParent(std::shared_ptr<Building> &building);
    std::shared_ptr<BuildingParent> & GetParent() {
        return this->parent_;
    }
    /*! \brief Remove nodes which are very close together and therefore are not visible or can lead to rounding errors.
     *
     * For outer rings we first check every second node, because sometimes small round structures are mapped
     * (e.g. antennas), which might be nice to keep (e.g. in Simple 3d) - but not with the same number of nodes
     * and not with nodes so close. After checking every second node, we check then every node.
     * This should result in that some edges are gone, but still enough remain.
     *
     * E.g. when processing triangulation in Python very close nodes can get the same due to float rounding.
     * See [(1920.6350103019627, 4345.37078023166), (1920.6350103019627, 4345.37078023166),
     * (1919.4017258347312, 4344.948704927007), (1909.5354500968785, 4323.73942086818),
     * (1909.5354500968785, 4322.262157301894), (1920.223915479552, 4308.544709900662),
     * (1928.4458119277626, 4307.595040465192), (1934.6122342639203, 4299.6811285029435),
     * (1937.489898020794, 4299.364572024453), (1950.2338375155205, 4309.283341683806), (1921.8682947691939, 4344.7376672746805)]
     * b.osm_id == 94852795; https://www.openstreetmap.org/way/94852795
     *
     * @returns a boolean and the number of nodes to be removed (for statistics).
     *          If false then not enough nodes left in outer ring (e.g. if a thin antenna has been mapped as a building at
     *          https://www.openstreetmap.org/way/406710839) and the (antenna-)building should be removed
     *          -> we do not get buildings who's edge length are minimal and therefore not visible
     */
    std::pair<bool, uint8_t> CheckDistanceNodes(const std::map<long, std::unique_ptr<Node>> &building_nodes);

    /*! \brief Make sure that other objects do not point to the building object anymore, so it can be removed.
     *
     * Uses static with pointer because shared_from_this() does not always work.
     */
    static void PrepareToBeReleased(std::shared_ptr<Building> &building);
    /*! \brief Simplify the geometry at the outside of the building.
     * Can only be done if there are no inner nodes - otherwise the simplified outer boundary might overlap with the
     * not simplified inner rings.
     *
     * @return the number of points removed
     */
    [[nodiscard]] long SimplifyOuterGeometry(const std::map<long, std::unique_ptr<Node>> &building_nodes, const TileHandler &tile_handler);
    /*! \brief For roofs without inner holes but having neighbours calculate some hints to be used when the final geometry is created.
     * To be able to calc roof hints and for other situations, calculate and set the field in Building::has_neighbours_.
     */
    void CalcNeighboursAndRoofHints(const std::map<long, std::unique_ptr<Node>> &building_nodes, const TileHandler &tile_handler);
    [[nodiscard]] bool HasRoofHint() const {
        return this->roof_hint_ != nullptr;
    }
    /*! \brief Get an existing roof hint - maybe create one if it does not exist yet.
     *
     * @param create_if_missing
     * @return
     */
    RoofHint* GetRoofHint(bool create_if_missing) {
        if (create_if_missing and not this->HasRoofHint()) {
            roof_hint_ = std::make_unique<RoofHint>();
        }
        return roof_hint_.get();
    }
};

/*! \brief checks whether edges, which look like balconies on a building, can be removed from a line.
 * Removes always 4 points at a time. It does not matter whether the balcony faces outwards or inwards
 * the building.
 * Let us assume a building front with nodes 0, 1, .., 5 - where nodes [1, 2, 3, 4] would form a balcony.
 * Then after removing these four points the new front would be [0, 5].
 * To make sure that it was a balcony and we do not remove e.g. something that looks like a staircase,
 * we make sure that neither point 1 or 4 is very distant from the new front -> parameter distance_tolerance_line.
 * Also to make sure it is not a too distinguishing feature by not letting the outer points 2 and 3 be too far
 * away from the new front.
 * Does one simplification at a time and should therefore be called until False is returned.
 */
bool CheckForRemovableBalcony(const geos::geom::Coordinate line_nodes[6], const TileHandler &tile_handler);


std::pair<std::vector<std::shared_ptr<Building>>, node_map_t>
ProcessBuildings(const TileHandler &, const OSMDBDataReader &, TimeLogger &);

class BuildingParent : public Element {
   private:
    const bool simple_3d_;
    std::map<osm_id_t, std::weak_ptr<Building>> children_ {};
    bool inside_tile_ = true;
   public:
    BuildingParent(osm_id_t, bool);
    [[nodiscard]] bool IsSimple3d() const {
        return simple_3d_;
    }
    void AddChild(const std::shared_ptr<Building> &building) {
        children_[building->GetOSMId()] = building;
    }
    void RemoveChild(const std::shared_ptr<Building> &building) {
        children_.erase(building->GetOSMId());
    }
    [[nodiscard]] bool ContainsChild(const std::shared_ptr<Building> &building) const {
        return children_.contains(building->GetOSMId());
    }
    LonLat CalculateMidPointOfAllChildren(const std::map<long, std::unique_ptr<Node>> &building_nodes);
    std::vector<osm_id_t> GetOSMIdOfChildren();
    /*! \brief Check whether there is at least one child with a member role of outline.
     * There can be more than one.
     * @return
     */
    [[nodiscard]] bool HasChildRoleOutline() const {
        for (const auto& it : children_) {
            auto sh_ptr = it.second.lock();
            if (sh_ptr and sh_ptr->IsRoleOutline()) {
                return true;
            }
            sh_ptr.reset();
        }
        return false;
    }
    [[nodiscard]] const std::map<osm_id_t, std::weak_ptr<Building>>* GetChildren() const {
        const std::map<osm_id_t, std::weak_ptr<Building>>* children_pointer = &children_;
        return children_pointer;
    }
    /*! \brief Transfer all children from one parent to another.
     * Once all children have a new parent then there will be no pointers to this one -> no del of children necessary.
     */
    static void MoveChildrenToOtherParent(std::shared_ptr<BuildingParent> &from_parent, std::shared_ptr<BuildingParent> &to_parent);

    void SetOutsideTile() {
        inside_tile_ = false;
    }

    [[nodiscard]] bool IsInsideTile() const {
        return inside_tile_;
    }
};

#endif  // OSM2GEAR_BUILDINGS_H
