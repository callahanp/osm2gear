#include "stg_io.h"

#include <fstream>

#include "parameters.h"
#include "utils.h"

std::string GetSceneryTypeDirectoryName(SceneryType scenery_type) {
    switch(scenery_type) {
        case SceneryType::buildings:
            return "Buildings";
        case SceneryType::roads:
            return "Roads";
        case SceneryType::pylons:
            return "Pylons";
        case SceneryType::details:
            return "Details";
        case SceneryType::trees:
            return "Trees";
        case SceneryType::objects:
            return "Objects";
        case SceneryType::terrain:
            return "Terrain";
        default:
            throw std::logic_error("Missing mapping for SceneryType");
    }
}

STGManager::STGManager(SceneryType scenery_type, LonLat tile_centre): tile_centre_ {tile_centre} {
    stg_directory_path_ = Parameters::Get().PATH_TO_OUTPUT;
    stg_directory_path_.append(GetSceneryTypeDirectoryName(scenery_type));
    stg_directory_path_.append(LocationDirectoryName(tile_centre));
    fs::create_directories(stg_directory_path_);
}

void STGManager::AddTreeList(const std::string &list_name, const std::string &material_name) {
    std::stringstream stream;
    stream << std::fixed << std::setprecision(5);
    stream << "TREE_LIST " << list_name << " " << material_name;
    stream << " " << tile_centre_.lon << " " << tile_centre_.lat << " 0.0";
    entries_.emplace_back(stream.str());
}

void STGManager::Write(const std::string &tile_index) const {
    if (entries_.empty()) {
        return; //no need to write an empty file
    }
    fs::path file_path {stg_directory_path_};
    file_path.append(tile_index + ".stg");
    std::ofstream stg_file {file_path, std::ios::out};
    for (auto & entry : entries_) {
        stg_file << entry << std::endl;
    }
}
