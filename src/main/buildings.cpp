#include <iostream>

#include <geos/geom/CoordinateSequenceFactory.h>

#include "buildings.h"
#include "parameters.h"
#include "../proto/buildings.pb.h"

Building::Building(osm_id_t osm_id, std::unique_ptr<geos::geom::LinearRing> && outer_ring,
                   const tags_t* tags, const refs_t* outer_refs,
                   const TileHandler& tile_handler) : Element(osm_id) {
    for (auto & it : *tags) {
        tags_[it.first] = it.second;
    }
    for (auto & it : *outer_refs) {
        outer_refs_.push_back(it);
    }
    geos::geom::GeometryFactory::Ptr factory = TileHandler::MakeGeometryFactory();
    std::unique_ptr<geos::geom::LinearRing> ring = factory->createLinearRing(outer_ring->getCoordinates());
    outer_polygon_ = factory->createPolygon(std::move(ring));
}

/*! \brief Based on tags determine whether we are interested in the building at all.
 *
 * Meaning: whether we use it in land-use processing and for rendering as a building.
 * Note: While a building can be used in land-use processing, it might get excluded
 * from rendering - because it is too small or we do not yet have a renderer or
 * because it gets rendered in another module (e.g. pylons for chimneys)
 *
 * @param tags
 * @return
 */
bool Building::IsBuildingProcessed(const tags_t* tags) {
    if (ParseIsSmallBuildingDetail(tags, true) or ParseIsSmallBuildingDetail(tags, false)) {
        return false;
    }
    //no indoor and nothing in the underground
    if (ParseIsIndoor(tags) or ParseIsUndergroundBuilding(tags)) {
        return false;
    }
    // wrong tagging cf. https://wiki.openstreetmap.org/wiki/Tag:building%3Dgreenhouse
    // and https://wiki.openstreetmap.org/wiki/Tag:building%3Dglasshouse.
    // As of Aug 2022 only 1700 tagged like this
    if (ParseIsGlasshouse(tags, true) or ParseIsGlasshouse(tags, false)) {
        return false; // wrong tagging
    }
    // will do in different module, but not in built-up areas
    if (ParseIsGreenhouse(tags, true) or ParseIsGreenhouse(tags, false)) {
        return false;
    }
    // not really a building - rendered in module pylons
    if (ParseIsChimney(tags)) {
        return false;
    }
    return true;
}

bool Building::IsBuildingRendered(double random_0_1) const {
    // never skip a building with inner rings (holes)
    if (this->HasHoles()) {
        return true;
    }
    // never skip a building with a parent
    if (this->HasParent()) {
        return true;
    }

    //NB: anything already excluded in IsBuildingProcessed does not need to be excluded again
    // rendered in module pylons
    if (ParseIsStorageTank(GetTags(), true) or ParseIsStorageTank(GetTags(), false)) {
        return false;
    }
    // too small type to be rendered
    if (ParseIsTooSmallBuildingTypeForRendering(GetTags(), true) or
        ParseIsTooSmallBuildingTypeForRendering(GetTags(), false)) {
        return false;
    }
    if (GetArea() < Parameters::Get().BUILDING_MIN_AREA) {
        return false;
    }
    if (not Parameters::Get().OWBB_GENERATE_BUILDINGS) { // it does not make sense to remove and then add again
        if (GetArea() < Parameters::Get().BUILDING_REDUCE_THRESHOLD) {
            if (Parameters::Get().BUILDING_REDUCE_RATE > random_0_1) {
                return false;
            }
        }
    }
    return true;
}

geos::geom::Geometry* Building::GetOuterPolygon() const {
    return outer_polygon_.get();
}

void Building::RefreshOuterPolygon(const std::map<long, std::unique_ptr<Node>> &building_nodes, const TileHandler &tile_handler) {
    std::unique_ptr<geos::geom::LinearRing> outer_ring = Way::LinearRingFromRefs(GetOSMId(), GetOuterRefs(),
                                                                                 building_nodes, tile_handler);
    geos::geom::GeometryFactory::Ptr factory = TileHandler::MakeGeometryFactory();
    std::unique_ptr<geos::geom::LinearRing> ring = factory->createLinearRing(outer_ring->getCoordinates());
    outer_polygon_ = factory->createPolygon(std::move(ring));
}

double Building::GetArea() const {
    return outer_polygon_->getArea();
}

void Building::SetZone(Zone * zone) {
    zone_ = zone;
}

bool Building::HasBuildingAndBuildingPartTag() const {
    // According to OSM tag-info this is the case for less than 2% of building:part
    return tags_.contains(k_building) and tags_.contains(k_building_part);
}

bool Building::IsBuildingPart() const {
    return tags_.contains(k_building_part);
}

void Building::AddInnerRefs(const refs_t* ring_refs) {
    refs_t inner_ring_refs {};
    for (auto ref : *ring_refs) {
        inner_ring_refs.push_back(ref);
    }
    inner_refs_.push_back(inner_ring_refs);
}

void Building::RemoveOuterRef(osm_id_t ref_to_remove) {
    // remove from outer ring
    bool is_first_ref = *outer_refs_.begin() == ref_to_remove;
    auto it = std::remove(outer_refs_.begin(), outer_refs_.end(), ref_to_remove);
    outer_refs_.erase(it, outer_refs_.end());

    // need to handle special situation if ref in first position is removed, because then last has to be added, too.
    if (is_first_ref) {
        outer_refs_.push_back(*outer_refs_.begin());
    }

    //then clean up the shared reference if available
    std::set<std::shared_ptr<Building>> shared_refs_to_remove;
    for (auto &it_pair : shared_refs_) {
        if (it_pair.second.contains(ref_to_remove)) {
            it_pair.second.erase(ref_to_remove);
            if (it_pair.second.empty()) {
                shared_refs_to_remove.insert(it_pair.first);
            }
        }
    }
    for (auto &building : shared_refs_to_remove) {
        shared_refs_.erase(building);
    }
}

std::pair<bool, uint8_t> Building::CheckDistanceNodes(const std::map<long, std::unique_ptr<Node>> &building_nodes) {
    uint8_t number_removed {0};

    //check outer ring - here we try to remove conservatively by first only
    //checking few nodes and then more and more (5->3->1)
    int8_t step {5};
    while (step > 0) {
        //using a while loop because several
        osm_id_t ref_to_remove {0};
        // size() -1 because last is same as first reference
        for(size_t pos = 0; pos < outer_refs_.size() - 1; pos = pos + step) {
            auto next = pos + 1;
            auto sg_0 = building_nodes.at(outer_refs_[pos])->GetSGGeod();
            auto sg_1 = building_nodes.at(outer_refs_[next])->GetSGGeod();
            if (SGGeodesy::distanceM(sg_0, sg_1) < Parameters::Get().BUILDING_MIN_NODE_DISTANCE) {
                // distance too small -> either pos or next should be removed.
                // need to handle 4 situations: (a) pos is not shared with other building -> just remove pos and keep next
                //                              (b) pos is shared but next is not -> just remove next and keep pos
                //                              (c) pos and next are sharing the same building and not others -> remove next
                //                              (d) we take the risk of being too close and see whether it breaks something
                //                              it is hard and brittle to decide what to keep - especially if more than 2 buildings involved
                //                              the probability is very low if several buildings are involved
                std::set<std::shared_ptr<Building>> shared_buildings_at_pos {};
                for (auto &it : shared_refs_) {
                    if (it.second.contains(outer_refs_[pos])) {
                        shared_buildings_at_pos.insert(it.first);
                    }
                }
                if (shared_buildings_at_pos.empty()) {  // case (a)
                    ref_to_remove = outer_refs_[pos];
                    break;
                }
                std::set<std::shared_ptr<Building>> shared_buildings_at_next {};
                for (auto &it : shared_refs_) {
                    if (it.second.contains(outer_refs_[next])) {
                        shared_buildings_at_next.insert(it.first);
                    }
                }
                if (shared_buildings_at_next.empty()) {  // case (b)
                    ref_to_remove = outer_refs_[next];
                    break;
                }
                if (shared_buildings_at_pos.size() == 1 and shared_buildings_at_next.size() == 1 and
                    shared_buildings_at_pos == shared_buildings_at_next) {  // case (c)
                    this->RemoveOuterRef(outer_refs_[next]);
                    auto other_building = *shared_buildings_at_next.begin();
                    other_building->RemoveOuterRef(outer_refs_[next]);
                    break;
                }
            }
        }
        if (ref_to_remove != 0) {
            this->RemoveOuterRef(ref_to_remove);
            number_removed++;
        } else {
            step -= 2;
        }
    }
    if (GetOuterRefsBaseSize() < 3) {
        return std::make_pair(false, number_removed);
    }

    //check inner rings
    for (auto &inner_ring_refs : inner_refs_) {
        bool keep_checking {true};
        while (keep_checking) {
            // using a while loop because several
            int16_t pos_to_remove{-1};
            for (size_t pos = 0; pos < inner_ring_refs.size() - 1; ++pos) {
                auto next = pos + 1;
                auto sg_0 = building_nodes.at(inner_ring_refs[pos])->GetSGGeod();
                auto sg_1 = building_nodes.at(inner_ring_refs[next])->GetSGGeod();
                if (SGGeodesy::distanceM(sg_0, sg_1) < Parameters::Get().BUILDING_MIN_NODE_DISTANCE) {
                    pos_to_remove = pos;
                    break;
                }
            }
            if (pos_to_remove >= 0) {
                inner_ring_refs.erase(inner_ring_refs.begin() + pos_to_remove);
                number_removed++;
            } else {
                keep_checking = false;
            }
        }
    }
    return std::make_pair(true, number_removed);
}

void Building::SetParentAndSetAsChild(const std::shared_ptr<Building> &building, const std::shared_ptr<BuildingParent> &parent) {
    building->SetParent(parent);
    parent->AddChild(building);
}

void Building::RemoveParent(std::shared_ptr<Building> &building) {
    if (building->HasParent()) {
        building->GetParent()->RemoveChild(building);
    }
    building->SetParentNull();
}

void Building::PrepareToBeReleased(std::shared_ptr<Building> &building) {
    if (building->HasZone()) {
        building->GetZone()->UnRelateBuilding(building);
        building->SetZoneNull();
    }
    building->RemoveParent(building);
}

bool CheckForRemovableBalcony(const geos::geom::Coordinate line_nodes[6], const TileHandler &tile_handler) {
    geos::geom::GeometryFactory::Ptr factory = TileHandler::MakeGeometryFactory();
    std::vector<geos::geom::Coordinate> line_coords;
    line_coords.emplace_back(line_nodes[0]);
    line_coords.emplace_back(line_nodes[5]);
    const geos::geom::CoordinateSequenceFactory* coord_factory = factory->getCoordinateSequenceFactory();
    std::unique_ptr<geos::geom::LineString> line = factory->createLineString(coord_factory->create(std::move(line_coords)));

    //check the distance of the points from the baseline between 0 and 5
    if (factory->createPoint(line_nodes[1])->distance(line.get()) > Parameters::Get().BUILDING_BALCONY_TOLERANCE_BASE) {
        return false;
    }
    if (factory->createPoint(line_nodes[4])->distance(line.get()) > Parameters::Get().BUILDING_BALCONY_TOLERANCE_BASE) {
        return false;
    }
    if (factory->createPoint(line_nodes[2])->distance(line.get()) > Parameters::Get().BUILDING_BALCONY_TOLERANCE_RAILING) {
        return false;
    }
    if (factory->createPoint(line_nodes[3])->distance(line.get()) > Parameters::Get().BUILDING_BALCONY_TOLERANCE_RAILING) {
        return false;
    }

    //make sure that the geometry is most probably a near rectangle balcony
    std::vector<geos::geom::Coordinate> base_coords;
    base_coords.emplace_back(line_nodes[1]);
    base_coords.emplace_back(line_nodes[4]);
    std::unique_ptr<geos::geom::LineString> base_line = factory->createLineString(coord_factory->create(std::move(base_coords)));
    std::vector<geos::geom::Coordinate> railing_coords;
    railing_coords.emplace_back(line_nodes[2]);
    railing_coords.emplace_back(line_nodes[3]);
    std::unique_ptr<geos::geom::LineString> railing_line = factory->createLineString(coord_factory->create(std::move(railing_coords)));
    if (not CheckWithinRelativeToleranceOfMax(base_line->getLength(), railing_line->getLength(), Parameters::Get().BUILDING_BALCONY_TOLERANCE_DIFF)) {
        return false;
    }
    //make sure sequence of points on the front line (solves problem in osm-id=94855046, where point 4 ..595 is after
    // point 5 ..309 and therefore after removal point ..309 and ..940 would be on a line, which results in an
    //illegal geometry).
    if (line_nodes[0].distance(line_nodes[5]) <= line_nodes[0].distance(line_nodes[4]) or
        line_nodes[0].distance(line_nodes[5]) <= line_nodes[5].distance(line_nodes[1])) {
        return false;
    }
    return true;
}

long Building::SimplifyOuterGeometry(const std::map<long, std::unique_ptr<Node>> &building_nodes, const TileHandler &tile_handler) {
    if (HasHoles()) {
        return 0;
    }
    long count_simplified {0};
    bool keep_searching {true};
    while (keep_searching) {
        // after removing 4 points there should still be a triangle left
        if (this->GetOuterRefsBaseSize() < 7) {
            break;
        }
        std::set<osm_id_t> refs_shared {};
        for (auto &building : shared_refs_) {
            for (auto &ref : building.second) {
                refs_shared.insert(ref);
            }
        }
        // check a line of 6 consecutive nodes for balconies for each possible starting point
        keep_searching = false;
        for (size_t ref_index = 0; ref_index < outer_refs_.size() - 1; ++ref_index) {
            osm_id_t line_refs[6];
            geos::geom::Coordinate line_nodes[6];
            bool does_not_have_ref_shared {true};
            for (int node_index = 0; node_index < 6; ++node_index) {
                auto safe_ref_index = ref_index + node_index;
                if (safe_ref_index > GetOuterRefsBaseSize() - 1) {
                    safe_ref_index = safe_ref_index - GetOuterRefsBaseSize();
                }
                if (node_index != 0 and node_index != 5) {
                    if (refs_shared.contains(outer_refs_[safe_ref_index])) {
                        does_not_have_ref_shared = false;
                        break;
                    }
                }
                line_refs[node_index] = outer_refs_[safe_ref_index];
                line_nodes[node_index] = tile_handler.ToLocal(building_nodes.at(outer_refs_[safe_ref_index])->GetLonLat());
            }
            if (does_not_have_ref_shared and CheckForRemovableBalcony(line_nodes, tile_handler)) {
                for (int node_index = 0;  node_index < 6; ++node_index) {
                    if (node_index != 0 and node_index != 5) {
                        this->RemoveOuterRef(line_refs[node_index]);
                    }
                }
                count_simplified += 1;
                keep_searching = true;
                break;
            }
        }
    }
    return count_simplified;
}

void Building::CalcRidgeOrientation(const std::map<long, std::unique_ptr<Node>> &building_nodes, const TileHandler &tile_handler) {
    double longest_distance {0.0};

    for (auto &it : shared_refs_) {
        if (it.second.size() >= 2) {
            osm_id_t prev {0};
            for (size_t pos = 0; pos < outer_refs_.size() - 1; ++pos) {
                if (it.second.contains(outer_refs_[pos])) {
                    if (prev != 0) { // now we have 2 consecutive points shared with another building
                        auto sg_0 = building_nodes.at(prev)->GetSGGeod();
                        auto sg_1 = building_nodes.at(outer_refs_[pos])->GetSGGeod();
                        auto distance = SGGeodesy::distanceM(sg_0, sg_1);
                        if (distance > longest_distance) {
                            longest_distance = distance;
                            auto roof_hint = GetRoofHint(true);
                            roof_hint->SetRidgeOrientation(SGGeodesy::courseDeg(sg_0, sg_1));
                        }
                    }
                    prev = outer_refs_[pos];
                } else {
                    prev = 0;
                }
            }
        }
    }
}

constexpr double DIFF_ANGLE {20.};

void Building::CalcInnerNodeLShapeRoof(const std::map<long, std::unique_ptr<Node>> &building_nodes, const TileHandler &tile_handler) {
   // exclude situations where we will not act at all.
    if (GetOuterRefsBaseSize() < 4 or GetOuterRefsBaseSize() > 6) {
        return;
    }
    if (shared_refs_.size() > 2) {
        return;
    }

    // make sure that the shared sides of other buildings are a whole side and not just part of a side of another
    // building and that exactly 2 points are shared.
    std::set<std::shared_ptr<Building>> valid_neighbours;
    for (auto &it : shared_refs_) {
        if (it.second.size() == 2) {
            bool prev_found {false};
            for (size_t pos = 0; pos < outer_refs_.size() - 1; ++pos) {
                if (it.second.contains(outer_refs_[pos])) {
                    if (prev_found) {
                        valid_neighbours.insert(it.first);
                    }
                    prev_found = true;
                } else {
                    prev_found = false;
                }
            }
        }
    }
    std::set<osm_id_t> common_refs_both_neighbours;  // will only have values if there are 2 neighbours with 2 refs and there is a common ref
    if (valid_neighbours.size() == 2) {
        std::set_intersection(shared_refs_[*valid_neighbours.begin()].begin(), shared_refs_[*valid_neighbours.begin()].end(),
                              shared_refs_[*valid_neighbours.end()].begin(), shared_refs_[*valid_neighbours.end()].end(),
                              std::inserter(common_refs_both_neighbours, common_refs_both_neighbours.begin()));
    }
    // case building with 4 nodes and 2 neighbours, who share a node
    if (GetOuterRefsBaseSize() == 4 and not common_refs_both_neighbours.empty()) {
        auto roof_hint = GetRoofHint(true);
        roof_hint->SetInnerNode(tile_handler.ToLocal(building_nodes.at(*common_refs_both_neighbours.begin())->GetLonLat()));
        return;
    }
    if ((GetOuterRefsBaseSize() == 5 or GetOuterRefsBaseSize() == 6) and
        valid_neighbours.size() <= 2 and common_refs_both_neighbours.empty()) {
        //find the inner node by iterating over the outer_refs_ and then doing geometry and neighbourhood analysis
        for (std::size_t i = 0; i < outer_refs_.size() - 1; ++i) {
            auto prev_index = i == 0? outer_refs_.size() - 2 : i - 1;
            auto next_index = i == outer_refs_.size() - 2? 0 : i + 1;
            auto sg_prev = building_nodes.at(outer_refs_[prev_index])->GetSGGeod();
            auto sg_center = building_nodes.at(outer_refs_[i])->GetSGGeod();
            auto sg_next = building_nodes.at(outer_refs_[next_index])->GetSGGeod();
            auto delta = CalcDeltaBearing(SGGeodesy::courseDeg(sg_prev, sg_center),
                                          SGGeodesy::courseDeg(sg_center, sg_next));
            // if we have 5 nodes, then the inner node must be on the "straight" line with 3 nodes and have a neighbour
            if (GetOuterRefsBaseSize() == 5 and fabs(delta) < DIFF_ANGLE and not valid_neighbours.empty()) {
                for (auto &building : valid_neighbours){
                    if (shared_refs_[building].contains(outer_refs_[i])) {
                        auto roof_hint = GetRoofHint(true);
                        roof_hint->SetInnerNode(tile_handler.ToLocal(building_nodes.at(outer_refs_[i])->GetLonLat()));
                        roof_hint->SetNodeBeforeInnerIsShared(shared_refs_[building].contains(outer_refs_[prev_index]));
                        return;
                    }
                }
            }
            // if we have 6 nodes, then the inner node must be in a corner without any neighbours
            // and the corner is around 90 degs in clock direction
            if (GetOuterRefsBaseSize() == 6 and delta > 0 and fabs(delta - 90) < DIFF_ANGLE) {
                bool has_neighbour {false};
                for (auto &building : valid_neighbours) {
                    if (shared_refs_[building].contains(outer_refs_[i])) {
                        has_neighbour = true;
                        break;
                    }
                }
                if (not has_neighbour) {
                    auto roof_hint = GetRoofHint(true);
                    roof_hint->SetInnerNode(tile_handler.ToLocal(building_nodes.at(outer_refs_[i])->GetLonLat()));
                    return;
                }
            }
        }
    }
}

void Building::CalcNeighboursAndRoofHints(const std::map<long, std::unique_ptr<Node>> &building_nodes, const TileHandler &tile_handler) {
    CalcHasNeighbours();
    if (HasHoles() or not HasNeighbours()) {
        return;
    }
    CalcRidgeOrientation(building_nodes, tile_handler);
    CalcInnerNodeLShapeRoof(building_nodes, tile_handler);
}


/*! \brief Processes the members in a Simple3D relationship in order to make sure that a building outline exists.
 *
 * We need a 2 step process here, because a member of a Simple3D can already be part of another Simple3D or a
 * multipolygon. If the member is part of another Simple3D and that Simple3D has a true tagged outline, then we leave it
 * alone (first one wins). If the parent is not a Simple3D (it is a multipolygon with >= 2 outer), then we move it over.
 * Similarly: if this relation does not have an outline but there is a member's parent with outline, then we move all
 * members to that one.
 */
void ProcessSimple3dBuilding(const std::unique_ptr<Relation> &relation,
                             std::map<osm_id_t , std::shared_ptr<Building>> &buildings) {
    // first let us check all members and this relation to look for parents and outlines
    bool relation_has_outline = false;
    osm_id_t other_parent_with_outline{0};
    bool use_other_parent {false};
    for (auto & member : *relation->GetMembers()) {
        if (member->GetType() == MemberType::way) {
            if (buildings.contains(member->GetRef())) {
                std::shared_ptr<Building> related_building = buildings.at(member->GetRef());
                if (related_building->HasParent()) {
                    auto related_parent = related_building->GetParent();
                    if (related_parent->HasChildRoleOutline()) { // if there is more than one the last wins
                        other_parent_with_outline = related_building->GetOSMId();
                    }
                }
                if (member->GetRole() == MemberRole::outline) {
                    relation_has_outline = true;
                }
            }
        }
    }
    std::shared_ptr<BuildingParent> parent;
    if (not relation_has_outline and other_parent_with_outline > 0) {
        use_other_parent = true;
        parent = buildings.at(other_parent_with_outline)->GetParent();
    } else {
        parent = std::make_shared<BuildingParent>(IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::building_relation), true);
    }
    for (auto & member : *relation->GetMembers()) {
        if (member->GetType() == MemberType::way) {
            if (buildings.contains(member->GetRef())) {
                std::shared_ptr<Building> related_building = buildings.at(member->GetRef());
                if (use_other_parent and related_building->GetOSMId() == other_parent_with_outline) {
                    continue;  // already using the correct parent;
                } else if (related_building->HasParent()) {
                    BuildingParent::MoveChildrenToOtherParent(related_building->GetParent(), parent);
                } else {
                    Building::SetParentAndSetAsChild(related_building, parent);
                }
                if (member->GetRole() == MemberRole::outline) {
                    related_building->SetAsRoleOutline();
                }
            }
        }
    }
}

/*!
Processes the members in a multipolygon relationship. Returns the number of buildings actually created.
If there are several members of type 'outer', then multiple buildings are created.
Also, if there are several 'outer', then these buildings are combined in a parent.
 */
void ProcessMultipolygonBuilding(const TileHandler &tile_handler,
                                 const std::unique_ptr<Relation> &relation,
                                 std::map<osm_id_t , std::shared_ptr<Building>> &buildings,
                                 const OSMReadRelationsResult &rel_result,
                                 const node_map_t &all_nodes) {
    std::vector<std::shared_ptr<Way>> outer_ways {};
    std::vector<std::shared_ptr<Way>> outer_ways_multiple {}; // # outer ways, where multiple ways form one or more closed rings
    std::vector<std::shared_ptr<Way>> inner_ways {};
    std::vector<std::shared_ptr<Way>> inner_ways_multiple {}; // # inner ways (holes), where multiple ways form one or more closed rings
    // find relationships
    for (auto & member : *relation->GetMembers()) {
        if (member->GetType() == MemberType::way) {
            if (rel_result.rel_ways_dict.contains(member->GetRef())) {
                auto way = rel_result.rel_ways_dict.at(member->GetRef());
                // check whether we really want to have this member
                if (ParseIsUndergroundBuilding(way->GetTags())) {
                    continue ;
                }
                if (member->GetRole() == MemberRole::outer or member->GetRole() == MemberRole::inner) {
                    if (buildings.contains(way->GetOSMId())) {
                        // because the member way has already been processed as a normal way, we need to remove it again.
                        // Otherwise, we might get flickering due to two buildings on top of each other
                        auto existing_building = buildings.at(way->GetOSMId());
                        Building::RemoveParent(existing_building); // should be extremely seldom, because could only be from a prev relationship multipolygon
                        buildings.erase(way->GetOSMId());
                    }
                    auto refs = way->GetRefs();
                    if (refs->front() == refs->back()) {
                        if (member->GetRole() == MemberRole::outer) {
                            outer_ways.push_back(way);
                            BOOST_LOG_TRIVIAL(debug) << "add way outer " << way->GetOSMId();
                        } else {
                            inner_ways.push_back(way);
                            BOOST_LOG_TRIVIAL(debug) << "add way inner " << way->GetOSMId();
                        }
                    } else {
                        if (member->GetRole() == MemberRole::outer) {
                            outer_ways_multiple.push_back(way);
                            BOOST_LOG_TRIVIAL(debug) << "add way outer multiple " << way->GetOSMId();
                        } else {
                            inner_ways_multiple.push_back(way);
                            BOOST_LOG_TRIVIAL(debug) << "add way inner multiple " << way->GetOSMId();
                        }
                    }
                }
            }
        }
    }

    // Process multiple and add to outer_ways/inner_ways as whole rings
    auto closed_ways = ClosedWaysFromMultipleWays(inner_ways_multiple, true);
    std::ranges::move(closed_ways, std::back_inserter(inner_ways));
    closed_ways = ClosedWaysFromMultipleWays(outer_ways_multiple, true);
    std::ranges::move(closed_ways, std::back_inserter(outer_ways));

    // Create polygons to allow some geometry analysis
    std::map<osm_id_t, std::unique_ptr<geos::geom::Polygon>> polygons {};
    for (const auto &way : outer_ways) {
        polygons[way->GetOSMId()] = way->PolygonFromOSMWay(all_nodes, tile_handler);
    }
    for (const auto &way : inner_ways) {
        polygons[way->GetOSMId()] = way->PolygonFromOSMWay(all_nodes, tile_handler);
    }

    // exclude inner islands for outer_ways (should never happen, but apparently does)
    auto copy_outer_ways = outer_ways;
    auto it = outer_ways.begin();
    bool remove_me;
    while (it != outer_ways.end()) {
        remove_me = false;
        for (auto &copy : copy_outer_ways) {
            if (it->get()->GetOSMId() != copy->GetOSMId()) {
                if(polygons[it->get()->GetOSMId()]->within(polygons[copy->GetOSMId()].get())) {
                    remove_me = true;
                    break;
                }
            }
        }
        if (remove_me) {
            it = outer_ways.erase(it);
        } else {
            ++it;
        }
    }

    // create the actual building
    std::shared_ptr<BuildingParent> building_parent;
    if (outer_ways.size() > 1) {
        building_parent = std::make_shared<BuildingParent>(IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::building_relation), false);
    }
    for (auto &outer_way : outer_ways) {
        auto tags = CombineTags(relation->GetTags(), outer_way->GetTags());
        try {
            std::unique_ptr<geos::geom::LinearRing> outer_ring = Way::LinearRingFromRefs(outer_way->GetOSMId(),
                                                                                         outer_way->GetRefs(),
                                                                                         all_nodes,
                                                                                         tile_handler);
            std::shared_ptr<Building> building = std::make_shared<Building>(outer_way->GetOSMId(),
                                                                            std::move(outer_ring),
                                                                            &tags, outer_way->GetRefs(),
                                                                            tile_handler);
            for (auto &inner_way : inner_ways) {
                if (polygons[inner_way->GetOSMId()]->within(polygons[outer_way->GetOSMId()].get())) {
                    // we want inner rings to be visible from a plane
                    // in real world data (e.g. https://www.openstreetmap.org/way/380854827) points can be very close,
                    // so that CheckDistanceNodes(...) might remove all but one node, which would result
                    // in an invalid inner ring
                    if (polygons[inner_way->GetOSMId()]->getArea() > Parameters::Get().BUILDING_INNER_MIN_AREA) {
                        building->AddInnerRefs(inner_way->GetRefs());
                    }
                }
            }
            if (building_parent) {
                Building::SetParentAndSetAsChild(building, building_parent);
            }
            buildings[building->GetOSMId()] = std::move(building);
        } catch (InvalidGeometryFromOSM& e) {
            LogDebugWarning(e.what());
        }
    }
}

/*!
    Adds buildings based on relation tags. There are two scenarios: multipolygon buildings and Simple3D tagging.
    Only multipolygon and simple 3D buildings are implemented currently.
    The added buildings go into parameter my_buildings.
    Multipolygons:
        * see FIXMEs in building_lib whether inner rings etc. actually are supported.
        * Outer rings out of multiple parts are supported.
        * Islands are removed

    There is actually a third scenario from Simple3D buildings, where the "building" and "building:part" are not
    connected with a relation. This is handled separately in _process_building_parts()

    See also http://wiki.openstreetmap.org/wiki/Key:building:part
    See also http://wiki.openstreetmap.org/wiki/Buildings

    === Simple multipolygon buildings ===
    http://wiki.openstreetmap.org/wiki/Relation:multipolygon

    <relation id="4555444" version="1" timestamp="2015-02-03T19:59:54Z" uid="505667" user="Bullroarer"
    changeset="28596876">
    <member type="way" ref="326274370" role="outer"/>
    <member type="way" ref="326274316" role="inner"/>
    <tag k="type" v="multipolygon"/>
    <tag k="building" v="yes"/>
    </relation>

    === 3D buildings ===
    See also http://wiki.openstreetmap.org/wiki/Relation:building (has also examples and demo areas at end)
    http://taginfo.openstreetmap.org/relations/building#roles
    http://wiki.openstreetmap.org/wiki/Simple_3D_buildings

    Example of church: http://www.openstreetmap.org/relation/3792630
    <relation id="3792630" ...>
    <member type="way" ref="23813200" role="outline"/>
    <member type="way" ref="285981235" role="part"/>
    <member type="way" ref="285981232" role="part"/>
    <member type="way" ref="285981237" role="part"/>
    <member type="way" ref="285981234" role="part"/>
    <member type="way" ref="285981236" role="part"/>
    <member type="way" ref="285981233" role="part"/>
    <member type="way" ref="285981231" role="part"/>
    <member type="node" ref="1096083389" role="entrance"/>
    <tag k="note" v="The sole purpose of the building relation is to group the individual building:part members.
    The tags of the feature are on the building outline."/>
    <tag k="type" v="building"/>
    </relation>
 */

void ProcessOSMRelations(const TileHandler &tile_handler,
                         std::map<osm_id_t , std::shared_ptr<Building>> &buildings,
                         const OSMReadRelationsResult &rel_result,
                         const node_map_t &all_nodes) {
    uint32_t processed_simple_3d {};
    uint32_t processed_multipoly_buildings {};
    // because a multipolygon can be part of a Simple3D building (often as outline)
    // they should be processed before the Simple3D are processed
    for (auto &it : rel_result.relations_dict) {
        if (it.second->GetTags()->contains(k_type) and it.second->GetTags()->at(k_type) == v_multipolygon) {
            // one building made of multiple polygons (inner and outer)
            try {
                ProcessMultipolygonBuilding(tile_handler, it.second, buildings,
                                            rel_result, all_nodes);
                processed_multipoly_buildings++;
            } catch (std::exception &e) {
                BOOST_LOG_TRIVIAL(warning) << "There was an error in ProcessMultipolygonBuilding: " << e.what();
            }
        }
    }
    for (auto &it : rel_result.relations_dict) {
        if (it.second->GetTags()->contains(k_type) and it.second->GetTags()->at(k_type) == v_building) {
            try {
                // one building made of multiple building parts
                // only v_building exists, building_part or building::part are not allowed and do almost not exist
                ProcessSimple3dBuilding(it.second, buildings);
                processed_simple_3d++;
            } catch (std::exception &e) {
                BOOST_LOG_TRIVIAL(warning) << "There was an error in ProcessSimple3dBuilding: " << e.what();
            }
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Processed " << processed_multipoly_buildings << " multipolygon building relations";
    BOOST_LOG_TRIVIAL(info) << "Processed " << processed_simple_3d << " Simple3d building relations";
}

/*! \brief Finds building:parts without parents and tries to assign to candidates.
 * The assumption is that there are relatively few building:parts and outlines per tile and
 * therefore there is no need to do optimisation due to looping.
 * @param tile_handler
 * @param buildings
 * @param all_nodes
 */
void MatchBuildingPartsWithoutParents(const TileHandler &tile_handler,
                                        std::map<osm_id_t , std::shared_ptr<Building>> &buildings,
                                        const node_map_t &all_nodes) {
    std::vector<std::shared_ptr<Building>> outline_candidates{};
    for (auto &it : buildings) {
        if (it.second->HasParent() and it.second->IsRoleOutline()) {
            outline_candidates.push_back(it.second);
        }
    }
    long matched_building_parts{};
    long total_building_parts_without_parents{};
    for (auto &it : buildings) {
        if (it.second->IsBuildingPart() and not it.second->HasParent()) {
            total_building_parts_without_parents++;
            for (auto &outline_candidate: outline_candidates) {
                if (outline_candidate->GetOuterPolygon()->contains(it.second->GetOuterPolygon()) or outline_candidate->GetOuterPolygon()->intersects(it.second->GetOuterPolygon())) {
                    auto parent = outline_candidate->GetParent();
                    Building::SetParentAndSetAsChild(it.second, parent);
                    matched_building_parts++;
                    break;
                }
            }
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Matched " << matched_building_parts << " out of " << total_building_parts_without_parents << " building parts without parents";
}

/*! \brif Removes buildings with overlap to Simple3D buildings due to special modelling in OSM -> reduce flickering.
 * In many situation complex buildings are modelled as relations in OSM. And then one "building" is added to the
 * relationship as "outline". However, if we keep the outline, then we get flickering, because the outline
 * would in o2c be built around the building_parts.
 * E.g. https://www.openstreetmap.org/query?lat=21.31293&lon=-157.85991 -> Honolulu Park Place:
 * https://www.openstreetmap.org/way/500644307 building outline overlaps with https://www.openstreetmap.org/relation/7301766
 * relation, which has 3 ways. The relation with 3 ways models the building better than the building (outline),
 * however the 500644307 has better tags.
 * Also https://www.openstreetmap.org/query?lat=21.30903&lon=-157.86345 -> Harbour Court
 * https://www.openstreetmap.org/way/500162714 building is part as outline
 * of the https://www.openstreetmap.org/relation/7302050 relation.
 * Also https://www.openstreetmap.org/relation/7331962 relation (Hawaii Pacific University)
 *
 * Note that sometimes the combination of the floor plan of all relations could be larger than the building.
 *
 * The outline's tags are migrated to the parent.
 */
void RemoveBuildingsWithParentsAndRoleOutline(std::map<osm_id_t , std::shared_ptr<Building>> &buildings) {
    auto itr = buildings.begin();
    while (itr != buildings.end()) {
        if (itr->second->IsRoleOutline() and itr->second->HasParent()) {
            auto parent = itr->second->GetParent();
            for (const auto& it : *itr->second->GetTags()) {
                auto tag_value = it.second.substr();
                parent->AddTag(it.first, tag_value);
            }
            Building::RemoveParent(itr->second);
            itr = buildings.erase(itr);
        } else {
            ++itr;
        }
    }
}

std::pair<std::vector<std::shared_ptr<Building>>, node_map_t>
ProcessBuildings(const TileHandler &tile_handler, const OSMDBDataReader &osm_reader,
                 TimeLogger &time_logger) {
    auto result = osm_reader.FetchWaysFromKeys({k_building, k_building_part});
    auto rel_result = osm_reader.FetchRelationsBuildings();
    std::map<osm_id_t, std::shared_ptr<Building>> buildings{};
    node_map_t all_nodes{};
    all_nodes.merge(result.nodes_dict);
    all_nodes.merge(rel_result.rel_nodes_dict);

    // remove those buildings, which will not be rendered and not used in land-use processing
    for (auto itr = result.ways_dict.cbegin();
         itr != result.ways_dict.cend() /* not hoisted */; /* no increment */) {
        if (!Building::IsBuildingProcessed(itr->second->GetTags())) {
            result.ways_dict.erase(itr++);
        } else {
            ++itr;
        }
    }
    // do the simple buildings based on ways
    for (auto & it : result.ways_dict) {
        try {
            std::unique_ptr<geos::geom::LinearRing> outer_ring = Way::LinearRingFromRefs(it.second->GetOSMId(),
                                                                                         it.second->GetRefs(),
                                                                                         all_nodes,
                                                                                         tile_handler);
            std::shared_ptr<Building> building = std::make_shared<Building>(it.second->GetOSMId(),
                                                                            std::move(outer_ring),
                                                                            it.second->GetTags(), it.second->GetRefs(),
                                                                            tile_handler);
            buildings[building->GetOSMId()] = std::move(building);
        } catch (InvalidGeometryFromOSM& e) {
            LogDebugWarning(e.what());
        }
    }
    time_logger.Log("Time used in seconds for processing simple OSM buildings");
    BOOST_LOG_TRIVIAL(info) << buildings.size() << " buildings after simple buildings based on ways";

    ProcessOSMRelations(tile_handler, buildings, rel_result, all_nodes);
    time_logger.Log("Time used in seconds for processing OSM relations for buildings");
    BOOST_LOG_TRIVIAL(info) << buildings.size() << " buildings after buildings based on relations";

    MatchBuildingPartsWithoutParents(tile_handler, buildings, all_nodes);
    time_logger.Log("Time used in seconds for processing building parts");
    BOOST_LOG_TRIVIAL(info) << buildings.size() << " buildings after processing building parts";

    // According to https://wiki.openstreetmap.org/wiki/Simple_3D_buildings#Building_outlines the outline is not
    // used for rendering. Therefore, it is omitted from the parent/child relationship and removed.
    // However, we can first do it now, because we needed the outlines for matching of building parts
    RemoveBuildingsWithParentsAndRoleOutline(buildings);
    BOOST_LOG_TRIVIAL(info) << buildings.size() << " buildings after removing buildings with role outline";

    //now migrate from map to vector for convenience
    std::vector<std::shared_ptr<Building>> v_buildings {};
    v_buildings.reserve(buildings.size());
    for (auto &it : buildings) {
        v_buildings.push_back(it.second);
    }
    BOOST_LOG_TRIVIAL(info) << "Buildings found: " << buildings.size();
    return std::make_pair(std::move(v_buildings), std::move(all_nodes));
}

BuildingParent::BuildingParent(osm_id_t osm_id, bool simple_3d) : simple_3d_ {simple_3d}, Element(osm_id) {}

void BuildingParent::MoveChildrenToOtherParent(std::shared_ptr<BuildingParent> &from_parent, std::shared_ptr<BuildingParent> &to_parent) {
    //we need a temp_ptr because once all children have been moved, then no building would point
    //back to this parent and the ref count would go to 0 while looping, which
    //results in a SIGSEGV. One the loop is done and this method goes out of
    //scope, then the from_parent will also be deallocated.
    auto temp_ptr = from_parent;
    for (auto& it : *from_parent->GetChildren()) {
        auto sh_ptr = it.second.lock();
        if (sh_ptr) {
            Building::SetParentAndSetAsChild(sh_ptr, to_parent);
        }
        sh_ptr.reset();
    }
    temp_ptr.reset();
}

LonLat BuildingParent::CalculateMidPointOfAllChildren(const std::map<long, std::unique_ptr<Node>> &building_nodes) {
    LonLat average {};
    int counter {};
    for (auto &it : children_) {
        auto sh_ptr = it.second.lock();
        if (sh_ptr) {
            auto lon_lat =
                building_nodes.at(sh_ptr->GetOuterRefs()->at(0))->GetLonLat();
            average.lon += lon_lat.lon;
            average.lat += lon_lat.lat;
            counter++;
        }
    }
    average.lon /= counter;
    average.lat /= counter;
    return average;
}

std::vector<osm_id_t> BuildingParent::GetOSMIdOfChildren() {
    std::vector<osm_id_t> ids {};
    ids.reserve(children_.size());
    for (auto &it : children_) {
        ids.push_back(it.first);
    }
    return ids;
}
