#ifndef OSM2GEAR_STG_IO_H
#define OSM2GEAR_STG_IO_H

#include <filesystem>
#include <string>

#include "utils.h"

namespace fs = std::filesystem;

enum class STGVerbType {  // must be the same as actual string in lowercase in FGFS
    object_shared = 1,
    object_static = 2,
    object_building_mesh_rough = 3,
    object_building_mesh_detailed = 4,
    object_road_rough = 5,
    object_road_detailed = 6,
    object_railway_rough = 7,
    object_railway_detailed = 8,
    building_list = 10
};

enum class SceneryType {
    buildings = 1,
    roads = 2,
    pylons = 3,
    details = 4,
    trees = 5,
    //other types
    objects = 11,
    terrain = 12
};

/**
 * Manages STG objects. Knows about scenery path and handles the scenery type specific stg-file.
 */
class STGManager {
   private:
    LonLat tile_centre_;
    fs::path stg_directory_path_;
    std::vector<std::string> entries_ {};
   public:
    explicit STGManager(SceneryType scenery_type, LonLat tile_centre);
    /*! \brief Adds a TREE_LIST to the stg_file
     */
    void AddTreeList(const std::string &list_name, const std::string &material_name);
    std::filesystem::path GetSTGDirectoryPath() {
        return stg_directory_path_;
    }
    /*! \brief Write the stg-file if it contains at least one entry.
     */
    void Write(const std::string &tile_index) const;
};

#endif  // OSM2GEAR_STG_IO_H
