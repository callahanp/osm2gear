#include "plotting.h"

#include <cassert>
#include <fstream>

#include "geos/io/WKTWriter.h"

#include "buildings.h"
#include "enumerations.h"


std::string TransformSVGPointsFromWKTPoints(std::string &wkt_points) {
    int position, xy_pos;
    const std::string wkt_pt_separator {", "};  //between points
    const std::string wkt_x_y_separator {" "};
    const std::string svg_x_y_separator {","};
    const std::string svg_pt_separator {" "};
    std::string token;
    std::string token_x, token_y;
    double y;
    std::string points {};
    bool is_first = true;
    while ((position = wkt_points.find(wkt_pt_separator)) != std::string::npos) {
        token = wkt_points.substr(0, position);
        xy_pos = token.find(wkt_x_y_separator);
        token_x = token.substr(0, xy_pos);
        token_y = token.substr(xy_pos - 1 + wkt_x_y_separator.length());
        y = stod(token_y);
        if (is_first) {
            is_first = false;
        } else {
            points.append(svg_pt_separator);
        }
        points.append(token_x).append(svg_x_y_separator);
        points.append(std::to_string(-1*y));  //the svg coordinate system has positive y values downwards
        wkt_points.erase(0, position + wkt_pt_separator.length());
    }
    return points;
}

/*
 * A WKT representation of a polygon: POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10)) or POLYGON ((35 10, ...), (20 30, ...))
 * SVG equivalent: <polygon points="30,10 40,40 20,40 10,20 30,10" style="fill:lime;stroke:purple;stroke-width:1" />
 */
std::vector<std::string> SVGPointsFromWKTPolygon(std::string &wkt_string) {
    std::string separator = "), (";
    //erase the beginning and end
    wkt_string.erase(0, 10);
    wkt_string.erase(wkt_string.end() - 2, wkt_string.end());
    //split polygon so we have outer and inner rings
    std::vector<std::string> points {};
    size_t position;
    std::string token;
    position = wkt_string.find(separator);
    if (position == std::string::npos) {
        points.push_back(TransformSVGPointsFromWKTPoints(wkt_string));
    } else {
        while ((position = wkt_string.find(separator)) != std::string::npos) {
            token = wkt_string.substr(0, position);
            points.push_back(TransformSVGPointsFromWKTPoints(token));
            wkt_string.erase(0, position + separator.length());
        }
    }
    return points;
}

void DrawBuildings(geos::io::WKTWriter &wkt_writer, std::ofstream &svg_file, const std::string& colour,
                   const std::vector<std::shared_ptr<Building>> &buildings) {
    std::string wkt;
    for (auto & building : buildings) {
        assert(building->GetOuterPolygon()->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON);
        wkt = wkt_writer.write(building->GetOuterPolygon());
        auto svg_points = SVGPointsFromWKTPolygon(wkt);
        svg_file << "<polygon points=\"" << svg_points[0] << "\" style=\"fill:";
        svg_file << colour << ";stroke-width:0\" />" << std::endl;
    }
}

void DrawLitAreas(geos::io::WKTWriter &wkt_writer, const std::string &svg_start,
                  const std::vector<std::unique_ptr<geos::geom::Geometry>> &lit_areas,
                  const std::string &tile_index) {
    std::ofstream svg_file {
        BuildFileNameWithTileIndex("osm2gear_lit_areas", tile_index, "svg", true), std::ios::out};
    svg_file << svg_start << std::endl;
    for (auto & area : lit_areas) {
        assert(area->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON);
        std::string wkt = wkt_writer.write(area.get());
        bool more_than_one = false;
        auto svg_points = SVGPointsFromWKTPolygon(wkt);
        for (auto & points_string : svg_points) {
            svg_file << "<polygon points=\"" << points_string << "\" style=\"fill:";
            svg_file << (more_than_one ? "white" : "cyan") << ";stroke-width:0\" />" << std::endl;
            more_than_one = true;
        }
    }
    svg_file << "</svg>" << std::endl;
}

std::string ColourForBuildingZone(const BuildingZone * zone) {
    std::string fill_colour;
    switch(zone->GetBuildingZoneType()) {
        case BuildingZoneType::commercial:
            fill_colour = zone->IsGenerated() ? "lightblue" : "blue"; break;
        case BuildingZoneType::industrial:
            fill_colour = zone->IsGenerated() ? "lightgreen" : "green"; break;
        case BuildingZoneType::retail:
            fill_colour = zone->IsGenerated() ? "orange" : "darkorange"; break;
        case BuildingZoneType::residential:
            fill_colour = zone->IsGenerated() ? "pink" : "magenta"; break;
        case BuildingZoneType::farmyard:
            fill_colour = zone->IsGenerated() ? "sandybrown" : "chocolate"; break;
        case BuildingZoneType::aerodrome:
            fill_colour = "purple"; break;
        default:
            fill_colour = "red";
    }
    return fill_colour;
}

void DrawOSMZones(geos::io::WKTWriter &wkt_writer, const std::string &svg_start,
                  const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                  const std::string &tile_index) {
    std::ofstream svg_file {BuildFileNameWithTileIndex("osm_zones", tile_index, "svg", true), std::ios::out};
    svg_file << svg_start << std::endl;
    std::string wkt;
    for (auto & zone : built_up_zones) {
        if (zone->IsGenerated()) {
            continue;
        }
        assert(zone->GetGeometry()->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON);
        wkt = wkt_writer.write(zone->GetGeometry());
        auto svg_points = SVGPointsFromWKTPolygon(wkt);
        svg_file << "<polygon points=\"" << svg_points[0] << "\" style=\"fill:";
        svg_file << ColourForBuildingZone(zone.get()) << ";stroke-width:1;stroke:#000000\" />" << std::endl;
    }
    svg_file << "</svg>" << std::endl;
}

void DrawGeneratedZones(geos::io::WKTWriter &wkt_writer, const std::string &svg_start,
                        const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                        const std::string &tile_index) {
    std::ofstream svg_file {
        BuildFileNameWithTileIndex("generated_zones", tile_index, "svg", true), std::ios::out};
    svg_file << svg_start << std::endl;
    std::string wkt;
    for (auto & zone : built_up_zones) {
        if (!zone->IsGenerated()) {
            continue;
        }
        assert(zone->GetGeometry()->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON);
        wkt = wkt_writer.write(zone->GetGeometry());
        auto svg_points = SVGPointsFromWKTPolygon(wkt);
        svg_file << "<polygon points=\"" << svg_points[0] << "\" style=\"fill:";
        svg_file << ColourForBuildingZone(zone.get()) << ";stroke-width:1;stroke:#000000\" />" << std::endl;
    }
    svg_file << "</svg>" << std::endl;
}

void DrawAllZones(geos::io::WKTWriter &wkt_writer, const std::string &svg_start,
                  const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                  const std::vector<std::shared_ptr<Building>> &buildings,
                  const std::string &tile_index) {
    std::ofstream svg_file {BuildFileNameWithTileIndex("all_zones", tile_index, "svg", true), std::ios::out};
    svg_file << svg_start << std::endl;
    std::string wkt;
    for (auto & zone : built_up_zones) {
        assert(zone->GetGeometry()->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON);
        wkt = wkt_writer.write(zone->GetGeometry());
        auto svg_points = SVGPointsFromWKTPolygon(wkt);
        svg_file << "<polygon points=\"" << svg_points[0] << "\" style=\"fill:";
        svg_file << ColourForBuildingZone(zone.get()) << ";stroke-width:0\" />" << std::endl;
    }
    DrawBuildings(wkt_writer, svg_file, "black", buildings);
    svg_file << "</svg>" << std::endl;
}

void DrawSettlementTypes(geos::io::WKTWriter &wkt_writer, const std::string &svg_start,
                         const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                         const std::string &tile_index) {
    std::ofstream svg_file {
        BuildFileNameWithTileIndex("settlement_zones", tile_index, "svg", true), std::ios::out};
    svg_file << svg_start << std::endl;
    std::string wkt;
    std::string fill_colour;
    for (auto & zone : built_up_zones) {
        if (zone->GetBuildingZoneType() == BuildingZoneType::farmyard) {
            assert(zone->GetGeometry()->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON);
            wkt = wkt_writer.write(zone->GetGeometry());
            auto svg_points = SVGPointsFromWKTPolygon(wkt);
            svg_file << "<polygon points=\"" << svg_points[0] << "\" style=\"fill:";
            svg_file << ColourForBuildingZone(zone.get()) << ";stroke-width:0\" />" << std::endl;
        } else {
            for (auto & block : *zone->GetCityBlocks()) {
                switch(block->GetSettlementType()) {
                    case SettlementType::centre:
                        fill_colour = "blue"; break;
                    case SettlementType::block:
                        fill_colour = "green"; break;
                    case SettlementType::dense:
                        fill_colour = "magenta"; break;
                    case SettlementType::periphery:
                        fill_colour = "yellow"; break;
                    default:
                        fill_colour = "grey";
                }
                assert(block->GetGeometry()->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON);
                wkt = wkt_writer.write(block->GetGeometry());
                auto svg_points = SVGPointsFromWKTPolygon(wkt);
                svg_file << "<polygon points=\"" << svg_points[0] << "\" style=\"fill:";
                svg_file << fill_colour << ";stroke-width:0\" />" << std::endl;
            }
        }
    }
    svg_file << "</svg>" << std::endl;
}

void DrawCityBlocks(geos::io::WKTWriter &wkt_writer, const std::string &svg_start,
                    const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                    const std::string &tile_index) {
    std::ofstream svg_file{
        BuildFileNameWithTileIndex("city_blocks", tile_index, "svg", true), std::ios::out};
    svg_file << svg_start << std::endl;
    std::string wkt;
    std::string fill_colour;
    for (auto &zone: built_up_zones) {
        for (auto & block : *zone->GetCityBlocks()) {
            assert(block->GetGeometry()->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON);
            wkt = wkt_writer.write(block->GetGeometry());
            auto svg_points = SVGPointsFromWKTPolygon(wkt);
            svg_file << "<polygon points=\"" << svg_points[0] << "\" style=\"fill:";
            svg_file << "rgb(" << std::to_string(rand() % 256) << "," <<std::to_string(rand() % 256) << "," <<std::to_string(rand() % 256);
            svg_file << ");stroke-width:0\" />" << std::endl;
        }
    }
    svg_file << "</svg>" << std::endl;
}

void DrawSettlementClusters(geos::io::WKTWriter &wkt_writer, const std::string &svg_start,
                            const std::vector<std::unique_ptr<GridIndexedGeometry>> &settlement_clusters,
                            const std::vector<std::shared_ptr<Building>> &buildings,
                            const std::string &tile_index) {
    std::ofstream svg_file{
        BuildFileNameWithTileIndex("settlement_clusters", tile_index, "svg", true), std::ios::out};
    svg_file << svg_start << std::endl;
    std::string wkt;
    for (auto & cluster : settlement_clusters) {
        assert(cluster->GetGeometry()->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON);
        wkt = wkt_writer.write(cluster->GetGeometry());
        auto svg_points = SVGPointsFromWKTPolygon(wkt);
        svg_file << "<polygon points=\"" << svg_points[0] << "\" style=\"fill:";
        svg_file << "magenta" << ";stroke-width:1\" />" << std::endl;
    }
    DrawBuildings(wkt_writer, svg_file, "black", buildings);
    svg_file << "</svg>" << std::endl;
}

std::string CreateSVGStart(geos::geom::Coordinate min_coord, geos::geom::Coordinate max_coord) {
    return "<svg viewBox=\"" + std::to_string(min_coord.x) + " " + std::to_string(min_coord.y) + " " + std::to_string(max_coord.x) + " " + std::to_string(max_coord.y) + "\">";
}

void DrawLandUse(const geos::geom::Coordinate &min_coord, const geos::geom::Coordinate &max_coord,
                 const std::vector<std::shared_ptr<Building>> &buildings,
                 const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                 const std::vector<std::unique_ptr<geos::geom::Geometry>> &lit_areas,
                 const std::vector<std::unique_ptr<GridIndexedGeometry>> &settlement_clusters,
                 const std::string &tile_index) {
    geos::io::WKTWriter wkt_writer;
    wkt_writer.setTrim(true);
    auto svg_start = CreateSVGStart(min_coord, max_coord);
    DrawOSMZones(wkt_writer, svg_start, built_up_zones, tile_index);
    DrawGeneratedZones(wkt_writer, svg_start, built_up_zones, tile_index);
    DrawAllZones(wkt_writer, svg_start, built_up_zones, buildings, tile_index);
    DrawLitAreas(wkt_writer, svg_start, lit_areas, tile_index);
    DrawSettlementTypes(wkt_writer, svg_start, built_up_zones, tile_index);
    DrawCityBlocks(wkt_writer, svg_start, built_up_zones, tile_index);
    DrawSettlementClusters(wkt_writer, svg_start, settlement_clusters, buildings, tile_index);
}

void DrawTrees(const geos::geom::Coordinate &min_coord, const geos::geom::Coordinate &max_coord,
               const std::vector<std::shared_ptr<Building>> &buildings,
               const std::vector<std::unique_ptr<Tree>> &trees,
               const std::string &tile_index) {
    geos::io::WKTWriter wkt_writer;
    wkt_writer.setTrim(true);
    auto svg_start = CreateSVGStart(min_coord, max_coord);
    std::ofstream svg_file{BuildFileNameWithTileIndex("trees", tile_index, "svg", true), std::ios::out};
    svg_file << svg_start << std::endl;
    std::string wkt;
    DrawBuildings(wkt_writer, svg_file, "black", buildings); //must be first so we would see overlap
    for (auto & tree : trees) {
        svg_file << "<circle cx=\"" << tree->GetX() << "\" cy=\"" << -tree->GetY() << R"(" r="4" stroke-width="0" fill=")";
        switch(tree->GetTreeOrigin()) {
            case TreeOrigin::mapped:
                svg_file << "green";
                break;
            case TreeOrigin::park:
                svg_file << "red";
                break;
            case TreeOrigin::garden:
                svg_file << "blue";
                break;
        }
        svg_file << "\" />";
    }
    svg_file << "</svg>" << std::endl;
}
