#ifndef OSM2GEAR_JSONIO_H
#define OSM2GEAR_JSONIO_H

#include <optional>
#include <string>

/*! \brief Queries WikiData for population data of a given entity.
 *
 * @param entity_id from OSM tag "wikidata"
 * @return the population if everything went fine and the found value is > 0, otherwise std::nullopt
 */
std::optional<int> QueryPopulationWikidata(const std::string &entity_id);

#endif //OSM2GEAR_JSONIO_H
