#ifndef OSM2GEAR_ROOFS_H
#define OSM2GEAR_ROOFS_H

#include <tuple>

#include "geos/geom/Coordinate.h"

const float RIDGE_OFFSET = 90.0;
const float ORIENTATION_MAX_DEGREE = 180.0;


/*! \brief As set of hints for placing or constructing the roof on a building.
Not all buildings have a RoofHint - and most often only one of the fields will be available.

    See description in the 'how it works' section of the manual.

    If a building has an inner node, then no more simplifications should be done.

    Fields:
    * ridge_orientation_: the orientation in degrees of the ridge - for gabled roofs with 4 edges. Only set
                         if there are neighbours and therefore the ridge should be aligned.
    * inner_node:        for L-shaped roofs due to neighbours or L-shaped building:
                         the node which is at the inner side in the ca. 90 deg corner as follows:
                         Tuple of tuple(lon, lat) in local coordinates.
                         The lon/lat instead of a node position is kept because due to geometry changes
                         the sequence of the outer ring could change. This way we can test.
    * node_before_inner_is_shared_: only used for roof with 5 nodes to signal whether the node before inner is
                         a shared node with same neighbour as inner node or after.
                         Otherwise, there is not enough info to find the L.
    */
class RoofHint {
   private:
    double ridge_orientation_;
    geos::geom::Coordinate inner_node_;
    bool node_before_inner_is_shared_;

   public:
    RoofHint() {
        this->ridge_orientation_ = -1.0;
        this->node_before_inner_is_shared_ = false;
        this->inner_node_.setNull();  // Set inner_node_ to null using the setNull() method.
    }
    void SetRidgeOrientation(double angle) {
        ridge_orientation_ = angle + RIDGE_OFFSET;  // add 90 to the gable end orientation -> ridge
        if (ridge_orientation_ > ORIENTATION_MAX_DEGREE) {
            ridge_orientation_ -= ORIENTATION_MAX_DEGREE;
        }
    }
    [[nodiscard]] double GetRidgeOrientation() const {
        return ridge_orientation_;
    }
    [[nodiscard]] bool HasInnerNodeDefined() const {
        return not inner_node_.isNull();
    }
    void SetInnerNode(geos::geom::Coordinate local_node) {
        inner_node_ = local_node;
    }
    [[nodiscard]] std::pair<double, double> GetInnerNodeXY() const {
        return std::make_pair(inner_node_.x, inner_node_.y);
    }
    void SetNodeBeforeInnerIsShared(bool is_shared) {
        node_before_inner_is_shared_ = is_shared;
    }
    [[nodiscard]] bool GetNodeBeforeInnerIsShared() const {
        return node_before_inner_is_shared_;
    }
};

#endif  // OSM2GEAR_ROOFS_H
