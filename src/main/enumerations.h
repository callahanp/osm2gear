#ifndef OSM2GEAR_ENUMERATIONS_H
#define OSM2GEAR_ENUMERATIONS_H

enum class OSMFeatureType {
    building_relation = 1,
    building_generated = 2,
    building_owbb = 3,
    landuse = 4,
    road = 5,
    pylon_way = 6,
    generic_node =7,
    generic_way = 8,
    place = 9
};


enum class ZoneType {
    building_zone,
    city_block,
};


/*! \brief Used to classify the types of land-use in a zone - from OSM data directly or interpreted indirectly.
 *
 * Element names must match OSM values apart from non_osm and specialProcessing.
 *
 * The numbering is such that lower numbers have higher priority when overlapping land-uses need to be resolved
 * (e.g. it is more important to keep information about aerodrome right than for industrial - and it is more
 * important to keep boundaries (especially mapped in OSM) for e.g. retail than residential -- also because
 * residential is treated as the default).
 */
enum class BuildingZoneType {
    aerodrome = 1,
    port = 2,
    industrial = 3,
    retail = 4,
    farmyard = 5,
    commercial = 6,
    residential = 7,
    // for generated land-use zones this is not correctly applied, as several farmyards might be interpreted as one. See GeneratedBuildingZone.guess_building_zone_type
    nonOSM = 20,
    specialProcessing = 30, //can be used to mark special processing
};

enum class BuildingZoneOrigin {
    osm,  // directly from OSM
    from_buildings,  // generated from buildings outside of building zones from OSM
    apt_dat  // generated from airport boundaries in apt_dat
};

/*! \brief Used to classify buildings for processing on zone level and defining height per level in some cases
 */
enum class BuildingClass {
    residential = 100,
    residential_small = 110,
    terrace = 120,
    apartments = 130,
    commercial = 200,
    industrial = 300,
    warehouse = 301,
    retail = 400,
    parking_house = 1000,
    religion = 2000,
    publicx = 3000,  // "public" is a reserved word in C++
    farm = 4000,
    airport = 5000,
    undefined = 9999  // mostly because BuildingType can only be approximated to "yes"
};

enum class PlaceType {
    city,
    town,
    farm,
};

enum class SettlementType {
    centre = 9,  // elsewhere in the code the value is used for comparison, so centre should be highest
    block = 8,
    dense = 7,
    periphery = 6,  //default within lit area
    rural = 5  // only implicitly used for building zones without city blocks.
};

enum class HighwayType {
    roundabout = 16,
    motorway = 15,
    trunk = 14,
    primary = 13, // can also be a non-one-way motorway link, trunk link or primary link
    secondary = 12, // can also be a non-one-way secondary link
    tertiary = 11, // can also be a non-one-way tertiary link
    unclassified = 10,
    road = 9,
    one_way_multi_lane = 8, // for now assumed to be 2 lanes
    one_way_large = 7, // for one-way links of motorway and trunk
    one_way_normal = 6,
    residential = 5,
    living_street = 4,
    service = 3,
    pedestrian = 2,
    slow = 1, // cycle ways, tracks, footpaths etc
};

enum class WaterwayType {
    large, // river, canal
    narrow,
};

enum class RailwayLineType {
    rail,
    subway,
    monorail,
    light_rail,
    funicular,
    narrow_gauge,
    tram,
    other,
};

/*! \brief A reduced mapping of OSM building types.
 * Mostly match value of a tag with k=building.
 *
 * If changed, then also check use in osm_types.cpp::GetBuildingClass(), osm_types::ParseTagsForBuildingType
 * as well as is_...() methods in osmstrings.py."""
 */
enum class BuildingType {
    yes = 1, // default
    parking = 10, // k="parking" v="multi-storey"
    apartments = 21,
    attached = 210, // an apartment in a city block without space between buildings. Does not exist in OSM
    house = 22,
    detached = 23,
    residential = 24,
    dormitory = 25,
    terrace = 26,
    bungalow = 31,
    static_caravan = 32,
    cabin = 33,
    hut = 34,
    commercial = 41,
    office = 42,
    retail = 51,
    industrial = 61,
    warehouse = 62,
    storage_tank = 63,
    cathedral = 71,
    chapel = 72,
    church = 73,
    mosque = 74,
    temple = 75,
    synagogue = 76,
    publicx = 81, //public is a reserved word
    civic = 82,
    school = 83,
    hospital = 84,
    hotel = 85,
    farm = 101,
    barn = 102,
    cowshed = 103,
    farm_auxiliary = 104,
    greenhouse = 105,
    glasshouse = 106,
    stable = 107,
    sty = 108,
    riding_hall = 109,
    slurry_tank = 110,
    hangar = 201,
    stadium = 301,
    sports_hall = 302,
};

/*! \brief The tree type needs to correspond to the available types in the FG material for (OSM) trees.
 *
 * The materials are defined in fgdata/Materials/regions/global-summer.xml, where there also is an
 * explanation about the types as well how to regionalize them.
 *
 * In botany and horticulture, *deciduous* plants, including trees, shrubs and herbaceous perennials,
 * are those that lose all of their leaves for part of the year.
 *
 * A *shrub* is defined as a woody plant that is smaller than a tree and generally has a rounded shape.
 *
 *  Evergreen cone-shaped trees, growing needle- or scale-like leaves are called *conifers*.
 *
 *  The *Caatinga* is a xeric shrubland and thorn forest, which consists primarily of small,
 *  thorny trees that shed their leaves seasonally.
 */
enum class TreeType {
    significant_trees,
    garden_vegetation,
};

#endif //OSM2GEAR_ENUMERATIONS_H
