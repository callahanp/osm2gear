#ifndef OSM2GEAR_UTILS_H
#define OSM2GEAR_UTILS_H

#include <chrono>
#include <filesystem>
#include <sstream>

#include "boost/log/trivial.hpp"

#include <geos/geom/Coordinate.h>
#include <geos/geom/GeometryFactory.h>
#include <geos/geom/Polygon.h>

std::stringstream LocalTimeString();

/** \brief Gets the requested environment variable from the OS and throws an exception if not found
 * @param env_variable
 * @return
 */
char* GetEnv(const char * env_variable);

/** \brief Creates a DB connection string based on OS environment variables
 *
 * @return
 */
std::string CreateDBConnectionString(const std::string &db_name);

struct LonLat {
    double lon;
    double lat;

    LonLat() = default;
    LonLat(double c_lon, double c_lat);
};

struct Boundary {
    double west {};
    double south {};
    double east {};
    double north {};

    Boundary() = default;
    Boundary(double west, double south, double east, double north);

    [[nodiscard]] bool IsInsideBoundary(const LonLat lon_lat) const {
        if (lon_lat.lon < west or lon_lat.lon > east or lon_lat.lat < south or lon_lat.lat > north) {
            return false;
        }
        return true;
    }
};

constexpr static double EQURAD {6378137.0};
constexpr static double FLATTENING {298.257223563};
constexpr static double SQUASH {0.9966471893352525192801545};
constexpr static double RA2 = 1 / (EQURAD * EQURAD);

class TileHandler {
private:

    const static double E2;  //cannot be constexpr due to use of fabs()
    const static double E4;

    Boundary boundary_;
    LonLat tile_centre_;
    long tile_index_;

    double cos_lat_;
    double r1_;
    double r2_;

public:
    explicit TileHandler(long tile_index);

    static geos::geom::GeometryFactory::Ptr MakeGeometryFactory();

    [[nodiscard]] geos::geom::Coordinate ToLocal(const LonLat &) const;
    [[nodiscard]] LonLat ToGlobal(const geos::geom::Coordinate &) const;
    [[nodiscard]] LonLat ToGlobal(const LonLat &) const;
    [[nodiscard]] std::pair<geos::geom::Coordinate, geos::geom::Coordinate> GetTileExtentLocal() const;
    [[nodiscard]] Boundary CreateExtendedBoundary(double extension) const;
    [[nodiscard]] Boundary GetTileBoundary() const {
        return boundary_;
    }
    [[nodiscard]] LonLat GetTileCentre() const {
        return tile_centre_;
    }
    [[nodiscard]] std::string GetTileIndex() const {
        return std::to_string(tile_index_);
    }
};

/*! \brief Calculates how much a given point a distance away is elevated over the round world.
 * The distance_1 and distance_2 are right-angled in a cartesian coordinate system in metres.
 * (e.g. x-direction and y_direction).
 */
double CalcHorizonElev(double distance_1, double distance_2);

/*! \brief Calculates the difference between two bearings. If positive with clock, if negative against the clock sense.
 * I.e. from bearing1 to bearing2 you need to turn delta with or against the clock.
 * The bearings are in degrees - North is 0, East is 90.
 */
double CalcDeltaBearing(double bearing_1, double bearing_2);

std::string LocationDirectoryName(const LonLat &lon_lat);

/*! \brief """Returns the path to e.g. stg-files in an FG scenery directory hierarchy at a given global lat/lon location.
 *The scenery type is e.g. 'Terrain', 'Object', 'Buildings'.
 */
std::filesystem::path ConstructPathToFiles(const std::string &base_directory,
                                           const std::string &scenery_type,
                                           const LonLat &lon_lat);

std::unique_ptr<geos::geom::Polygon> MakeLocalBox(double x_min, double y_min, double x_max, double y_max);

class TimeLogger {
private:
    std::chrono::time_point<std::chrono::system_clock> reference_;
public:
    TimeLogger();
    void Log(std::string message);
};

/*! \brief Write a log message to debug level and mark it as a warning
 * If this warning is really for debugging/programming and not for the overall process.
 * Basically just makes sure that there is a standardised warning mark, so the log
 * can be searched systematically
 * @param message
 */
void LogDebugWarning(std::string message);


/*! \brief Returns the distance between two points based on local coordinates (x,y).
 */
double CalcDistanceLocal(double x_1, double y_1, double x_2, double y_2);

/*! \brief Validate that the smallest value is within a tolerance smaller than the largest value.
 * The tolerance is relative: e.g. for values 0.8 and 1.0 a tolerance of 0.2 would just make it.
 *
 * @param value_1
 * @param value_2
 * @param relative_tolerance
 * @return
 */
bool CheckWithinRelativeToleranceOfMax(double value_1, double value_2, double relative_tolerance);

std::string BuildFileNameWithTileIndex(const std::string &, const std::string &, const std::string &, bool include_time);

/*! \brief Tokenizes a string based on a delimiter and returns the result into an existing vector.
 *
 * @param s
 * @param delim
 * @param out
 */
void TokenizeString(const std::string &s, char delim, std::vector<std::string> &out);

/*! \brief Merge polygons into as few polygons as possible.
 * It makes sure that the type is Polygon and not e.g. MultiPolygon.
 */
std::vector<std::unique_ptr<geos::geom::Polygon>> MergePolygons(std::vector<std::unique_ptr<geos::geom::Polygon>> &original_list);

/*! \brief Merge geometries into as few polygons as possible.
 * It makes sure that the original type is Polygon or Multipolygon and only returns type Polygon.
 */
std::vector<std::unique_ptr<geos::geom::Polygon>> MergeGeomsToPolys(std::vector<std::unique_ptr<geos::geom::Geometry>> &original_list);
#endif //OSM2GEAR_UTILS_H
