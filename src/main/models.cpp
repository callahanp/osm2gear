#include "models.h"

#include <cassert>
#include <memory>
#include <cmath>

#include "boost/log/trivial.hpp"
#include <geos/geom/Geometry.h>
#include <geos/geom/prep/PreparedPolygon.h>

#include "buildings.h"
#include "json_io.h"
#include "parameters.h"


void GridIndexed::AddGridIndex(int index) {
    grid_indices_.emplace(index);
}

GridIndexedGeometry::GridIndexedGeometry(std::unique_ptr<geos::geom::Geometry> && geometry): geometry_ {std::move(geometry)} {}

geos::geom::Geometry* GridIndexedGeometry::GetGeometry() {
    geos::geom::Geometry* ptr = geometry_.get();
    return ptr;
}

bool GridIndexedGeometry::ContainsOther(const geos::geom::Geometry* other) const {
    return geometry_->contains(other);
}

bool GridIndexedGeometry::Intersects(const geos::geom::Geometry* other) const {
    return geometry_->intersects(other);
}

GridIndexedPreparedPolygon::GridIndexedPreparedPolygon(std::unique_ptr<geos::geom::Geometry> && geometry): GridIndexedGeometry(std::move(geometry)) {
    prepared_polygon_ = std::make_unique<geos::geom::prep::PreparedPolygon>(GetGeometry());
}

bool GridIndexedPreparedPolygon::IsDisjoint(const geos::geom::Geometry* other) const {
    return prepared_polygon_->disjoint(other);
}

OSMFeature::OSMFeature(osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry): osm_id_ {osm_id}, geometry_ {std::move(geometry)} {}

geos::geom::Geometry* OSMFeature::GetGeometry() const {
    return geometry_.get();
}

void OSMFeature::SetGeometry(std::unique_ptr<geos::geom::Geometry> new_geometry) {
    geometry_ = std::move(new_geometry);
}

OSMFeatureLinear::OSMFeatureLinear(osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, const tags_t* tags) : OSMFeature(osm_id, std::move(geometry)){
    tunnel_ = ParseIsTunnel(tags);
    has_embankment_or_cutting_ = ParseHasEmbankmentOrCutting(tags);
}

Highway::Highway(osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, const tags_t* tags): OSMFeatureLinear(osm_id, std::move(geometry), tags) {
    highway_type_ = ParseTagsForHighwayType(tags);
    roundabout_ = ParseIsRoundabout(tags);
    oneway_ = ParseIsOneWay(tags, highway_type_ == HighwayType::motorway);
    lanes_ = ParseLanes(tags, 1, highway_type_ == HighwayType::motorway);
}

double Highway::GetWidth() const {
    double my_width;
    if (highway_type_ == HighwayType::service or highway_type_ == HighwayType::residential or highway_type_ == HighwayType::living_street or highway_type_ == HighwayType::pedestrian) {
        my_width = 5.;
    } else if (highway_type_ == HighwayType::road or highway_type_ == HighwayType::unclassified or highway_type_ == HighwayType::tertiary) {
        my_width = 6.;
    } else if (highway_type_ == HighwayType::secondary or highway_type_ == HighwayType::primary or highway_type_ == HighwayType::trunk) {
        my_width = 7.;
    } else if (highway_type_ == HighwayType::one_way_multi_lane) {
        my_width = 3.5; // will be enhanced with more lanes below
    } else if (highway_type_ == HighwayType::one_way_large) {
        my_width = 4.;
    } else if (highway_type_ == HighwayType::one_way_normal) {
        my_width = 3.;
    } else { //HighwayType::slow
        my_width = 2.5;
    }
    if (highway_type_ == HighwayType::motorway) {
        my_width = 3.5 * lanes_;
        if (!oneway_) {
            my_width += 2 * 2. + 1.; //assuming limited hard shoulders, not much middle stuff
        } else {
            my_width += 2 * 3. + 2.; //hard shoulders, middle stuff
        }
    } else if (lanes_ > 1) {
        my_width += 0.8 * (lanes_ - 1) * my_width;
    }
    if (has_embankment_or_cutting_) {
        my_width += 4.;
    }
    return my_width;
}

bool Highway::UseForBuildingZoneSplit() const {
    if (tunnel_) {
        return false;
    }
    if (highway_type_ == HighwayType::motorway or highway_type_ == HighwayType::trunk or highway_type_ == HighwayType::primary) {
        return true;
    }
    return false;
}

bool Highway::PopulateBuildingsAlong() const {
    if (tunnel_) {
        return false;
    }
    if (highway_type_ == HighwayType::motorway or highway_type_ == HighwayType::trunk) {
        return false;
    }
    return true;
}

Waterway::Waterway(osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, const tags_t* tags) : OSMFeatureLinear(osm_id, std::move(geometry), tags){
    waterway_type_ = ParseTagsForWaterwayType(tags);
}

RailwayLine::RailwayLine(osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, const tags_t* tags) : OSMFeatureLinear(osm_id, std::move(geometry), tags){
    rail_way_line_type_ = ParseTagsForRailwayLineType(tags);
    tracks_ = ParseTracks(tags);
    service_spur_ = ParseIsServiceSpur(tags);
}

double RailwayLine::GetWidth() const {
    double my_width;
    if (rail_way_line_type_ == RailwayLineType::narrow_gauge or rail_way_line_type_ == RailwayLineType::tram or rail_way_line_type_ == RailwayLineType::funicular) {
        my_width = 5.0;
    } else if (rail_way_line_type_ == RailwayLineType::rail or rail_way_line_type_ == RailwayLineType::light_rail or rail_way_line_type_ == RailwayLineType::monorail or rail_way_line_type_ == RailwayLineType::subway) {
        my_width = 7.0;
    } else {
        my_width = 6.0;
    }
    if (tracks_ > 1) {
        my_width += 0.8 * (tracks_ - 1) * my_width;
    }
    if (has_embankment_or_cutting_) {
        my_width += 6.0;
    }
    return my_width;
}

bool RailwayLine::UseForBuildingZoneSplit() const {
    if (tunnel_ or service_spur_) {
        return false;
    }
    if (rail_way_line_type_ == RailwayLineType::rail or rail_way_line_type_ == RailwayLineType::light_rail or rail_way_line_type_ == RailwayLineType::subway) {
        return false;
    }
    return true;
}

Place::Place(osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, const tags_t* tags, bool is_point) : OSMFeature(osm_id, std::move(geometry)){
    place_type_ = ParseTagsForPlaceType(tags);
    is_point_ = is_point;
    ParseTagsForPopulation(tags);
}

void Place::ParseTagsForPopulation(const tags_t* tags) {
    if (tags->contains(k_population)) {
        this->population_ = ParseInt(tags->at(k_population));
    }
    if (!population_.has_value() && tags->contains(k_wikidata)) {
        auto population_result = QueryPopulationWikidata(tags->at(k_wikidata));
        if (population_result.has_value()) {
            population_ = population_result.value();
        }
    }
}

void Place::TransformToPoint() {
    if (is_point_) {
        return;
    }
    if (place_type_ == PlaceType::farm) {
        BOOST_LOG_TRIVIAL(warning) << "Attempted to transform Place of type farm from area to point";
        return;
    }
    is_point_ = true;
    geometry_ = geometry_->getCentroid();
}

double Place::CalculateCircleRadius(const long population, const double power) const {
    double radius = pow((double)population, power);
    if (place_type_ == PlaceType::city) {
        radius *= Parameters::Get().OWBB_PLACE_RADIUS_FACTOR_CITY;
    } else {
        radius *= Parameters::Get().OWBB_PLACE_RADIUS_FACTOR_TOWN;
    }
    return radius;
}

std::tuple<std::unique_ptr<GridIndexedPreparedPolygon>, std::unique_ptr<GridIndexedPreparedPolygon>, std::unique_ptr<GridIndexedPreparedPolygon>> Place::CreatePreparedPlacePolygons(const geos::geom::GeometryFactory::Ptr &factory) const {
    long population = population_.value_or(0);
    if (population == 0) {
        population = place_type_ == PlaceType::city ? Parameters::Get().OWBB_PLACE_POPULATION_DEFAULT_CITY : Parameters::Get().OWBB_PLACE_POPULATION_DEFAULT_TOWN;
    }
    std::unique_ptr<geos::geom::Geometry> centre_circle = factory->createPolygon();
    std::unique_ptr<geos::geom::Geometry> block_circle = factory->createPolygon();
    std::unique_ptr<geos::geom::Geometry> dense_circle;
    double radius;
    if (place_type_ == PlaceType::city) {
        radius = CalculateCircleRadius(population, Parameters::Get().OWBB_PLACE_RADIUS_EXPONENT_CENTRE);
        centre_circle = GetGeometry()->buffer(radius);
        radius = CalculateCircleRadius(population, Parameters::Get().OWBB_PLACE_RADIUS_EXPONENT_BLOCK);
        block_circle = GetGeometry()->buffer(radius);
        radius = CalculateCircleRadius(population, Parameters::Get().OWBB_PLACE_RADIUS_EXPONENT_DENSE);
        dense_circle = GetGeometry()->buffer(radius);
    } else {
        if (population_ > 0 and population > Parameters::Get().OWBB_PLACE_POPULATION_MIN_BLOCK) {
            radius = CalculateCircleRadius(population, Parameters::Get().OWBB_PLACE_RADIUS_EXPONENT_BLOCK);
            block_circle = GetGeometry()->buffer(radius);
        }
        radius = CalculateCircleRadius(population, Parameters::Get().OWBB_PLACE_RADIUS_EXPONENT_DENSE);
        dense_circle = GetGeometry()->buffer(radius);
    }
    return std::make_tuple(
            std::make_unique<GridIndexedPreparedPolygon>(std::move(centre_circle)),
            std::make_unique<GridIndexedPreparedPolygon>(std::move(block_circle)),
            std::make_unique<GridIndexedPreparedPolygon>(std::move(dense_circle)));
}

Zone::Zone(osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, ZoneType zone_type, BuildingZoneType building_zone_type) : zone_type_ {zone_type}, building_zone_type_ {building_zone_type}, OSMFeature(osm_id, std::move(geometry)) {}

void Zone::RelateBuilding(std::shared_ptr<Building> &building) {
    this->osm_buildings_.push_back(building);
    building->SetZone(this);
}

void Zone::UnRelateBuilding(const std::shared_ptr<Building> &building) {
    auto res = find(osm_buildings_.begin(), osm_buildings_.end(), building);
    if (res != osm_buildings_.end()) {
        osm_buildings_.erase(res);
    }
}

CityBlock::CityBlock(osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, BuildingZoneType building_zone_type) : Zone(osm_id, std::move(geometry), ZoneType::city_block, building_zone_type) {}

void CityBlock::MakePreparedGeometry() {
    prep_geom_ = std::make_unique<geos::geom::prep::PreparedPolygon>(GetGeometry());
}

BuildingZone::BuildingZone(osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry,
                           BuildingZoneType building_zone_type, std::optional<BuildingZoneOrigin> building_zone_origin) : Zone(osm_id, std::move(geometry), ZoneType::building_zone, building_zone_type) {
    building_zone_origin_ = building_zone_origin;
}

bool BuildingZone::ContainsOrIntersects(const geos::geom::Geometry* other_geometry) {
    if (this->geometry_->contains(other_geometry) or this->geometry_->intersects(other_geometry)) {
        return true;
    }
    return false;
}

std::unique_ptr<geos::geom::Geometry> BuildingZone::NonIntersectingDifference(const geos::geom::Geometry * other_geometry) {
    return other_geometry->difference(GetGeometry());
}

void BuildingZone::ReassignOSMBuildingsToCityBlocks() {
    for (auto & osm_building : osm_buildings_) {
        for (auto & city_block : city_blocks_) {
            if (city_block->ContainsProperly(osm_building->GetOuterPolygon()) or city_block->Intersects(osm_building->GetOuterPolygon())) {
                city_block->RelateBuilding(osm_building);
                break;
            }
        }
    }
}

void BuildingZone::SetSettlementTypeCityBlocks(SettlementType current_settlement_type,
                                               std::unique_ptr<GridIndexedPreparedPolygon>& urban_settlement) {
    for (auto & city_block : city_blocks_) {
        if (city_block->GetSettlementType() < current_settlement_type) {
            if (urban_settlement->ContainsOther(city_block->GetGeometry()) or urban_settlement->Intersects(city_block->GetGeometry())) {
                city_block->SetSettlementType(current_settlement_type);
            }
        }
    }
}

void BuildingZone::GuessBuildingZoneType(const std::vector<std::unique_ptr<Place>> &farm_places) {
    // TODO: https://gitlab.com/osm2city/osm2gear/-/issues/18 map alternative land-uses to BuildingZones

    // now we should only have non_osm based on lonely buildings
    assert(this->building_zone_type_ == BuildingZoneType::nonOSM);
    long num_residential_buildings {0};
    long num_commercial_buildings {0};
    long num_industrial_buildings {0};
    long num_retail_buildings {0};
    long num_farm_buildings {0};
    for (auto & osm_building : osm_buildings_) {
        if (ParseIsYesBuilding(osm_building->GetTags())) {
            continue; //we do not really know what they are and by default they are mapped to residential
        }
        auto bc = ParseTagsForBuildingClass(osm_building->GetTags());
        if (bc == BuildingClass::residential_small or bc == BuildingClass::residential
                or bc == BuildingClass::terrace) {
            ++num_residential_buildings;
        } else if (bc == BuildingClass::publicx or bc == BuildingClass::commercial) {
            ++num_commercial_buildings;
        } else if (bc == BuildingClass::industrial or bc == BuildingClass::warehouse) {
            ++num_industrial_buildings;
        } else if (bc == BuildingClass::retail) {
            ++num_retail_buildings;
        } else if (bc == BuildingClass::farm) {
            ++num_farm_buildings;
        }
    }
    building_zone_type_ = BuildingZoneType::residential; //default
    auto max_value = std::max({num_residential_buildings, num_commercial_buildings, num_industrial_buildings, num_retail_buildings, num_farm_buildings});
    if (0 < max_value) {
        if (osm_buildings_.size() < 10) {
            BOOST_LOG_TRIVIAL(debug) << "Checking generated land-use for place=farm based on " << farm_places.size() << " place tags.";
            for (auto & place : farm_places) {
                if (place->IsPoint()) {
                    if (place->GetGeometry()->within(GetGeometry())) {
                        building_zone_type_ = BuildingZoneType::farmyard;
                        BOOST_LOG_TRIVIAL(debug) << "Found farmyard generated building zone based on node place";
                        return;
                    }
                } else {
                    if (place->GetGeometry()->intersects(GetGeometry())) {
                        building_zone_type_ = BuildingZoneType::farmyard;
                        BOOST_LOG_TRIVIAL(debug) << "Found farmyard generated building zone based on area place";
                        return;
                    }
                }
            }
        }
        if (num_farm_buildings == max_value) { //in small villages farm houses might be tagged, but rest just ="yes"
            if (osm_buildings_.size() >= 10 and num_farm_buildings < (long)0.5 * osm_buildings_.size()) {
                building_zone_type_ = BuildingZoneType::residential;
            } else {
                building_zone_type_ = BuildingZoneType::farmyard;
            }
        } else if (num_commercial_buildings == max_value) {
            building_zone_type_ = BuildingZoneType::commercial;
        } else if (num_industrial_buildings == max_value) {
            building_zone_type_ = BuildingZoneType::industrial;
        } else if (num_retail_buildings == max_value) {
            building_zone_type_ = BuildingZoneType::retail;
        }
    }
}
