#ifndef OSM2GEAR_TREES_H
#define OSM2GEAR_TREES_H

#include "geos/geom/Coordinate.h"

#include "models.h"
#include "../osm/data_reader.h"

enum class TreeOrigin {
    mapped,
    park,
    garden,
};

/*! \brief A single tree from OSM or interpreted OSM data.

    Cf. https://wiki.openstreetmap.org/wiki/Tag:natural=tree?uselang=en
    Cf. https://wiki.openstreetmap.org/wiki/Key%3Adenotation
 */
class Tree {
private:
    geos::geom::Coordinate coord_ = geos::geom::Coordinate::getNull();
    TreeType type_;
    TreeOrigin origin_;
    void ParseTags(const tags_t* tags);
public:
    Tree(LonLat lon_lat, const tags_t* tags, TreeOrigin origin, const TileHandler &tile_handler, TreeType tree_type);
    Tree(const geos::geom::Coordinate &coord, TreeOrigin origin, TreeType tree_type);
    [[nodiscard]] double GetX() const {
        return coord_.x;
    }
    [[nodiscard]] double GetY() const {
        return coord_.y;
    }
    [[nodiscard]] TreeOrigin GetTreeOrigin() const {
        return origin_;
    }
    [[nodiscard]] TreeType GetTreeType() const {
        return type_;
    }
};

void ProcessTrees(std::vector<std::shared_ptr<Building>> &buildings,
                  const TileHandler &tile_handler, OSMDBDataReader &osm_reader);

#endif //OSM2GEAR_TREES_H
