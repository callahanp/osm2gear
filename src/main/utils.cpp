#include <cmath>
#include <iomanip> // put_time

#include <geos/geom/CoordinateSequence.h>
#include <geos/geom/CoordinateSequenceFactory.h>
#include <geos/geom/LinearRing.h>

#include "simgear/bucket/newbucket.hxx"

#include "utils.h"


std::stringstream LocalTimeString() {
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);
    std::stringstream datetime;
    datetime << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d_%H%M%S");
    return datetime;
}

char* GetEnv(const char * env_variable) {
    auto result = std::getenv(env_variable);
    if (result == nullptr) {
        throw std::invalid_argument(std::string(env_variable) + " must be set as an environment variable on operating system level");
    }
    return result;
}

std::string CreateDBConnectionString(const std::string &db_name) {
    std::string connection_string {};
    connection_string.append("dbname = ").append(db_name);
    connection_string.append(" user = ").append(GetEnv("O2C_DB_USER"));
    connection_string.append(" password = ").append(GetEnv("O2C_DB_PASSWORD"));
    connection_string.append(" hostaddr = ").append(GetEnv("O2C_DB_HOST"));
    connection_string.append(" port = ").append(GetEnv("O2C_DB_PORT"));
    return connection_string;
}

Boundary::Boundary(double west, double south, double east, double north): west {west}, south {south}, east {east}, north {north} {}

LonLat::LonLat(double c_lon, double c_lat): lon {c_lon}, lat {c_lat} {}

double radians(double degrees) {
    return degrees * M_PI / 180.0;
}

double degrees(double radians) {
    return radians * 180.0 / M_PI;
}

const double TileHandler::E2 = fabs(1. - SQUASH * SQUASH);
const double TileHandler::E4 = E2 * E2;

TileHandler::TileHandler(const long tile_index): tile_index_ {tile_index} {
    std::unique_ptr<SGBucket> sg_bucket = std::make_unique<SGBucket>(tile_index);
    if (not sg_bucket->isValid()) {
        throw std::invalid_argument("Submitted tile index is not valid");
    }
    // we could keep using sg_bucket instead of transforming - for now we simulate the way it was in Python
    tile_centre_ = {};
    tile_centre_.lon = sg_bucket->get_center_lon();
    tile_centre_.lat = sg_bucket->get_center_lat();
    boundary_ = {sg_bucket->get_center_lon() - sg_bucket->get_width() / 2,
    sg_bucket->get_center_lat() - sg_bucket->get_height() / 2,
    sg_bucket->get_center_lon() + sg_bucket->get_width() / 2,
    sg_bucket->get_center_lat() + sg_bucket->get_height() / 2};

    //Compute radii for local origin
    double f = 1. / FLATTENING;
    double e2 = f * (2.-f);

    this->cos_lat_ = cos(radians(tile_centre_.lat));
    double sin_lat = sin(radians(tile_centre_.lat));
    this->r1_ = EQURAD * (1.-e2)/pow((1. - e2 * pow(sin_lat, 2)), (3./2.));
    this->r2_ = EQURAD / sqrt(1 - e2 * pow(sin_lat, 2));
}

geos::geom::GeometryFactory::Ptr
TileHandler::MakeGeometryFactory() {  //FIXME: would it be possible to have a member factory and return it?
    std::unique_ptr<geos::geom::PrecisionModel> pm(new geos::geom::PrecisionModel());
    geos::geom::GeometryFactory::Ptr factory = geos::geom::GeometryFactory::create(pm.get(), -1);
    return factory;
}

geos::geom::Coordinate TileHandler::ToLocal(const LonLat &global) const {
    double y = r1_ * radians(global.lat - tile_centre_.lat);
    double x = r2_ * radians(global.lon - tile_centre_.lon) * cos_lat_;
    return geos::geom::Coordinate {x, y};
}

LonLat TileHandler::ToGlobal(const geos::geom::Coordinate &local) const {
    LonLat converted(local.x, local.y);
    return ToGlobal(converted);
}

LonLat TileHandler::ToGlobal(const LonLat &local) const {
    LonLat global {};
    global.lat = degrees(local.lat / r1_) + tile_centre_.lat;
    global.lon = degrees(local.lon / (r2_ * cos_lat_)) + tile_centre_.lon;
    return global;
}

std::pair<geos::geom::Coordinate, geos::geom::Coordinate>
TileHandler::GetTileExtentLocal() const {
    geos::geom::Coordinate lower_left = ToLocal(LonLat{boundary_.west, boundary_.south});
    geos::geom::Coordinate upper_right = ToLocal(LonLat{boundary_.east, boundary_.north});
    return std::make_pair(lower_left, upper_right);
}

Boundary TileHandler::CreateExtendedBoundary(const double extension) const {
    auto lower_left_local = this->ToLocal(LonLat{boundary_.west, boundary_.south});
    auto lower_left_global = this->ToGlobal(geos::geom::Coordinate {lower_left_local.x - extension,
                                                                    lower_left_local.y - extension});
    auto upper_right_local = this->ToLocal(LonLat{boundary_.east, boundary_.north});
    auto upper_right_global = this->ToGlobal(geos::geom::Coordinate {upper_right_local.x + extension,
                                                                    upper_right_local.y + extension});
    return Boundary {lower_left_global.lon, lower_left_global.lat, upper_right_global.lon, upper_right_global.lat};
}

double CalcHorizonElev(const double distance_1, const double distance_2) {
    auto distance_square = pow(distance_1, 2) + pow(distance_2, 2);
    auto hypotenuse = sqrt(distance_square + pow(EQURAD, 2));
    return hypotenuse - EQURAD;
}

double CalcDeltaBearing(double bearing_1, double bearing_2) {
    if (bearing_1 == 360) {
        bearing_1 = 0.;
    }
    if (bearing_2 == 360) {
        bearing_2 = 0.;
    }
    double delta = bearing_2 - bearing_1;
    if (delta > 180) {
        delta -= 360;
    } else if (delta < -180) {
        delta += 360;
    }
    return delta;
}

bool CheckWithinRelativeToleranceOfMax(double value_1, double value_2, double relative_tolerance) {
    return fmax(value_1, value_2) - value_1 <= relative_tolerance * fmax(value_1, value_2);
}

std::string LocationDirectoryName(const LonLat &lon_lat) {
    SGBucket bucket {lon_lat.lon, lon_lat.lat};
    return bucket.gen_base_path();
}

std::filesystem::path ConstructPathToFiles(const std::string &base_directory,
                                           const std::string &scenery_type,
                                           const LonLat &lon_lat) {
    std::filesystem::path p {};
    p.append(base_directory);
    p.append(scenery_type);
    p.append(LocationDirectoryName(lon_lat));
    return p;
}

std::unique_ptr<geos::geom::Polygon> MakeLocalBox(const double x_min, const double y_min, const double x_max, const double y_max) {
    geos::geom::GeometryFactory::Ptr factory =
        TileHandler::MakeGeometryFactory();
    std::vector<geos::geom::Coordinate> coordinates {};
    coordinates.emplace_back(x_min, y_min);
    coordinates.emplace_back(x_max, y_min);
    coordinates.emplace_back(x_max, y_max);
    coordinates.emplace_back(x_min, y_max);
    coordinates.emplace_back(x_min, y_min);  // must close linear ring

    const geos::geom::CoordinateSequenceFactory* coord_factory = factory->getCoordinateSequenceFactory();
    std::unique_ptr<geos::geom::LinearRing> ring = factory->createLinearRing(coord_factory->create(std::move(coordinates)));
    return factory->createPolygon(std::move(ring));
}

TimeLogger::TimeLogger() {
    reference_ =std::chrono::system_clock::now();
}

void TimeLogger::Log(std::string message) {
    auto now_time = std::chrono::system_clock::now();
    auto elapsed_time_s = std::chrono::duration<double>(now_time - reference_).count();
    BOOST_LOG_TRIVIAL(info) << message << ": " << elapsed_time_s;
    reference_ = now_time;
}

void LogDebugWarning(std::string message) {
    BOOST_LOG_TRIVIAL(debug) << "**WARNING** " << message;
}

double CalcDistanceLocal(double x_1, double y_1, double x_2, double y_2) {
    return sqrt(pow(x_1 - x_2, 2) + pow(y_1 - y_2, 2));
}

std::string BuildFileNameWithTileIndex(const std::string &title, const std::string &tile_index, const std::string &post_fix, const bool include_time) {
    std::stringstream file_name;
    file_name << "osm2gear_" << title << "_";
    file_name << tile_index;
    if (include_time) {
        file_name << "_" << LocalTimeString().str();
    }
    file_name << "." << post_fix;
    return file_name.str();
}


void TokenizeString(const std::string &s, const char delim, std::vector<std::string> &out) {
    std::string::size_type beg = 0;
    for (auto end = 0; (end = s.find(delim, end)) != std::string::npos; ++end) {
        out.push_back(s.substr(beg, end - beg));
        beg = end + 1;
    }
    out.push_back(s.substr(beg));
}


std::vector<std::unique_ptr<geos::geom::Polygon>> MergePolygons(std::vector<std::unique_ptr<geos::geom::Polygon>> &original_list) {
    geos::geom::GeometryFactory::Ptr factory = TileHandler::MakeGeometryFactory();
    std::unique_ptr<geos::geom::MultiPolygon> collection = factory->createMultiPolygon(std::move(original_list));
    auto unified = collection->Union();

    std::vector<std::unique_ptr<geos::geom::Polygon>> handled_list {};
    if (unified->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON) {
        auto* p = dynamic_cast<geos::geom::Polygon*>(unified.get());
        if (p) {
            handled_list.push_back(p->clone());
        }
    } else if (unified->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_MULTIPOLYGON) {
        auto number_geometries = unified->getNumGeometries();
        for (int i = 0; i < number_geometries; ++i) {
            auto part_unified = unified->getGeometryN(i)->clone();
            if (!part_unified->isEmpty() and part_unified->isValid() and
                part_unified->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON) {
                auto* p = dynamic_cast<geos::geom::Polygon*>(part_unified.get());
                if (p) {
                    handled_list.push_back(p->clone());
                }
            }
        }
    }
    return handled_list;
}

std::vector<std::unique_ptr<geos::geom::Polygon>> MergeGeomsToPolys(std::vector<std::unique_ptr<geos::geom::Geometry>> &original_list) {
    //first make sure that the polygons to be merged are actually good polygons
    std::vector<std::unique_ptr<geos::geom::Polygon>> cleaned_list {};
    for (auto &poly : original_list) {
        if (poly->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON) {
            if (!poly->isEmpty() and poly->isValid()) {
                auto* p = dynamic_cast<geos::geom::Polygon*>(poly.get());
                if (p) {
                    cleaned_list.push_back(p->clone());
                }
            }
        } else if (poly->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_MULTIPOLYGON) {
            auto number_geometries = poly->getNumGeometries();
            for (int i = 0; i < number_geometries; ++i) {
                auto part_poly = poly->getGeometryN(i)->clone();
                if (!part_poly->isEmpty() and part_poly->isValid()) {
                    auto* p = dynamic_cast<geos::geom::Polygon*>(part_poly.get());
                    if (p) {
                        cleaned_list.push_back(p->clone());
                    }
                }
            }
        }
    }
    if (cleaned_list.size() < 2) {
        return cleaned_list;
    }
    return MergePolygons(cleaned_list);
}
