// Copyright Rick Gruber-Riemer 2022.
// Distributed under the GPL v2
// (See accompanying file LICENSE
// or copy at https://opensource.org/licenses/gpl-2.0.php)

#include <boost/program_options.hpp>
#include <iostream>

#include "boost/log/trivial.hpp"
#include "boost/log/utility/setup.hpp"
#include "main/landuse_processor.h"
#include "main/parameters.h"
#include "main/utils.h"

namespace po = boost::program_options;

//suppress the warning by GEOS to use the C-API instead
#define USE_UNSTABLE_GEOS_CPP_API

// based on https://github.com/snikulov/boost-log-example/blob/master/src/main.cpp
static void init_log(long tile_index) {
    static const std::string COMMON_FMT("[%TimeStamp%][%Severity%]:  %Message%");

    boost::log::register_simple_formatter_factory< boost::log::trivial::severity_level, char >("Severity");

    // Output message to console
    boost::log::add_console_log(
            std::cout,
            boost::log::keywords::format = COMMON_FMT,
            boost::log::keywords::auto_flush = true
    );

    // Output message to file, rotates when file reached 1mb or at midnight every day. Each log file
    // is capped at 1mb and total is 20mb
    boost::log::add_file_log (
            boost::log::keywords::file_name = BuildFileNameWithTileIndex("log", std::to_string(tile_index), "log", true),
            boost::log::keywords::format = COMMON_FMT,
            boost::log::keywords::auto_flush = true
    );

    boost::log::add_common_attributes();
}

constexpr char option_tile_index[] = "tile_index";
constexpr char option_db_name[] = "db_name";
constexpr char option_scenery[] = "scenery";
constexpr char option_output[] = "output";

/** \brief The main method of osm2gear to be called from command line.
 * There is no UI for osm2gear.
 * @param ac
 * @param av
 * @return 0 (zero) if everything worked as expected, 1 (one) in all other cases.
 */
int main(int ac, char* av[]) {
    try {
        // program options
        bool is_help{false}, create_trees{false}, use_ws3{false}, do_debug_plot{false}, do_debug_log{false};
        po::options_description desc("Available options for osm2gear");
        desc.add_options()
                ("help,h", po::bool_switch(&is_help), "produce help message")(
                "tile_index,i", po::value<long>(),
                    "set the index of the FG scenery tile to be processed")(
                "db_name,d", po::value<std::string>(),
                    "the database name for OSM data")(
                "scenery,s", po::value<std::string>(),
                    "the path to scenery")(
                "output,o", po::value<std::string>(),
                    "the path to the output")(
                "trees,t", po::bool_switch(&create_trees),
                "create trees")(
                "ws3,w", po::bool_switch(&use_ws3),
                "use WS 3.0 scenery")(
                "plot,p", po::bool_switch(&do_debug_plot),
                "run debug plotting to svg-files")(
                "l", po::bool_switch(&do_debug_log),
                "do logging at DEBUG level instead of INFO");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, desc), vm);
        po::notify(vm);

        if (is_help) {
            std::cout << desc << "\n";
            return 0;
        }
        if (not vm.count(option_tile_index)) {
            std::cout << "The mandatory parameter tile index was not set.\n";
            return 1;
        }
        if (not vm.count(option_db_name)) {
            std::cout << "The mandatory parameter db_name was not set.\n";
            return 1;
        }
        if (not vm.count(option_scenery)) {
            std::cout << "The mandatory parameter scenery was not set.\n";
            return 1;
        }
        if (not vm.count(option_output)) {
            std::cout << "The mandatory parameter output was not set.\n";
            return 1;
        }
        // enable and configure logging
        init_log(vm[option_tile_index].as<long>());
        if (do_debug_log) {
            boost::log::core::get()->set_filter(boost::log::trivial::severity >=
                                                boost::log::trivial::debug);

        } else {
            boost::log::core::get()->set_filter(boost::log::trivial::severity >=
                                                boost::log::trivial::info);
        }

        // set parameters from options
        Parameters::Get().PATH_TO_SCENERY = vm[option_scenery].as<std::string>();
        Parameters::Get().PATH_TO_OUTPUT = vm[option_output].as<std::string>();
        Parameters::Get().USE_WS30_SCENERY = use_ws3;
        Parameters::Get().DEBUG_PLOT_LANDUSE = do_debug_plot;
        Parameters::Get().DEBUG_PLOT_TREES = do_debug_plot;

        // the main code
        BOOST_LOG_TRIVIAL(info) << "osm2gear is the magic between OpenStreetMap data and FlightGear scenery";

        const TileHandler tile_handler{ vm[option_tile_index].as<long>()};
        BOOST_LOG_TRIVIAL(info) << "working on tile " << tile_handler.GetTileIndex();
        const Boundary extended_boundary = tile_handler.CreateExtendedBoundary(
            Parameters::Get().OWBB_PLACE_BOUNDARY_EXTENSION);
        OSMDBDataReader osm_reader{CreateDBConnectionString(vm[option_db_name].as<std::string>()),
                                   tile_handler.GetTileBoundary(),
                                   extended_boundary};
        ProcessLandUse(tile_handler, osm_reader, create_trees);

        BOOST_LOG_TRIVIAL(info) << "osm2gear is done with tile " << tile_handler.GetTileIndex() << std::endl;
    }
    catch(std::exception& e) {
        std::cerr << "There was an error in osm2gear: " << e.what() << "\n";
        return 1;
    }
    catch(...) {
        std::cerr << "There was an exception of unknown type in osm2gear!\n";
        return 1;
    }
    return 0;
}