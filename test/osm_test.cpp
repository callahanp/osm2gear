#include <gtest/gtest.h>

#include "main/buildings.h"
#include "main/utils.h"

TEST(UtilsTests, TestCalcDistanceLocal) {
    EXPECT_EQ(1, CalcDistanceLocal(0, 0, 0, 1));
    EXPECT_EQ(5, CalcDistanceLocal(0, -1, -4, 2));
}

TEST(UtilsTests, TestCalcHorizonElev) {
    auto elev_1 = CalcHorizonElev(2000, 2000);
    auto elev_2 = CalcHorizonElev(1000, 1000);
    EXPECT_GT(elev_1, elev_2);
}

TEST(UtilsTests, TestCalcDeltaBearing) {
    EXPECT_EQ(0, CalcDeltaBearing(0, 0)) << "No difference 0";
    EXPECT_EQ(0, CalcDeltaBearing(0, 360)) << "No difference 0/360";
    EXPECT_EQ(0, CalcDeltaBearing(360, 0)) << "No difference 360/0";
    EXPECT_EQ(180, CalcDeltaBearing(20, 200)) << "180 degrees";
    EXPECT_EQ(10, CalcDeltaBearing(10, 20)) << "10 with clock";
    EXPECT_EQ(-10, CalcDeltaBearing(20, 10)) << "10 against clock";
    EXPECT_EQ(20, CalcDeltaBearing(350, 10)) << "Through 0: 20 with clock";
    EXPECT_EQ(-20, CalcDeltaBearing(10, 350)) << "Through 0: 20 against clock";
}

TEST(UtilsTests, TestCheckWithinRelativeToleranceOfMax) {
    EXPECT_TRUE(CheckWithinRelativeToleranceOfMax(0.8, 1.0, 0.2));
    EXPECT_TRUE(CheckWithinRelativeToleranceOfMax(1.0, 0.8, 0.2));
    EXPECT_TRUE(CheckWithinRelativeToleranceOfMax(1.2, 1.0, 0.17));
    EXPECT_FALSE(CheckWithinRelativeToleranceOfMax(0, 2.0, 0.99));
}

TEST(BuildingsTests, TestCheckForRemovableBalcony) {
    const TileHandler tile_handler{1939999};
    geos::geom::Coordinate nodes[] = {geos::geom::Coordinate(0,0),
                                      geos::geom::Coordinate(2, 0),
                                      geos::geom::Coordinate(2, 0.9 * Parameters::Get().BUILDING_BALCONY_TOLERANCE_RAILING),
                                      geos::geom::Coordinate(6, 0.9 * Parameters::Get().BUILDING_BALCONY_TOLERANCE_RAILING),
                                      geos::geom::Coordinate(6, 0),
                                      geos::geom::Coordinate(8, 0),
    };
    EXPECT_TRUE(CheckForRemovableBalcony(nodes, tile_handler)) << "Balcony outwards ok";

    nodes[2] = geos::geom::Coordinate(2, -0.9 * Parameters::Get().BUILDING_BALCONY_TOLERANCE_RAILING);
    nodes[3] = geos::geom::Coordinate(6, -0.9 * Parameters::Get().BUILDING_BALCONY_TOLERANCE_RAILING);
    EXPECT_TRUE(CheckForRemovableBalcony(nodes, tile_handler)) << "Balcony inwards ok";

    nodes[1] = geos::geom::Coordinate(2,1.1 * Parameters::Get().BUILDING_BALCONY_TOLERANCE_BASE);
    EXPECT_FALSE(CheckForRemovableBalcony(nodes, tile_handler)) << "Base tolerance violated";

    nodes[1] = geos::geom::Coordinate(2,0);
    nodes[2] = geos::geom::Coordinate(2, 1.1 * Parameters::Get().BUILDING_BALCONY_TOLERANCE_RAILING);
    nodes[3] = geos::geom::Coordinate(6, 1.1 * Parameters::Get().BUILDING_BALCONY_TOLERANCE_RAILING);
    EXPECT_FALSE(CheckForRemovableBalcony(nodes, tile_handler)) << "Railing tolerance violated";

    nodes[2] = geos::geom::Coordinate(2, 0.9 * Parameters::Get().BUILDING_BALCONY_TOLERANCE_RAILING);
    nodes[3] = geos::geom::Coordinate(6, -0.9 * Parameters::Get().BUILDING_BALCONY_TOLERANCE_RAILING);
    EXPECT_FALSE(CheckForRemovableBalcony(nodes, tile_handler)) << "Diff tolerance violated";

    nodes[3] = geos::geom::Coordinate(8.1, 0.9 * Parameters::Get().BUILDING_BALCONY_TOLERANCE_RAILING);
    nodes[4] = geos::geom::Coordinate(8.1, 0);
    EXPECT_FALSE(CheckForRemovableBalcony(nodes, tile_handler)) << "Node sequence on front line violated";
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}