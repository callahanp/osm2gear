include(FetchContent)

# fetched with vcpkg
find_package(GTest REQUIRED)
if (GTest_FOUND)
    message(STATUS "Found GTest: ${GTest_CONFIG} (found version ${GTest_VERSION})")
endif ()


add_executable(${CMAKE_PROJECT_NAME}_test osm_test.cpp)


target_link_libraries(${CMAKE_PROJECT_NAME}_test
        PRIVATE
        GTest::GTest
        PUBLIC
        ${CMAKE_PROJECT_NAME}_lib
)
