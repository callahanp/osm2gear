include(FetchContent)
include(CMakePrintHelpers)

# using libGL.so (see also CMakeLists.txt at root of FlightGear)
if(POLICY CMP0072)
    cmake_policy(SET CMP0072 NEW)
    set(OpenGL_GL_PREFERENCE "LEGACY") # using libGL.so
endif()

# CLion uses built-in 3.26.x in April 2023 (Settings->Builds...->Toolchain)
# Ubuntu 23.04 Lunar has 3.25.x
# CMAKE with C++ modules: https://www.kitware.com/import-cmake-c20-modules/ needs at least 3.25
cmake_minimum_required(VERSION 3.25)
project(osm2gear)

# using -D CMAKE_CXX_COMPILER=/usr/bin/clang++-16 - https://www.jetbrains.com/help/clion/how-to-switch-compilers-in-clion.html
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS_RELEASE "-O3")
set(CMAKE_CXX_EXTENSIONS OFF)

set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/CMakeModules;${CMAKE_MODULE_PATH}")

add_subdirectory(src)
add_subdirectory(test)

enable_testing()
add_test (NAME AllTests COMMAND OSMTest)
