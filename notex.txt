Boundary boundary {-3.5, 55.875, -3.25, 56.0}; //Edinburgh 2892922
Boundary boundary {8.5, 47.375, 8.75, 47.5}; //LSZH 388986
Boundary boundary {8.75, 47.0, 9.0, 47.125}; //Wägitalersee 3088965
Boundary boundary {8.25, 47, 8.5, 47.125}; //LSME 3088961
Lelystad: NW (core): 3040161, SW: 3040153, SE (airport): 3040154, NE: 3040162

Gruyeres LSGT: -i 3072544 -d swiss -s /home/vanosten/custom_scenery/WS30_Alps -o /home/vanosten/custom_scenery/WS30_Alps -t -w

California: -i 942074 -d california -s /home/vanosten/custom_scenery/WS30_CA2 -o /home/vanosten/custom_scenery/WS30_CA2_O2C -w -p

Martinique TFFF -> Anse Marigot in South West: -i 1939999 -d caribic -s /home/vanosten/custom_scenery/WS30_MTQ_5m -o /home/vanosten/custom_scenery/WS30_MTQ_5m_O2C -w -p
                   East of runway (some town): 1956384
                   Fort de France:             1940007


=======
SIMGEAR
=======

SimGear:
* math/SGGeod: cartesian 3d earth centred
* simgear/tgdb/.. :
    * the TerraGear and WS3.0 stuff
    * VPBTechnique.cpp: good reference for how Stuart does the calculations and transformations for roads
* misc/sg_path.hxx: path differences between OS's (e.g. MacOS)
* io/sg_file.hxx: file handling

FlightGear:
* flightgear/utils/stgmerge: another good source to see how SGGeod is used


========================
Stuff to mention in docs
========================
* Minimal exception handling. It is expected that e.g. file paths exist and/or can be written to. Most of the stuff is not recoverable no matter what.
* Operations are always on tile level. It is not possible to only write out parts of a tile.


https://postgis.net/docs/using_postgis_dbmanagement.html#OGC_Validity


================================================
NASTY STUFF FOR THE INEXPERIENCED C++ PROGRAMMER
================================================
* Process finished with exit code 139 (interrupted by signal 11: SIGSEGV): memory problem somewhere -> debug
* If libraries cannot be found etc.: close the IDE and remove file:///.../osm2gear/cmake-build-debug/CMakeCache.txt
* From time to time: rebuild from scratch


===============
Google Protobuf
===============
* Using proto3
* https://protobuf.dev/
* v3 guide: https://protobuf.dev/programming-guides/proto3/
* Python and C++ versions should be in sync. See https://protobuf.dev/support/version-support/ for compatible versions
    * Python: 4.21.12
    * C++: 3.21.12 (from Ubuntu 23.04 - $ protoc --version)
* Download the "python" tar.gz from https://github.com/protocolbuffers/protobuf/releases/tag/v21.12 and extract into $home/bin
    * To install the runtime and compiler follow the readme.md in subfolder /src. On Ubuntu no need to change LD_LIBRARY_PATH.
* The fields in the message correspond in general to the naming in C++ (however without the trailing "_")
* Generating C++ (e.g. from Terminal within CLion): ~/develop_vcs/osm2gear$ protoc --cpp_out=./src/proto buildings.proto
* Generating .py (e.g. from Terminal within PyCharm): ~/develop_vcs/osm2city$ protoc --proto_path=../osm2gear --python_out=osm2city/proto ../osm2gear/buildings.proto

====================
Compiler Performance
====================
Using Release in CMake (in Clion) vs. Debug does not show considerable improvements. Maybe GEOS should be built with Release instead of Debug and then there would probably be significant improvements.

=====
vcpkg
=====
Using https://vcpkg.io/en/index.html

In CLion CMake config: -D CMAKE_CXX_COMPILER=/usr/bin/clang++-16 -DCMAKE_TOOLCHAIN_FILE=/home/vanosten/.vcpkg-clion/vcpkg/scripts/buildsystems/vcpkg.cmake